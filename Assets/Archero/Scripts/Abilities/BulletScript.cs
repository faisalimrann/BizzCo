﻿using UnityEngine;
using System.Collections;
using System;

public class BulletScript : MonoBehaviour
{

	public int MaxDamageAllowed = 0;
	public float CritChance =0f;
	public int PassiveDamageFire =0;
	public int FireDamageDuration = 0;
	public int PassiveDamagePoison = 0;
	public int PoisonDamageDuration = 0;
	public int CriticalDamage = 0;
	// Use this for initialization
	public float Pushback;
    public Action<Collision> OnHit;
    public int HitCount { get; private set; } 
	
    private Rigidbody body;
	[SerializeField]
	LayerMask destructiveBodies;

	private void OnEnable()
	{
		body = GetComponent<Rigidbody>();
		body.velocity = 1 * 10f * transform.forward;
		Destroy(this.gameObject, 3f);
	}

	public void ApplyModification(DamageModifierData data)
	{
		MaxDamageAllowed = data.MaxDamageAllowed;
		CritChance = data.CritChance;
		PassiveDamageFire = data.PassiveDamageFire;
		FireDamageDuration = data.FireDamageDuration;
		PassiveDamagePoison = data.PassiveDamagePoison;
		PoisonDamageDuration = data.PoisonDamageDuration;
		CriticalDamage = data.CritDamage;
	}

	void OnCollisionEnter( Collision collision )
	{
		if ( collision.gameObject.GetComponent<Enemy>() != null )
		{
			int damage = MaxDamageAllowed;
			float random = UnityEngine.Random.Range(0f, 100.1f);
			if ( random <= CritChance )
			{
				damage += CriticalDamage;
			}
			
			Enemy enemy = collision.gameObject.GetComponent<Enemy>();
			enemy.TakeDamage(damage);

			//enemy.GetComponent<Rigidbody>().AddForce(this.transform.forward * Pushback);

			this.gameObject.SetActive(false);
		}
		else if (destructiveBodies == (1<<collision.gameObject.layer | destructiveBodies)) {
			this.gameObject.SetActive(false);
		}
	}

}
