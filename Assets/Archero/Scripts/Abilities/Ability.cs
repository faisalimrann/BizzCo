﻿using System.Collections;
using System.Collections.Generic;
using GameData;
using UnityEngine;

public abstract class Ability : MonoBehaviour
{
	public abstract void ApplyAbilityOnWeapon(ref WeaponData weapon, ref DamageModifierData damageData);
	public abstract void ApplyAbilityOnHero(Warrior warrior);
	public AbilityID abilityID;
	public string Name => DataManager.Instance.GetAbilityData(abilityID).AbilityName;

}
