﻿

public struct WeaponData
{
	public int FrontArrowCount;
	public int SideArrowCount;
	public int DiagonalArrowCount;
	public int ShotCount;
	public int RearArrowCount;
	public void Init()
	{
		FrontArrowCount = 1;
		SideArrowCount = 0;
		DiagonalArrowCount = 0;
		ShotCount = 1;
		RearArrowCount = 0;
	}
}
