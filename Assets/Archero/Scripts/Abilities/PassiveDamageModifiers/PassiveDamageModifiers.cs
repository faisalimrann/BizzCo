﻿using UnityEngine;
using System.Collections;

public class PassiveDamageModifiers : Ability
{
	[SerializeField]
	protected int Damage;
	[SerializeField]
	protected int Duration;

	public override void ApplyAbilityOnHero( Warrior warrior )
	{
	}

	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
	}
}
