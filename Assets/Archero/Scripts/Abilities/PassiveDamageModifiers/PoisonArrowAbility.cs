﻿using UnityEngine;
using System.Collections;

public class PoisonArrowAbility : PassiveDamageModifiers
{
	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
		base.ApplyAbilityOnWeapon(ref weapon, ref damageData);
		damageData.FireDamageDuration = Duration;
		damageData.PassiveDamageFire = Damage;
	}
}
