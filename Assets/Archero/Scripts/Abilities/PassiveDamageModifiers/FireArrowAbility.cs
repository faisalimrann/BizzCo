﻿using UnityEngine;
using System.Collections;

public class FireArrowAbility : PassiveDamageModifiers
{

	// Use this for initialization
	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
		base.ApplyAbilityOnWeapon(ref weapon, ref damageData);
		damageData.PoisonDamageDuration = Duration;
		damageData.PassiveDamagePoison = Damage;
	}
}
