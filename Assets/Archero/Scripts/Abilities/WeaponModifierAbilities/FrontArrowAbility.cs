﻿using UnityEngine;
using System.Collections;

public class FrontArrowAbility : WeaponModifiers
{

	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
		base.ApplyAbilityOnWeapon(ref weapon, ref damageData);
		weapon.FrontArrowCount++;
	}

}
