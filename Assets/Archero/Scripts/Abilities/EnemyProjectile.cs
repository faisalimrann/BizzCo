﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
	private Rigidbody body;
	[SerializeField]
	LayerMask destructiveBodies;
	[SerializeField]
	LayerMask playerLayer;

	public int damage = 300;
	private int firingAngle;
	private float gravity;

	[SerializeField] ParticleSystem FloorHitParticleFX;
	[SerializeField] ParticleSystem ProjectiileParticle;

	[Range(30, 60)]
	public float LaunchAngle = 45f;

	Transform _targetObject;
	public Transform TargetTransform { get
		{
			return _targetObject;
		}
		set
		{
			_targetObject = value;
			Launch();
		}
	}

	private void OnEnable()
	{
		body = GetComponent<Rigidbody>();

	}

	public void ApplyModification( DamageModifierData data )
	{

	}

	void OnCollisionEnter( Collision collision )
	{

			Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, 1, playerLayer);
			if ( hitColliders.Length == 1 )
			{
            hitColliders[0].GetComponentInParent<PlayerManager>().TakeDamage(damage);
        }
			Destroy(this.gameObject, 1.5f);
			ProjectiileParticle.Stop();
			body.constraints = RigidbodyConstraints.FreezeAll;
			GetComponent<SphereCollider>().enabled = false;
			FloorHitParticleFX.Play();

	}

	// launches the object towards the TargetObject with a given LaunchAngle
	void Launch()
	{
		// think of it as top-down view of vectors: 
		//   we don't care about the y-component(height) of the initial and target position.
		Vector3 projectileXZPos = new Vector3(transform.position.x, 0.0f, transform.position.z);
		Vector3 targetXZPos = new Vector3(TargetTransform.position.x, 0.0f, TargetTransform.position.z);

		// rotate the object to face the target
		transform.LookAt(targetXZPos);

		// shorthands for the formula
		float R = Vector3.Distance(projectileXZPos, targetXZPos);
		float G = Physics.gravity.y;
		float tanAlpha = Mathf.Tan(LaunchAngle * Mathf.Deg2Rad);
		float H = 0;

		// calculate the local space components of the velocity 
		// required to land the projectile on the target object 
		float Vz = Mathf.Sqrt(G * R * R / (2.0f * (H - R * tanAlpha)));
		float Vy = tanAlpha * Vz;

		// create the velocity vector in local space and get it in global space
		Vector3 localVelocity = new Vector3(0f, Vy, Vz);
		Vector3 globalVelocity = transform.TransformDirection(localVelocity);

		// launch the object by setting its initial velocity and flipping its state
		body.velocity = globalVelocity;
	}
}

