﻿using UnityEngine;
using System.Collections;

public class HealthModifiers : Ability
{
	[SerializeField]
	protected int HealthIncrease;
	[SerializeField]
	protected float Multiplier; 

	public override void ApplyAbilityOnHero( Warrior warrior )
	{
	}

	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
	}

}
