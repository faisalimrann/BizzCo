﻿using UnityEngine;
using System.Collections;

public class HPBoost : HealthModifiers
{
	public override void ApplyAbilityOnHero(  Warrior warrior )
	{
		base.ApplyAbilityOnHero( warrior);
		warrior.MaxHealth += HealthIncrease;
		warrior.Health += HealthIncrease;
	}

}
