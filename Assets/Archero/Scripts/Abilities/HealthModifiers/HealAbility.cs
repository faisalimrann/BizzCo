﻿using UnityEngine;
using System.Collections;

public class HealAbility : HealthModifiers
{
	public override void ApplyAbilityOnHero( Warrior warrior )
	{
		base.ApplyAbilityOnHero( warrior);
		if ( (warrior.Health + (HealthIncrease * Multiplier)) > warrior.MaxHealth )
		{
			warrior.Health = warrior.MaxHealth;
		}
		else
		{
			warrior.Health += (int)(HealthIncrease * Multiplier);
		}
	}
}
