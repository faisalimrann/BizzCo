﻿using UnityEngine;
using System.Collections;

public class AttackBoostAbility : DamageModifiers
{
	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
		base.ApplyAbilityOnWeapon(ref weapon, ref damageData);
		damageData.MaxDamageAllowed += DamageIncrease;
	}

}
