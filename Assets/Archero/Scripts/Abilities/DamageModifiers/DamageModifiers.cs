﻿using UnityEngine;
using System.Collections;

public class DamageModifiers : Ability
{
	[SerializeField]
	protected float CritChanceIncrease;
	[SerializeField]
	protected int DamageIncrease;

	public override void ApplyAbilityOnHero( Warrior warrior )
	{
		
	}

	public override void ApplyAbilityOnWeapon(ref WeaponData weapon, ref DamageModifierData damageData )
	{

	}
}
