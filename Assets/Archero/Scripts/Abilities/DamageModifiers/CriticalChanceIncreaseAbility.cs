﻿using UnityEngine;
using System.Collections;

public class CriticalChanceIncreaseAbility : DamageModifiers
{
	public override void ApplyAbilityOnWeapon( ref WeaponData weapon, ref DamageModifierData damageData )
	{
		base.ApplyAbilityOnWeapon(ref weapon, ref damageData);
		damageData.CritChance += CritChanceIncrease;
	}
}
