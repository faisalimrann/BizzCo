﻿using UnityEngine;
using System.Collections;

public struct DamageModifierData
{
	public int MaxDamageAllowed;
	public float CritChance;
	public int PassiveDamageFire;
	public int FireDamageDuration;
	public int PassiveDamagePoison;
	public int PoisonDamageDuration;
	public int CritDamage;
	// Use this for initialization

	public void Init()
	{
		MaxDamageAllowed = 0;
		CritChance = 0f;
		PassiveDamageFire = 0;
		FireDamageDuration = 0;
		PassiveDamagePoison = 0;
		PoisonDamageDuration = 0;
		CritDamage = 0;
	}
}

