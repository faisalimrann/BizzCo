﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelsManager : MonoBehaviour
{
    private void Awake()
    {
        manager = this;
    }
    private static LevelsManager manager;
    public static LevelsManager Manager
    {
        get
        {
            if(!manager)
            {
                manager = Instantiate(Resources.Load<LevelsManager>("Prefabs/FragLevelsManager"));
                DontDestroyOnLoad(manager.gameObject);
            }
            return manager;
        }
    }
    public uint Level { get; private set; }
    public abstract int GetLevels();
    public abstract void LoadLevel(out List<Obstacle> obstacles, out List<Enemy> enemies);
    public void SetLevel(uint level)
    {
        Level = level;
    }
	public int GetLevel()
	{
		return (int)Level;
	}
}

public class Level
{
    public string name;
    public string data;
}
[System.Serializable]
public class LevelData
{
    public ObstacleData[] obstaclesData;
    public EnemyParameters[] enemyParameters;

    public static Vector3 ToPositionOnMap(Vector3 value)
    {
        return new Vector3((int)value.x, (int)value.y, (int)value.z);
    }
    public static Quaternion ToRotationOnMap(Quaternion value)
    {
        return new Quaternion((int)value.x, (int)value.y, (int)value.z, (int)value.w);
    }
}

