﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUIManager : MonoBehaviour
{
    [SerializeField] private Button m_LevelButton;

    private void Awake()
    {
        int TotalLevels = LevelsManager.Manager.GetLevels();
        m_LevelButton.gameObject.SetActive(false);
        for (int i = 0; i < TotalLevels; i++)
        {
            Button button = Instantiate(m_LevelButton, m_LevelButton.transform.parent);
            int index = i;
            button.onClick.AddListener(() =>
            {
                LoadLevel((uint)index + 1);
            });
            button.GetComponentInChildren<Text>().text = "Level "+i;
            button.gameObject.SetActive(true);
        }
    }

    private void LoadLevel(uint level)
    {
        LevelsManager.Manager.SetLevel(level);
        SceneManager.LoadScene("Game");
    }
}
