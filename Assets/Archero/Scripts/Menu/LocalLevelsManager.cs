﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LocalLevelsManager : LevelsManager
{
    [SerializeField] private TextAsset[] levelsAssets;

    public override int GetLevels()
    {
        Level[] levels = new Level[levelsAssets.Length];
        //for (int i = 0; i < levels.Length; i++)
        //{
        //    levels[i] = new Level { name = levelsAssets[i].name, data = levelsAssets[i].text };
        //}
        return levels.Length;
    }

    public override void LoadLevel(out List<Obstacle> obstacles, out List<Enemy> enemies)
    {
        LevelData levelData = JsonUtility.FromJson<LevelData>(levelsAssets[Level - 1].text);
        obstacles = new List<Obstacle>();
        int index = 0;
        foreach (var obstacleData in levelData.obstaclesData)
        {
            obstacles[index] = Instantiate(Resources.Load<Obstacle>("Prefabs/Obstacles/" + obstacleData.type), obstacleData.position, obstacleData.rotation);
            index++;
        }
        enemies = new List<Enemy>();
        index = 0;
        foreach (var warriorParameters in levelData.enemyParameters)
        {
            enemies[index] = Instantiate(Resources.Load<Enemy>("Prefabs/Enemies/" + warriorParameters.name),
            warriorParameters.moveParameters.position, warriorParameters.moveParameters.rotation);
            enemies[index].Init();
            index++;
        }
    }
}
