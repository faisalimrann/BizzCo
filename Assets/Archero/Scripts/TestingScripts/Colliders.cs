﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colliders : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(gameObject.name + "OnCollisionEnter" + collision.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(gameObject.name + "OnTriggerEnter" + other.gameObject);
    }


}
