﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WASDInputManager : MonoBehaviour, IMoveInputManager
{
    public event Action<Vector3> OnControl;

	public void Disable()
	{
		this.enabled = false;
	}

	private void FixedUpdate()
    {
        Vector3 direction = Vector3.zero;
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		if ( h != 0f)
        {
			if(h < 0)
				direction += Vector3.left;
			else if(h>0)
				direction += Vector3.right;
        }

		if ( v != 0f )
		{
			if ( v > 0 )
				direction += Vector3.forward;
			else if ( v < 0 )
				direction += Vector3.back;
		}
		OnControl?.Invoke(direction.normalized);
    }
}
