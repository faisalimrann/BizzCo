﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShotManager : DefaultShotManager
{
    private EnemyHealthManager enemyHealthManager;

    public void SetEnemyHealthManager(EnemyHealthManager enemyHealthManager)
    {
        this.enemyHealthManager = enemyHealthManager;
    }

    protected override Transform FindTarget()
    {
        if(enemyHealthManager.Enemies.Count == 0)
        {
            return null;
        }
        return FindNearEnemy(enemyHealthManager.Enemies);
    }

    private Transform FindNearEnemy(List<Enemy> enemies)
    {
        float distance = float.MaxValue;
        Transform target = null;
        foreach (var enemy in enemies)
        {
            if (enemy)
            {
                float newDistance = Vector3.Distance(transform.position, enemy.DamagePoint.position);
                if (distance > newDistance)
                {
                    distance = newDistance;
                    target = enemy.DamagePoint;
                }
            }
        }
        return target;
    }
}
