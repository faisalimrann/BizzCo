﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
[RequireComponent(typeof(PlayerShotManager))]
public class PlayerManager : Warrior
{

	WeaponData weaponData;
	DamageModifierData damageModifierData;

	List<HealthModifiers> healthModifiersList = new List<HealthModifiers>();
	List<DamageModifiers> damageModifiersList = new List<DamageModifiers>();
	List<PassiveDamageModifiers> passiveDamageModifiersList = new List<PassiveDamageModifiers>();
	List<WeaponModifiers> weaponModifiersList = new List<WeaponModifiers>();

	[SerializeField]
	float MaxArrowDeviations = 1f;

	[SerializeField]
	BulletScript prefabInstance;

	[SerializeField]
	Transform bulletInstatiateTransform;

	public Action OnEnterDoor;
	private PlayerShotManager playerShotManager;

	List<Ability> appliedAbilities = new List<Ability>();

	public delegate void AbilityApplied( Ability ability );
	public static event AbilityApplied onAbilityApplied;

	[SerializeField] 
	LayerMask meleeAttackLayer;

    public void Revive()
    {
		Health = MaxHealth;
		isDead = false;
		isInvincible = true;
		moveManager.Enable();
		animation.PlayAnimation(AnimationData.Idle);
		animation.SetIsDeadForPlayer(false);
		coroutine = StartCoroutine("StartShooting");
		healthBar.UpdateHealthBar(1f,"", MaxHealth.ToString());
		vFXManager.Stop(VFX.Death);
		//GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		StartCoroutine("Invincible");
	}

	public void AddAbility( Ability ability )
	{
		appliedAbilities.Add(ability);
		onAbilityApplied?.Invoke(ability);
		if ( ability is HealthModifiers)
		{
			healthModifiersList.Add((HealthModifiers)ability);
			ability.ApplyAbilityOnHero(this);
			healthBar.UpdateHealthBar((Health*1f)/MaxHealth,"", Health.ToString());
			vFXManager.PlayVFX(VFX.Heal);
		}
		else if ( ability is DamageModifiers )
		{
			damageModifiersList.Add((DamageModifiers)ability);
			ability.ApplyAbilityOnWeapon(ref weaponData, ref damageModifierData);
		}
		else if ( ability is PassiveDamageModifiers  )
		{
			passiveDamageModifiersList.Add((PassiveDamageModifiers)ability);
		}
		else if ( ability is WeaponModifiers)
		{
			weaponModifiersList.Add((WeaponModifiers)ability);
			ability.ApplyAbilityOnWeapon(ref weaponData, ref damageModifierData);
		}
	}

	Coroutine coroutine;
	EnemyHealthManager enemyHealthManager;

	IEnumerator Invincible()
	{
		yield return new WaitForSeconds(10f);
		isInvincible = false;
	}

	public override void Init()
	{
		//if ( SystemInfo.deviceType == DeviceType.Desktop )
		//{
		inputManager = Instantiate(Resources.Load<WASDInputManager>("PlayerControl/WASDManager"));
		//}
		//else if ( SystemInfo.deviceType == DeviceType.Handheld )
		{
			//Todo
			//Instantiate(Resources.Load<FixedJoystick>(@"Joystick Pack/Prefabs/Fixed Joystick"));
		}
		playerShotManager = GetComponent<PlayerShotManager>();
		base.Init();
		//FixedJoystick.OnControl += moveManager.Move;
		FloatingJoystick.OnControl += moveManager.Move;
		weaponData.Init();
		damageModifierData.Init();
		moveManager.OnStartMove += () =>
		{
			//Debug.Log("Running");
			vFXManager.Stop(VFX.DamageTaken);
			vFXManager.Stop(VFX.DamageTakenFromThorn);
			if ( coroutine != null )
				StopCoroutine(coroutine);
			
			coroutine = null;
			SoundManager.Play("Run");
		};
	
		moveManager.OnStopMove += () => { SoundManager.Stop("Run"); coroutine = StartCoroutine("StartShooting"); };
		GetDataFromPlayerData();
	}

	void GetDataFromPlayerData() {
		PlayerData instance = PlayerData.Instance;
		MaxHealth = 0;
		if ( instance.CurrentWeapon != null )
		{

			float value = float.Parse(instance.CurrentWeapon.CurrentWeaponLevel.CritcalChance.Split('%')[0]);
			damageModifierData.MaxDamageAllowed += (int)instance.CurrentWeapon.CurrentWeaponLevel.WeaponDamage;
			damageModifierData.CritChance += value;
			damageModifierData.CritDamage =( (int)instance.CurrentWeapon.CurrentWeaponLevel.CriticalDamage - (int)instance.CurrentWeapon.CurrentWeaponLevel.WeaponDamage);

		}

		if ( instance.CurrentHero != null )
		{
			MaxHealth = (int)instance.CurrentHero.GetCurrentHeroLevel.HeroHealth;
			damageModifierData.MaxDamageAllowed += (int)instance.CurrentHero.GetCurrentHeroLevel.HeroDamage;
		}
		
		if ( instance.CurrentHelmet!= null )
		{
			MaxHealth += (int)instance.CurrentHelmet.CurrentEquipmentLevel.EquipmentHealth;
			damageModifierData.MaxDamageAllowed += (int)instance.CurrentHelmet.CurrentEquipmentLevel.Damage;
			//Todo AttackSpeed
		}
		if ( instance.CurrentBoots != null )
		{
			MaxHealth += (int)instance.CurrentBoots.CurrentEquipmentLevel.EquipmentHealth;
			damageModifierData.MaxDamageAllowed += (int)instance.CurrentBoots.CurrentEquipmentLevel.Damage;
			//Todo AttackSpeed
		}
		if ( instance.CurrentArmour != null )
		{
			MaxHealth += (int)instance.CurrentArmour.CurrentEquipmentLevel.EquipmentHealth;
			damageModifierData.MaxDamageAllowed += (int)instance.CurrentArmour.CurrentEquipmentLevel.Damage;
			//Todo AttackSpeed
		}
		Health = MaxHealth;
		healthBar.UpdateHealthBar(1f,"", MaxHealth.ToString());

	}

	public override void StopShot()
	{
		base.StopShot();

		StopCoroutine("StartShooting");
		coroutine = null;
	}
	public List<BulletScript> InstatniateBullets()
	{
		List<BulletScript> bullets = new List<BulletScript>();
		float deviation = (weaponData.FrontArrowCount > 1) ? MaxArrowDeviations / weaponData.FrontArrowCount : 0f;
		float j = -1 * deviation;
		Vector3 position;
		BulletScript temporary;
		for ( int i = 0; i < weaponData.FrontArrowCount; i++, j += deviation )
		{
			position = new Vector3(bulletInstatiateTransform.position.x + j, bulletInstatiateTransform.position.y, bulletInstatiateTransform.position.z);
			temporary = Instantiate(prefabInstance, position, Quaternion.identity);
			temporary.transform.forward = bulletInstatiateTransform.forward;
			bullets.Add(temporary);
		}

		deviation = (weaponData.SideArrowCount > 1) ? MaxArrowDeviations / weaponData.SideArrowCount : 0f;
		j = -1 * deviation;

		for ( int i = 0; i < weaponData.SideArrowCount; i++, j += deviation )
		{
			//Left Side Arrow
			position = new Vector3(bulletInstatiateTransform.position.x, bulletInstatiateTransform.position.y +j, bulletInstatiateTransform.position.z);
			temporary = Instantiate(prefabInstance, position, Quaternion.identity);
			temporary.transform.forward = -1 * bulletInstatiateTransform.right;
			bullets.Add(temporary);
			//Right side arrow
			position = new Vector3(bulletInstatiateTransform.position.x, bulletInstatiateTransform.position.y + j, bulletInstatiateTransform.position.z);
			temporary = Instantiate(prefabInstance, position, Quaternion.identity);
			temporary.transform.forward = bulletInstatiateTransform.right;
			bullets.Add(temporary);
		}

		deviation = (weaponData.DiagonalArrowCount > 1) ? MaxArrowDeviations / weaponData.DiagonalArrowCount : 0f;
		j = -1 * deviation;

		Vector3 upRight = (bulletInstatiateTransform.forward + bulletInstatiateTransform.right);
		Vector3 upLeft = (bulletInstatiateTransform.forward - bulletInstatiateTransform.right);
		for ( int i = 0; i < weaponData.DiagonalArrowCount; i++, j += deviation )
		{
			//Left Side Arrow
			position = new Vector3(bulletInstatiateTransform.position.x + j, bulletInstatiateTransform.position.y + j, bulletInstatiateTransform.position.z);
			temporary = Instantiate(prefabInstance, position, Quaternion.identity);
			temporary.transform.forward = upLeft;
			bullets.Add(temporary);
			//Right side arrow
			position = new Vector3(bulletInstatiateTransform.position.x + j, bulletInstatiateTransform.position.y + j, bulletInstatiateTransform.position.z);
			temporary = Instantiate(prefabInstance, position, Quaternion.identity);
			temporary.transform.forward = upRight;
			bullets.Add(temporary);
		}

		deviation = (weaponData.RearArrowCount > 1) ? MaxArrowDeviations / weaponData.RearArrowCount : 0f;
		j = -1 * deviation;

		for ( int i = 0; i < weaponData.RearArrowCount; i++, j += deviation )
		{
			position = new Vector3(bulletInstatiateTransform.position.x + j, bulletInstatiateTransform.position.y, bulletInstatiateTransform.position.z);
			temporary = Instantiate(prefabInstance, position, Quaternion.identity);
			temporary.transform.forward = -1 * bulletInstatiateTransform.forward;
			bullets.Add(temporary);

		}

		
		for ( int i = 0; i < bullets.Count; i++ )
		{
			bullets[i].ApplyModification(damageModifierData);
		}
			return bullets;
	}

	IEnumerator StartShooting()
	{
		List<BulletScript> bulletsToSpawn = new List<BulletScript>();
		Transform target;
		while ( true )
		{
			target= FindTarget();
			if ( !target || !gameObject.activeInHierarchy )
			{
				break;
			}
            yield return new WaitForSeconds(0.1f);
            Vector3 direction = (target.position - transform.position).normalized;
            //transform.rotation = Quaternion.LookRotation(target.position - transform.position);
            transform.forward = Vector3.ProjectOnPlane(direction, Vector3.up).normalized;
            for ( int i = 0; i < weaponData.ShotCount; i++ )
			{
				animation.PlayAnimation(AnimationData.Attack);
				vFXManager.PlayVFX(VFX.Shot);
				bulletsToSpawn = InstatniateBullets();
				for ( int j = 0; j < bulletsToSpawn.Count; j++ )
				{
					bulletsToSpawn[j].gameObject.SetActive(true);
					SoundManager.Play("Pistol");
				}
				if ( weaponData.ShotCount > 1 )
				{
					yield return new WaitForSeconds(0.1f);
				}
			}

			yield return new WaitForSeconds(1f);
		}
	}

	public void SetEnemyHealthManager( EnemyHealthManager _enemyHealthManager )
	{
		enemyHealthManager = _enemyHealthManager;
		playerShotManager.SetEnemyHealthManager(_enemyHealthManager);
	}

	private void OnTriggerEnter( Collider other )
	{
		if ( other.gameObject.tag == "Finish" )
		{
			SoundManager.Play("Level Up");
			OnEnterDoor?.Invoke();
		}
	}

	Transform FindTarget()
	{
		if ( enemyHealthManager.Enemies.Count == 0 )
		{
			return null;
		}
		return FindNearEnemy(enemyHealthManager.Enemies);
	}

	private Transform FindNearEnemy( List<Enemy> enemies )
	{
		float distance = float.MaxValue;
		Transform target = null;
		foreach ( var enemy in enemies )
		{
			if ( enemy  && !enemy.gameObject.name.Contains("Angel"))
			{
				float newDistance = Vector3.Distance(transform.position, enemy.DamagePoint.position);
				if ( distance > newDistance )
				{
					distance = newDistance;
					target = enemy.DamagePoint;
				}
			}
		}
		return target;
	}

	private void OnCollisionEnter( Collision collision )
	{
		Bow bullet = collision.gameObject.GetComponent<Bow>();
		if ( bullet )
		{
			TakeDamage(bullet.damage);
		}
		if ( meleeAttackLayer == (1 << collision.gameObject.layer | meleeAttackLayer) )
		{
			TakeDamage(prefabInstance.MaxDamageAllowed);
		}
	}

}


