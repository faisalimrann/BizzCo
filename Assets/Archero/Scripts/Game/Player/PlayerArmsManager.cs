﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameData;

public class AbilityPopupPopulator{
	public SpriteRenderer sprite;
	public string name;
}

public class PlayerArmsManager : MonoBehaviour
{
   [SerializeField] private Ability[] armsItems;
    private List<Ability> selectedArmsItems;

	[SerializeField]
	int HealAbilityIndex = 0;

	public List<AbilityPopupPopulator> Get3RandomSprites()
    {
        if(armsItems.Length < 4)
        {
            Debug.LogWarning("Arms Length < 4");
            return null;
        }
        List<AbilityPopupPopulator> sprites = new List<AbilityPopupPopulator>();
        selectedArmsItems = new List<Ability>();
        int randomIndex = UnityEngine.Random.Range(0, armsItems.Length * 100) % armsItems.Length;
        for (int i = 0; i < 3; i++)
        {
			AbilityPopupPopulator temp = new AbilityPopupPopulator();
				temp.sprite = armsItems[randomIndex].GetComponent<SpriteRenderer>();
			temp.name = armsItems[randomIndex].Name;

			while (sprites.Exists(item => item.name.Equals(temp.name) ))
			{
                randomIndex = UnityEngine.Random.Range(0, armsItems.Length * 100) % armsItems.Length;
				temp.sprite = armsItems[randomIndex].GetComponent<SpriteRenderer>();
				temp.name = armsItems[randomIndex].Name;
			}
            sprites.Add(temp);
            selectedArmsItems.Add(armsItems[randomIndex]);
			randomIndex = UnityEngine.Random.Range(0, armsItems.Length *100) % armsItems.Length;
		}
        return sprites;
    }


	public List<AbilityPopupPopulator> GetSpritesForAngel()
	{
		if ( armsItems.Length < 4 )
		{
			Debug.LogWarning("Arms Length < 4");
			return null;
		}
		List<AbilityPopupPopulator> sprites = new List<AbilityPopupPopulator>();
		selectedArmsItems = new List<Ability>();
		//SpriteRenderer temp = armsItems[HealAbilityIndex].GetComponent<SpriteRenderer>();
		AbilityPopupPopulator temp = new AbilityPopupPopulator();
		temp.sprite = armsItems[HealAbilityIndex].GetComponent<SpriteRenderer>();
		temp.name = armsItems[HealAbilityIndex].Name;

		sprites.Add(temp);
		selectedArmsItems.Add(armsItems[HealAbilityIndex]);
		List<Ability> toSelectFrom = new List<Ability>();
		for ( int i = 0; i < armsItems.Length; i++ )
		{
			if ( i != HealAbilityIndex )
			{
				if ( armsItems[i] is HealthModifiers || armsItems[i] is DamageModifiers )
				{
					toSelectFrom.Add(armsItems[i]);
				}
			}
		}
		int randomIndex = UnityEngine.Random.Range(0, toSelectFrom.Count * 100) % toSelectFrom.Count;
		temp =new AbilityPopupPopulator();
		temp.sprite = toSelectFrom[randomIndex].GetComponent<SpriteRenderer>();
		temp.name = toSelectFrom[randomIndex].Name;
		sprites.Add(temp);
		selectedArmsItems.Add(toSelectFrom[randomIndex]);
		return sprites;
	}

	public Ability GetArms(int id)
    {
        if(id < selectedArmsItems.Count)
        {
            return selectedArmsItems[id];
        }
        return null;
    }
}

[Serializable]
public class ArmsItem
{
    public Sprite sprite;
    public Arms arms;
}

