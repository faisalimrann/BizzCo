﻿using System.Collections;
using UnityEngine;
using System;
using GameData;

public abstract class Warrior : MonoBehaviour
{
    [SerializeField] protected string prefabName;
    public Transform DamagePoint;

	public int Health;
	public int MaxHealth;
	public IMoveInputManager inputManager;
    public IMoveManager moveManager;
    public IShotManager shotManager;
    public IHealthManager healthManager;
    private Coroutine shotCoroutine;

	protected bool isInvincible = false;
	[SerializeField]
	protected HealthBarManager healthBar;

	[SerializeField]
	protected VFXManager vFXManager;

	[SerializeField]
	protected AnimationDataAndContorller animation;

	[SerializeField]
	bool isPlayer;
	protected bool isDead;

	public virtual void Init()
    {
        InitManagers();
    }

    protected virtual void InitManagers()
    {
        moveManager = gameObject.GetComponent<IMoveManager>();
        shotManager = gameObject.GetComponent<IShotManager>();
        healthManager = gameObject.GetComponent<IHealthManager>();
		healthBar.UpdateHealthBar(1f);
    }

    public void ResetPositionOnMap()
    {
        transform.position = LevelData.ToPositionOnMap(transform.position);
        transform.rotation = LevelData.ToRotationOnMap(transform.rotation);
        moveManager.SetPosition(transform.position, transform.rotation);
    }

    public void StartShot(float shotFrequency)
    {
        if (shotCoroutine != null)
        {
            StopCoroutine(shotCoroutine);
        }
        shotCoroutine = StartCoroutine(RepeatShot(shotFrequency));
    }

    public virtual void StopShot()
    {
        if (shotCoroutine != null)
        {
            StopCoroutine(shotCoroutine);
        }
    }

    IEnumerator RepeatShot(float shotFrequency)
    {
        WaitForSeconds wait = new WaitForSeconds(shotFrequency);
        while (true)
        {
            yield return wait;
			if(!isDead)
			    shotManager.OnShot();
        }
    }

	public void TakeDamage( int damage, bool DamageFromThorn = false )
	{
		if ( isInvincible )
		{
			return;
		}
		string dam = "-"+ damage.ToString();
		if ( Health <= 0 )
		{
			dam = "";
		}
		Health -= damage;
		if ( Health <= 0 )
		{
			Health = 0;
		}
		if ( !isPlayer )
		{
			healthBar.UpdateHealthBar((Health * 1f) / MaxHealth, dam);
		}
		else
		{
			healthBar.UpdateHealthBar((Health * 1f) / MaxHealth, dam, Health.ToString());
		}
		if ( vFXManager != null && !isDead)
		{
			if ( DamageFromThorn )
			{
				vFXManager.PlayVFX(VFX.DamageTakenFromThorn);
			}
			else
			{
				vFXManager.PlayVFX(VFX.DamageTaken);
			}
			
		}
		if ( !isPlayer )
		{
			GetComponent<Rigidbody>().AddForce(this.transform.forward.normalized * -0.25f,ForceMode.Impulse);
		}
		if ( Health <= 0 && !isDead )
		{
			isDead = true;
			GetComponent<Rigidbody>().velocity = Vector3.zero;

			if ( animation != null )
			{
				if ( isPlayer )
				{
					SoundManager.Stop("Run");
					if ( PlayerData.Instance.CurrentHero.HeroKeyID == HeroID.BizzCo)
						SoundManager.Play("BizzCo Death");
					else
						SoundManager.Play("Space Robot Death");
					animation.SetIsDeadForPlayer(true);
				}
				animation.PlayAnimation(AnimationData.Die);
				if ( vFXManager != null && isPlayer )
				{
					vFXManager.PlayVFX(VFX.DamageTaken);
					moveManager.Disable();
					StopShot();
					StartCoroutine("DeathCoroutine");
				}
				else if ( vFXManager != null && !isPlayer )
				{
					inputManager.Disable();
					StopShot();
					vFXManager.PlayVFX(VFX.Death);
					Destroy(this.gameObject, 1.5f);
					this.healthManager.Destroy();
				}
				else
				{
					//Destroy(this.gameObject, 1f);
					this.healthManager.Destroy();
				}
			}
		}

		
	}
	IEnumerator DeathCoroutine()
	{
		yield return new WaitForSeconds(2);
		this.vFXManager.PlayVFX(VFX.Death);
		yield return new WaitForSeconds(3);
		//Destroy(this.gameObject, 1f);
		this.healthManager.Destroy();
	}
}


[Serializable]
public class WarriorParameters //Warrior Parameters is not ScriptableObject, you can set individual Parameters for same type Warrior  
{
    public string name;
    public MoveParameters moveParameters;
    public ShotParameters shotParameters;
    public HealthParameters healthParameters;
}

[Serializable]
public struct MoveParameters
{
    public float movementSpeed;
    [HideInInspector] public Vector3 position;
    [HideInInspector] public Quaternion rotation;

    public void SetPosition(Transform transform)
    {
        position = transform.position;
        rotation = transform.rotation;
    }
}
[Serializable]
public struct ShotParameters
{
    public float shotFrequency;
}

[Serializable]
public struct HealthParameters
{
    public float health;
    public float damage;
}



