﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultMoveManager : MonoBehaviour, IMoveManager
{
    [SerializeField] private MoveParameters parameters;
    public MoveParameters Parameters { get => parameters; set => parameters = value; }

    private Rigidbody body;
    private bool inMove;

    public event Action OnStartMove;
    public event Action OnStopMove;

    bool isDead = false;

    AnimationDataAndContorller animationController;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        animationController = GetComponent<AnimationDataAndContorller>();
    }

    public virtual void Move(Vector3 direction)
    {
        if(!body || isDead)
        {
            return;
        }
        body.velocity = parameters.movementSpeed * direction;
        
        if (direction != Vector3.zero)
        {
            //Quaternion rotation = Quaternion.identity;
            //rotation.SetLookRotation(direction);
            //body.rotation = Quaternion.Lerp(body.rotation, rotation, 10f * Time.fixedDeltaTime);
            //transform.forward = Vector3.Lerp(transform.forward, direction, 10f * Time.fixedDeltaTime);
            transform.forward = direction;

          
           //     StartCoroutine("PlayFootStepSound");
          //  SoundManager.Play("Running", 0.5f);
            if (!inMove)
            {
                inMove = true;
                if (animationController != null)
					animationController.PlayAnimation(AnimationData.Forward);
			    OnStartMove?.Invoke();
			}
        }
        else if (inMove)
        {
            inMove = false;
			if(animationController != null)
				animationController.PlayAnimation(AnimationData.Idle);
        //    SoundManager.Stop("Running");
            OnStopMove?.Invoke();
		}
	}

    public void SetPosition(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
    }

	public void Disable()
	{
		isDead = true;
	}

	public void Enable()
	{
		isDead = false;
	}
}
