﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultHealthManager : MonoBehaviour, IHealthManager
{
    [SerializeField] private HealthParameters parameters;
    public HealthParameters Parameters { get => parameters; set => parameters = value; }

    public event Action<HealthParameters> OnDamage;
    public event Action OnDestroy;

    public virtual void DecreaseHealth(float value)
    {
        parameters.health -= value;
    }

    public virtual void Damage()
    {
        OnDamage?.Invoke(parameters);
    }

    public virtual void Destroy()
    {
        OnDestroy?.Invoke();
        //Destroy(gameObject);
    }
}
