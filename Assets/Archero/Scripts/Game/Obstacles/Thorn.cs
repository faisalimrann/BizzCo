﻿using System.Collections;
using UnityEngine;

public class Thorn : Obstacle
{
	//Todo
	public int Damage;
	bool DamageGiven = false;
	PlayerManager playerCollider = null;
	private void OnTriggerEnter( UnityEngine.Collider other )
	{
		if ( other.tag == "Player" )
		{
			DamageGiven = true;
			playerCollider = other.GetComponent<PlayerManager>();
			playerCollider.TakeDamage(Damage, true);
			StartCoroutine("GiveDamage");
		}
	}
	private void OnTriggerExit( UnityEngine.Collider other )
	{
		if ( other.tag == "Player" )
		{
			StopAllCoroutines();
			DamageGiven = false;
			playerCollider = null;
		}
	}

	IEnumerator GiveDamage()
	{
		while ( DamageGiven )
		{
			yield return new WaitForSeconds(1f);
			if ( DamageGiven  && playerCollider != null)
			{
				playerCollider.TakeDamage(Damage, true);
			}
		}
	}
}
