﻿using UnityEngine;

public abstract class Obstacle : MonoBehaviour
{
     
}

[System.Serializable]
public class ObstacleData
{
    [HideInInspector] public string type;
    [HideInInspector] public Vector3 position;
    [HideInInspector] public Quaternion rotation;

    public ObstacleData(Obstacle obstacle)
    {
        type = obstacle.GetType().ToString();
        obstacle.transform.position = LevelData.ToPositionOnMap(obstacle.transform.position);
        obstacle.transform.rotation = LevelData.ToRotationOnMap(obstacle.transform.rotation);
        position = obstacle.transform.position;
        rotation = obstacle.transform.rotation; 
    }
}
