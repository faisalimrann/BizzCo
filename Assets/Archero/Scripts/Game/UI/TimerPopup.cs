﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerPopup : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text timer;
    [SerializeField] private int waitTime = 3;
    private Coroutine waitAndClose;

    public void Open(Action complete)
    {
        gameObject.SetActive(true);
        SoundManager.Play("321");
        if (waitAndClose != null)
        {
            StopCoroutine(waitAndClose);
        }
        waitAndClose = StartCoroutine(WaitAndClose(complete));
    }
    IEnumerator WaitAndClose(Action complete)
    {
        timer.text = (waitTime -1)+ "";
        int time = waitTime;
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(1f);
        yield return new WaitForSecondsRealtime(0.1f);
        while (time >1)
        {
            timer.text = (time -1)+ "";
            time -= 1;
            yield return wait;
        }
		timer.text = "GO";
         yield return wait;
	   gameObject.SetActive(false);
        complete?.Invoke();
    }
}
