﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{
    public event Action OnPause;
    public event Action OnHome;
    public event Action OnPlay;

    [SerializeField] private PausePopup pausePopup;
    [SerializeField] private TimerPopup timerPopup;
    [SerializeField] private ArmsPopup armsPopup;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Text achievements;

    private void Awake()
    {
       pauseButton.onClick.AddListener(Pause);
        armsPopup.Close();
    }

    private void Pause()
    {
        SoundManager.Play("Popup");
        //pausePopup.Open(OnHome, OnPlay);
        //OnPause?.Invoke();
    }

    public void OpenTimerPopup(Action complete)
    {
        timerPopup.Open(complete);
    }

    public void OpenArmsPopup(List<AbilityPopupPopulator> abilities, Action<int> callback)
    {
        armsPopup.Open(abilities, callback);
    }

	public void OpenArmsPopupForAngel( List<AbilityPopupPopulator> abilities, Action<int> callback )
	{
		armsPopup.OpenForAngel(abilities, callback);
	}

	public void SetAchievements(int value)
    {
        achievements.text = value + "";
    }
}
