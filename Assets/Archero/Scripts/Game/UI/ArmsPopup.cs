﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ArmsPopup : MonoBehaviour
{
    private Action<int> callback;
	int clickedIndex = -1;
    [SerializeField] private GameObject template;
	[SerializeField] private Transform contentTransform;
	[SerializeField] DOTweenAnimation animation;
	[SerializeField] Text headerText;
	public void Open( List<AbilityPopupPopulator> sprites, Action<int> callback)
    {
        this.callback = callback;
		if( !headerText.text.Equals("Select your starting ability"))
			headerText.text = "Select a new ability!";

		for (int i = 0; i < sprites.Count; i++)
        {
            int index = i;
			GameObject temp = Instantiate(template, contentTransform );
			temp.gameObject.SetActive(true);
			Button button = temp.transform.GetChild(0).GetComponent<Button>();
			button.image.sprite = sprites[i].sprite.sprite;
			Text text = temp.transform.GetChild(1).GetComponent< Text>();
			text.text = sprites[i].name.ToString();
			button.onClick.AddListener(() =>
            {
				SoundManager.Play("Default Button");
				this.clickedIndex = index;
				//callback(index);
				PlayAnimation();
				// Close();
            });
		}
        gameObject.SetActive(true);
    }


	public void OpenForAngel( List<AbilityPopupPopulator> sprites, Action<int> callback )
	{
		this.callback = callback;
		headerText.text = "You have found an Angel";
		for ( int i = 0; i < sprites.Count; i++ )
		{
			int index = i;
			GameObject temp = Instantiate(template, contentTransform);
			Button button = temp.transform.GetChild(0).GetComponent<Button>();
			button.image.sprite = sprites[i].sprite.sprite;
			Text text = temp.transform.GetChild(1).GetComponent<Text>();
			text.text = sprites[i].name.ToString();
			button.onClick.AddListener(() =>
			{
				SoundManager.Play("Heal+Boost");
				this.clickedIndex = index;
				//callback(index);
				PlayAnimation();
				//Close();
			});
			temp.gameObject.SetActive(true);
		}
		gameObject.SetActive(true);
	}

	void PlayAnimation()
	{
		animation.DORestart();
	}

	public void Close()
    {
		Debug.Log("clicked index: "+ clickedIndex);
		
		callback?.Invoke(clickedIndex);
		//if ( clickedIndex > -1 )
		//	SoundManager.Play("321");
		animation.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
		for (int i = 0; i < contentTransform.childCount; i++)
        {
			Destroy(contentTransform.GetChild(i).gameObject);
        }
        gameObject.SetActive(false);
    }
}
