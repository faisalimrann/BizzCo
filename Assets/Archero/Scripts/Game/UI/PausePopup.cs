﻿using System;
using DG.Tweening;
using GameData;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PausePopup : MonoBehaviour
{
    [SerializeField] private Button homeButton;
	[SerializeField] Slider BGM;
	[SerializeField] Slider Sound;
	[SerializeField] TMPro.TMP_Text BGMText;
	[SerializeField] TMPro.TMP_Text SoundText;
	[SerializeField] Button falseCancelButton; 
	[SerializeField] Button ActualCancelButton;
	[SerializeField] Button PopupCancel;
	[SerializeField] DOTweenAnimation animation;

	// Start is called before the first frame update
	void Start()
	{
		falseCancelButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			animation.DORestart();
		});
		PopupCancel.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			animation.DORestart();
		});

		BGM.onValueChanged.AddListener(( value ) =>
		{
			BGMText.text = (value * 100).ToString("f1");
		});
		Sound.onValueChanged.AddListener(( value ) =>
		{
			SoundText.text = (value * 100).ToString("f1");
		});
		homeButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			LoaderScreen.Instance.SetStatus(true);
			SceneManager.sceneLoaded += SceneManager_sceneLoaded;
			SceneManager.LoadScene("MainMenu");
		});
	}

	private void SceneManager_sceneLoaded( Scene arg0, LoadSceneMode arg1 )
	{
		PlayerData.Instance.GetGameData();
	}

	public void PLaySound( string name )
	{
		SoundManager.Play(name);
	}


	public void AniamtionEnd()
	{
		Debug.Log("AnimationEnded");
		ActualCancelButton.onClick?.Invoke();
		animation.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
	}

	private void OnEnable()
	{
		SoundManager.Play("Popup", 0.2f);
		Time.timeScale = 0;
	}

	private void OnDisable()
	{
		Time.timeScale = 1;
	}

}
