﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DefaultShotManager : MonoBehaviour, IShotManager
{
    [SerializeField] protected ShotParameters parameters;
    [SerializeField] private Arms arms;
    protected Arms newArms;
    [SerializeField] protected Transform shotPoint;
    public ShotParameters Parameters { get => parameters; set => parameters = value; }
	protected AnimationDataAndContorller animationDataAndContorller;
	protected Transform player;
	public int Damage;

	private void Awake()
    {
        if(newArms == null && arms != null)
        {
            newArms = Instantiate(arms, transform);
        }
		animationDataAndContorller = GetComponent<AnimationDataAndContorller>();
    }

    public void SetArms(Arms arms)
    {
        newArms = Instantiate(arms, transform);
    }

    public virtual void OnShot()
    {
        Transform target = FindTarget();
        if(!target || !gameObject.activeInHierarchy)
        {
            return;
        }
		Vector3 direction = ((target.position + new Vector3(0, 0.75f, 0)) - shotPoint.position).normalized;
		if ( player != null )
		{
			direction = (target.position - shotPoint.position).normalized;
		}
        transform.forward = Vector3.ProjectOnPlane(direction, Vector3.up).normalized;
        newArms?.Shot(shotPoint, direction, target, Damage);
		if( animationDataAndContorller != null)
			animationDataAndContorller.PlayAnimation(AnimationData.Attack);
    }

	public virtual void SetPlayer( Transform player )
	{
		this.player = player;
	}
	protected abstract Transform FindTarget();

}
