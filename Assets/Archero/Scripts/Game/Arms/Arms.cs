﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class Arms : MonoBehaviour
{
    public Action<Collision> OnBowHit;
    [SerializeField] protected float speedKoeficient = 1f;
    [SerializeField] protected Bow bow;
	[SerializeField] protected EnemyProjectile enemyProjectile;

    public abstract void Shot(Transform point, Vector3 direction, Transform playerTarget, int Damage = 0);
}