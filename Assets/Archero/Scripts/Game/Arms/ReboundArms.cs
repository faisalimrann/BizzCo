﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ReboundArms : Arms
{
    private Vector3 firstDirection;

	public override void Shot( Transform point, Vector3 direction, Transform playerTarget, int Damage )
	{
		firstDirection = direction;
        Bow currentBow = Instantiate(bow, point.position, point.rotation);
		currentBow.damage = Damage;
		currentBow.OnHit += (Collision collision) => CurrentBow_OnHit(currentBow, collision);
        currentBow.Shot(point.position, direction, speedKoeficient);
    }

    void CurrentBow_OnHit(Bow currentBow, Collision collision)
    {
        OnBowHit?.Invoke(collision);
        if (currentBow.HitCount >= 2)
        {
            Destroy(currentBow.gameObject);
        }
        else
        {
            ContactPoint contactPoint = collision.contacts[0];

            currentBow.Shot(currentBow.transform.position + 0.1f * contactPoint.normal,
            (firstDirection - 2f * Vector3.Project(firstDirection, -contactPoint.normal)).normalized, speedKoeficient);
        }
    }
}
