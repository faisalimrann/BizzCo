﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TripleArms : Arms
{
	public override void Shot( Transform point, Vector3 direction, Transform playerTarget, int Damage )
	{
		ShotBow(point, direction, Damage);
        ShotBow(point, (direction + 0.5f * point.right).normalized, Damage);
        ShotBow(point, (direction - 0.5f * point.right).normalized, Damage);
    }

    private void ShotBow(Transform point, Vector3 direction, int Damage)
    {
        Bow currentBow = Instantiate(bow, point.position, point.rotation);
        SoundManager.Play(bow.ToString());
		currentBow.damage = Damage;
		currentBow.OnHit += (Collision collision) => CurrentBow_OnHit(currentBow, collision);
        currentBow.Shot(point.position, direction, speedKoeficient);
    }

    void CurrentBow_OnHit(Bow currentBow, Collision collision)
    {
        OnBowHit?.Invoke(collision);
        Destroy(currentBow.gameObject);
    }
}
