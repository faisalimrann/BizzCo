﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileArms: Arms
{
	public override void Shot( Transform point, Vector3 direction, Transform playerTarget, int Damage )
	{
		ShotBow(point,direction, playerTarget, Damage);
	}
	
	private void ShotBow( Transform point, Vector3 direction, Transform playerTarget, int Damage )
	{
		EnemyProjectile currentBow = Instantiate(enemyProjectile, point.position, point.rotation);
		SoundManager.Play(enemyProjectile.ToString());
		currentBow.transform.forward = direction;
		currentBow.TargetTransform = playerTarget;
		currentBow.damage = Damage;
		currentBow.gameObject.SetActive(true);

	}

	void CurrentBow_OnHit( Bow currentBow, Collision collision )
	{
		OnBowHit?.Invoke(collision);
		Destroy(currentBow.gameObject);
	}
}

