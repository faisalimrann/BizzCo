﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SimpleArms : Arms
{
    public override void Shot(Transform point, Vector3 direction, Transform playerTarget, int Damage)
    {
        Bow currentBow = Instantiate(bow, point.position, point.rotation);
        Debug.Log(bow.ToString());
        SoundManager.Play(bow.ToString());
		currentBow.damage = Damage; 
        currentBow.OnHit += (Collision collision) => CurrentBow_OnHit(currentBow, collision);
        currentBow.Shot(point.position, direction, speedKoeficient);
    }

    void CurrentBow_OnHit(Bow currentBow, Collision collision)
    {
        OnBowHit?.Invoke(collision);
        Destroy(currentBow.gameObject);
    }
}
