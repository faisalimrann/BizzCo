﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DoubleArms : Arms
{
	public override void Shot( Transform point, Vector3 direction, Transform playerTarget, int Damage )
	{
		StartCoroutine(DoubleShot(point, direction, Damage));
    }

    private IEnumerator DoubleShot(Transform point, Vector3 direction, int Damage)
    {
        ShotBow(point, direction, Damage);
        yield return new WaitForSeconds(0.1f);
        ShotBow(point, direction, Damage);
    }

    private void ShotBow(Transform point, Vector3 direction, int Damage)
    {
        Bow currentBow = Instantiate(bow, point.position, point.rotation);
		currentBow.damage = Damage;
		currentBow.OnHit += (Collision collision) => CurrentBow_OnHit(currentBow, collision);
        currentBow.Shot(point.position, direction, speedKoeficient);
    }

    void CurrentBow_OnHit(Bow currentBow, Collision collision)
    {
        OnBowHit?.Invoke(collision);
        Destroy(currentBow.gameObject);
    }
}
