﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Camera))]
public class MapCameraManager : MonoBehaviour
{
    [SerializeField] private float standartSize = 3.333f;
    [SerializeField] private int standartWidth = 1536;
    [SerializeField] private int standartHeight = 2024;
    [SerializeField] private float trackingSpeed = 5f;
    [SerializeField] private MapManager mapManager;
    [SerializeField] private Transform player;
	[SerializeField] Transform spaceRobot;

    private int width;
    private int height;
    private float angleX;
    private float positionY;
    private float minPositionZ;
    private float maxPositionZ;
    private Vector3 pivotPosition;
    private Camera mapCamera;

    private void Awake()
		{
        mapCamera = GetComponent<Camera>();
		//if ( GameData.PlayerData.Instance.CurrentHero.HeroKeyID == GameData.HeroID.SpaceRobot )
		//{
		//	player = spaceRobot;
		//}

		RessetCamera();
    }

    void Update()
    {
        if (width != Screen.width || height != Screen.height
         || Mathf.Abs(angleX - transform.localRotation.eulerAngles.x) > Mathf.Epsilon
            || Mathf.Abs(positionY - transform.position.y) > Mathf.Epsilon)
        {
            RessetCamera();
        }

    }
    private void FixedUpdate()
    {
        Move(Time.fixedDeltaTime);
    }
    private void RessetCamera()
    {
        float sinValue = Vector3.Dot(transform.up, Vector3.up);
        if (sinValue < 0f)
        {
            transform.localRotation = Quaternion.Euler(-1f, 0f, 0f);
        }
        else if (sinValue > 0.5f)
        {
            transform.localRotation = Quaternion.Euler(-29f, 0f, 0f);
        }
        width = Screen.width;
        height = Screen.height;
        angleX = transform.localRotation.eulerAngles.x;
        positionY = transform.position.y;

        mapCamera.orthographicSize = standartSize * ((float)standartWidth / (float)standartHeight) / ((float)width / (float)height);
        float angleFactor = transform.position.y * Mathf.Sin(sinValue);
        transform.localPosition = new Vector3(0.0f, mapCamera.orthographicSize - angleFactor, 0.0f);
        minPositionZ = -mapCamera.orthographicSize/2.5f;
		maxPositionZ = mapCamera.orthographicSize /1f;
		pivotPosition = transform.parent.position;
        UpdatePositionZ();
    }
    private void UpdatePositionZ()
    {
        if (pivotPosition.z > maxPositionZ)
        {
            pivotPosition = new Vector3(0f, transform.parent.position.y, maxPositionZ);
        }
        else if (pivotPosition.z < minPositionZ)
        {
            pivotPosition = new Vector3(0f, transform.parent.position.y, minPositionZ);
        }
        transform.parent.position = pivotPosition;
    }
    private void Move(float deltaTime)
    {
        if(!player)
        {
            return;
        }
        pivotPosition = Vector3.Lerp(pivotPosition, new Vector3(pivotPosition.x, pivotPosition.y, player.position.z - mapCamera.orthographicSize), trackingSpeed * deltaTime);
        UpdatePositionZ();
    }

	private void OnDisable()
	{
		Debug.LogError(Environment.StackTrace);
	}
}
