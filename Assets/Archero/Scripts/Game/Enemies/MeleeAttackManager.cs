﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackManager : DefaultShotManager
{

    protected override Transform FindTarget()
    {
        return player;
    }

	public override void OnShot()
	{
		Transform target = FindTarget();
		if ( !target || !gameObject.activeInHierarchy )
		{
			return;
		}
		SoundManager.Play("Alien Horror");
		Vector3 direction = (target.position - shotPoint.position).normalized;
		transform.forward = Vector3.ProjectOnPlane(direction, Vector3.up).normalized;
		//newArms?.Shot(shotPoint, direction, target);
		if ( animationDataAndContorller != null && direction.magnitude<=1f)
			animationDataAndContorller.PlayAnimation(AnimationData.Attack);
	}
}
