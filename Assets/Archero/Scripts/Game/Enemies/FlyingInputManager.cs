﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingInputManager : EnemyInputManager
{
	public override Vector3 CalculateDirection()
	{
		Vector3 direction = default;
		int randomDirection = UnityEngine.Random.Range(0, 400);
		if ( randomDirection % 4 == 0 )
		{
			direction = Vector3.forward;
		}
		else if ( randomDirection % 4 == 1 )
		{
			direction = Vector3.back;
		}
		else if ( randomDirection % 4 == 2 )
		{
			direction = Vector3.left;
		}
		else
		{
			direction = Vector3.right;
		}
		//if ( Physics.Raycast(transform.position, direction, out RaycastHit hit, Parameters.movementDisplacement, mask) )
		//{
		//	if ( !hasOtherDirection || Physics.Raycast(transform.position, otherDirection, Parameters.movementDisplacement, mask) )
		//	{
		//		otherDirection = Mathf.Abs(Vector3.Dot(hit.normal, Vector3.right)) > 0.5f ? Vector3.forward : Vector3.right;
		//		if ( Vector3.Dot(target.position - transform.position, otherDirection) > 0f &&
		//		!Physics.Raycast(transform.position, otherDirection, Parameters.movementDisplacement, mask) )
		//		{
		//			direction = otherDirection;
		//		}
		//		else
		//		{
		//			direction = -otherDirection;
		//		}
		//		hasOtherDirection = true;
		//	}
		//	else
		//	{
		//		direction = otherDirection;
		//	}
		//}
		//else
		//{
		//	hasOtherDirection = false;
		//}

		return direction;
	}
}
