﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class WalkingInputManager : EnemyInputManager
{
    [SerializeField] private LayerMask mask;
    bool hasOtherDirection;
    private Vector3 otherDirection;

	[SerializeField]
	NavMeshAgent navMeshAgent;

	[SerializeField]
	NavMeshPath path;

	//Todo
	public override Vector3 CalculateDirection()
    {
		if ( navMeshAgent == null )
		{
			navMeshAgent = GetComponent<NavMeshAgent>();
			path = new NavMeshPath();
		}
        if(!target)
        {
            return Vector3.zero;
        }

		navMeshAgent.CalculatePath(target.position, path);

		//while ( path.status != NavMeshPathStatus.PathComplete)
		//{
		//}
		int index = 0;
		if ( path.corners.Length > 1 )
		{
			index = 1;
		}
		Vector3 direction = Vector3.ProjectOnPlane(path.corners[index] - this.transform.position, Vector3.up).normalized;
		if ( Physics.Raycast(transform.position, direction, out RaycastHit hit, Parameters.movementDisplacement, mask) )
		{
			if ( !hasOtherDirection || Physics.Raycast(transform.position, otherDirection, Parameters.movementDisplacement, mask) )
			{
				otherDirection = Mathf.Abs(Vector3.Dot(hit.normal, Vector3.right)) > 0.5f ? Vector3.forward : Vector3.right;
				if ( Vector3.Dot(path.corners[index] - transform.position, otherDirection) > 0f &&
				!Physics.Raycast(transform.position, otherDirection, Parameters.movementDisplacement, mask) )
				{
					direction = otherDirection;
				}
				else
				{
					direction = -otherDirection;
				}
				hasOtherDirection = true;
			}
			else
			{
				direction = otherDirection;
			}
		}
		else
		{
			hasOtherDirection = false;
		}

		return direction;
	}
}
