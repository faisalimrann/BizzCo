﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyInputManager : MonoBehaviour, IMoveInputManager
{
    public event Action<Vector3> OnControl;
    private float moveTime;
    protected Transform target;
    [SerializeField] protected EnemyInputParameters parameters;

    public EnemyInputParameters Parameters { get => parameters; set => parameters = value; }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void SetMoveTime(float moveTime)
    {
        this.moveTime = moveTime;
    }

	public void Disable()
	{
		this.enabled = false;
	}

	protected virtual IEnumerator Start()
    {
        WaitForSeconds sleepTime = new WaitForSeconds(parameters.sleepTime);
        WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
        while (true)
        {
            if (OnControl != null)
            {
                OnControl(Vector3.zero);
                yield return sleepTime;
                float time = 0f;
                Vector3 direction = CalculateDirection();

                while (time < moveTime)
                {
                    time += Time.fixedDeltaTime;
                    yield return waitForFixedUpdate;
                    OnControl(direction);
                }
            }
            else
            {
                yield return null;
            }
        }
    }
   
    public abstract Vector3 CalculateDirection();
}

[Serializable]
public struct EnemyInputParameters
{
    public float movementDisplacement;
    public float sleepTime;
}

