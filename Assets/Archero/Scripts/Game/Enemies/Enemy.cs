﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameData;

[RequireComponent(typeof(EnemyInputManager))]
[RequireComponent(typeof(DefaultShotManager))]
public class Enemy : Warrior
{
    private EnemyInputManager enemyInputManager;
    private DefaultShotManager enemyShotManager;
	float shotFrequency;
	int Damage;
	public EnemyID enemyID;
    public override void Init()
    {
        base.Init();
		inputManager.OnControl += moveManager.Move;
		moveManager.OnStartMove += StopShot;
		moveManager.OnStopMove += () => { StartShot(shotFrequency); };
		StartShot(shotFrequency);
	}

    protected override void InitManagers()
    {
        inputManager = enemyInputManager = gameObject.GetComponent<EnemyInputManager>();
        enemyShotManager = gameObject.GetComponent<DefaultShotManager>();
		enemyShotManager.Damage = Damage;

		base.InitManagers();

        enemyInputManager.SetMoveTime(enemyInputManager.Parameters.movementDisplacement / moveManager.Parameters.movementSpeed);
    }

    public void SetTarget(Transform target)
    {
        enemyInputManager.SetTarget(target);
        enemyShotManager.SetPlayer(target);
    }


    public EnemyParameters GetParameters()
    {
        InitManagers();
        MoveParameters moveParameters = moveManager.Parameters;
        moveParameters.SetPosition(transform);
        moveManager.Parameters = moveParameters;
        return new EnemyParameters
        {
            name = prefabName,
            enemyInputParameters = enemyInputManager.Parameters,
            moveParameters = moveManager.Parameters,
            shotParameters = shotManager.Parameters,
            healthParameters = healthManager.Parameters
        };
    }
    public void SetParameters(EnemyParameters parameters)
    {
        gameObject.name = parameters.name;
        moveManager.SetPosition(parameters.moveParameters.position, parameters.moveParameters.rotation);
        enemyInputManager.Parameters = parameters.enemyInputParameters;
        moveManager.Parameters = parameters.moveParameters;
        shotManager.Parameters = parameters.shotParameters;
        healthManager.Parameters = parameters.healthParameters;
    }

	public void SetData(int health, int _shotFrequency, int damage, EnemyID id)
	{
		Health = MaxHealth = health;
		shotFrequency = 1f / (_shotFrequency / 100f);
		Damage = damage;
		enemyID = id;
		healthBar.UpdateHealthBar(1f);
	}
}

[Serializable]
public class EnemyParameters : WarriorParameters //Enemy Parameters is not ScriptableObject, you can set individual Parameters for same type Enemy  
{
    public EnemyInputParameters enemyInputParameters;
}

