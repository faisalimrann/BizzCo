﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Threading;

public class MapManager : MonoBehaviour
{
    public Vector3 AreaSize => 10f * area.lossyScale;
    [SerializeField] private Transform area;
    [SerializeField] private Transform dataContainer;
    [SerializeField] private GameObject door;
    [SerializeField] private GameObject doorVFX;
    //[SerializeField] NavMeshSurface navMeshSurface;
    public List<Obstacle> Obstacles { get; private set; }
    public List<Enemy> Enemies { get; private set; }
	Vector3 _Target;
	CancellationTokenSource cts;
    private void Awake()
    {
        if (SceneManager.GetActiveScene().name.Contains("Level_"))
        {
            Debug.LogError("Please do not play scenes named Level_");
            return;
        }
	}
    public void Init(Transform enemyTarget)
    {
        ClearPreviousLevel();
        LevelsManager.Manager.LoadLevel(out List<Obstacle> obstacles, out List<Enemy> enemies);
        Obstacles = obstacles;
        Enemies = enemies;
        CreateMap(Obstacles, Enemies, enemyTarget);
		CloseDoor();
		 _Target = door.transform.position + (door.transform.up * 3.77f);
	}
	private void CreateMap(List<Obstacle> obstacles, List<Enemy> enemies, Transform enemyTarget)
    {
        Debug.Log("CreateMap");
        door.SetActive(true);
        foreach (var obstacle in obstacles)
        {
            obstacle.transform.SetParent(dataContainer);
        }
        foreach (var enemy in enemies)
        {
            enemy.SetTarget(enemyTarget);
            enemy.transform.SetParent(dataContainer);
        }
    }

    void ClearPreviousLevel()
    {
        if (Obstacles != null)
        {
            for (int i = 0; i < Obstacles.Count; i++)
            {
                if (Obstacles[i] != null)
                    Destroy(Obstacles[i].gameObject);
            }
        }
        if (Enemies != null)
        {
            for (int i = 0; i < Enemies.Count; i++)
            {
                if (Enemies[i] != null)
                    Destroy(Enemies[i].gameObject);
            }
        }
    }

   public async void DestroyDoor()
    {
        Debug.Log("DestroyDoor");
		cts = new CancellationTokenSource();
		//door.SetActive(false);
        doorVFX.SetActive(true);
		await Task.Delay(2000);
        await OpenDoor(cts.Token);
    }

    async Task OpenDoor(CancellationToken token)
    {
		Vector3 Target = _Target;
        while ( !token.IsCancellationRequested && (door.transform.position - Target).magnitude > 0.1f)
        {
            await Task.Yield();
            door.transform.position = Vector3.Lerp(door.transform.position, Target, Time.deltaTime * 3f);
        }
    }

	public  void CloseDoor()
	{
		if ( cts != null )
		{
			cts.Cancel();
		}
        doorVFX.SetActive(false);
		door.transform.position = new Vector3(door.transform.position.x, 1f, door.transform.position.z);
		Debug.LogError("ClosingDoor");

	}
}


