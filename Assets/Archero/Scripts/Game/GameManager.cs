﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using RoboArchero.Networking;
using System.Collections.Generic;
using GameData;
using System;

public class GameManager : MonoBehaviour
{
	[SerializeField] private GameUIManager m_UIManager;
	[SerializeField] private PlayerArmsManager m_PlayerArmsManager;
	[SerializeField] private MapManager m_MapManager;
	[SerializeField] private PlayerManager m_PlayerManager;
	[SerializeField] private PlayerManager SpaceRobotPlayerManager;
	[SerializeField] private EnemyHealthManager m_EnemyHealthManager;
	[SerializeField] private AchievementsManager m_AchievementsManager;
	private bool hasArms;
	[SerializeField] TMPro.TMP_Text StageText;
	[SerializeField] TMPro.TMP_Text XPText;
	[SerializeField] Image XPMeter;
	[SerializeField] VictoryPopupController victoryPopupController;
	[SerializeField] int MaxRevivesAllowed = 2;
	[SerializeField] SatoshiRewardManager rewardManager;
	[SerializeField] Text TopBarSatoshi;
	[SerializeField] Text TopBarBizzCoin;
	[SerializeField] TMPro.TMP_Text AbilityDescription;
	[SerializeField] GameObject bizzCoinPopup;


	List<GameObject> rewards = new List<GameObject>();
	int reviveCount = 0;
	int SatoshiRewardByKilling = 0;
	int _totalSatoshiCollected = 0;
	public int TotalSatoshiCollected { get { return _totalSatoshiCollected; } private set { } }
	int _totalEnergyCollected = 0;
	public int TotalEnergyCollected { get { return _totalEnergyCollected; } private set { } }
	int _totalBizzCoinCollected = 0;
	public int TotalBizzCoinCollected { get { return _totalBizzCoinCollected; } private set { } }
	[SerializeField] string[] LightmapsFolderNames;

	public delegate void AfterCollidedWithAngelEvent();
	public static event AfterCollidedWithAngelEvent onAfterCollidedWithAngelEvent;

	private void Start()
	{
		//if (LevelsManager.Manager.Level == 0)
		//{
		//    SceneManager.LoadScene("Menu");
		//    enabled = false;
		//    return;
		//}
		if ( PlayerData.Instance.CurrentHero.HeroKeyID == HeroID.SpaceRobot )
		{
			m_PlayerManager.gameObject.SetActive(false);
			m_PlayerManager = SpaceRobotPlayerManager;
		}
		else
		{
			SpaceRobotPlayerManager.gameObject.SetActive(false);
		}

		m_UIManager.SetAchievements(m_AchievementsManager.GetAchievements());
		m_UIManager.OnPause += UiManager_OnPause;
		m_UIManager.OnPlay += UiManager_OnPlay;
		m_UIManager.OnHome += UiManager_OnHome;

		m_MapManager.Init(m_PlayerManager.DamagePoint);
		m_EnemyHealthManager.OnEnemiesEnded += OnEnemiesFinished;
		m_EnemyHealthManager.OnEnemyDestroy += M_EnemyHealthManager_OnEnemyDestroy;
		m_EnemyHealthManager.Init(m_MapManager.Enemies);

		Time.timeScale = 0f;
		hasArms = false;
		//m_MapManager.CloseDoor();
		m_UIManager.OpenArmsPopup(m_PlayerArmsManager.Get3RandomSprites(), ( int id ) =>
		{
			hasArms = true;
			m_PlayerManager.Init();
			
			m_PlayerManager.AddAbility(m_PlayerArmsManager.GetArms(id));
			AbilityDescription.text = DataManager.Instance.GetAbilityData(m_PlayerArmsManager.GetArms(id).abilityID).AbilityName + ": " + DataManager.Instance.GetAbilityData(m_PlayerArmsManager.GetArms(id).abilityID).AbilityDescription;
			StartCoroutine("SetABilityDescriptionOff");
			m_PlayerManager.SetEnemyHealthManager(m_EnemyHealthManager);
			m_PlayerManager.healthManager.OnDestroy += GameIsEnede;
			m_PlayerManager.OnEnterDoor += OnLevelComplete;
			m_UIManager.OpenTimerPopup(() =>
			{
				Time.timeScale = 1f;
				//Debug.Log("Start Game");
			});
		});

		CollidedWithAngel.onCollidedWithAngelEvent += OnAngelCollission;
		LoadLightMaps();

	}
	void OnEnemiesFinished()
	{
		onDoorEntered?.Invoke();
		int stage = (int)LevelsManager.Manager.Level;
		if ( (stage + 1) % 5 != 0 )
		{
			m_MapManager.DestroyDoor();
		}
		else if ( (stage + 1) % 10 == 0 )
		{
			AdsManager.Instance.showRewardedAd(() =>
			{
				//LoaderScreen.Instance.SetStatus(true);
			}, () =>
			{
				AdMobManager.Instance.ShowAdmobRewardedAd(() =>
				{
					//LoaderScreen.Instance.SetStatus(true);
				},
				() =>
				{
				}, () =>
				{
				});
			}, () =>
			{

			});
			m_MapManager.DestroyDoor();
		}

		GameData.StageData FinishedStage = DataManager.Instance.GetStage(LevelsManager.Manager.Level);
		//GameData.PlayerData.Instance.Wallet.Satoshi.Earn(FinishedStage.SatoshiReward);
		XpEarned += FinishedStage.XpReward;
		if ( XpEarned > DataManager.Instance.TitleData().XpThreshold )
		{
			CurrentXPLevel++;
			XpEarned = 0;
			XPText.text = CurrentXPLevel.ToString();
			XPMeter.fillAmount = XpEarned / (float)DataManager.Instance.TitleData().XpThreshold;
			m_UIManager.OpenArmsPopup(m_PlayerArmsManager.Get3RandomSprites(), ( int id ) =>
			{
				hasArms = true;
				m_PlayerManager.AddAbility(m_PlayerArmsManager.GetArms(id));
				AbilityDescription.text = DataManager.Instance.GetAbilityData(m_PlayerArmsManager.GetArms(id).abilityID).AbilityName + ": " + DataManager.Instance.GetAbilityData(m_PlayerArmsManager.GetArms(id).abilityID).AbilityDescription;
				StartCoroutine("SetABilityDescriptionOff");
			});
		}
		XPMeter.fillAmount = XpEarned / (float)DataManager.Instance.TitleData().XpThreshold;
		_totalSatoshiCollected += SatoshiRewardByKilling;
		if ( _totalSatoshiCollected > 5000 && !isRewardCollected )
		{
			isRewardCollected = true;
			TopBarBizzCoin.text = "1";
			_totalBizzCoinCollected = 1;
			bizzCoinPopup.SetActive(true);
			_totalBizzCoinCollected = 1;
		}
		_totalSatoshiCollected += (int)DataManager.Instance.GetStage(LevelsManager.Manager.Level).SatoshiReward;
		TopBarSatoshi.text = _totalSatoshiCollected.ToString();
	}

	bool isRewardCollected = false;
	private void M_EnemyHealthManager_OnEnemyDestroy( Enemy enemy )
	{
		SoundManager.Play("Coin Dropping");
		rewards.Add(rewardManager.GetRandomCoins(enemy.transform.position));
		m_UIManager.SetAchievements(m_AchievementsManager.GetAchievements(enemy));
		SatoshiRewardByKilling += (int)DataManager.Instance.GetEnemy(enemy.enemyID).SatoshiReward;
	}

	uint XpEarned = 0;
	uint CurrentXPLevel = 1;
	public delegate void EnterDoor();
	public static event EnterDoor onDoorEntered;
	bool levelCompleted = false;
	public bool isRevived = false;
	void OnLevelComplete()
	{
		if ( levelCompleted )
		{
			return;
		}
		levelCompleted = true;

		Networking.UpdateSession((int)(LevelsManager.Manager.Level + 1),SatoshiRewardByKilling, reviveCount,( sM ) =>
		{
			SatoshiRewardByKilling = 0;
			_totalEnergyCollected += (int)DataManager.Instance.GetStage(LevelsManager.Manager.Level).EnergyReward;
			rewards.ForEach(( obj ) =>
			{
				DestroyImmediate(obj);
			});
			if ( LevelsManager.Manager.Level + 1 < DataManager.Instance.TotalStages(1) )
			{
				LevelsManager.Manager.SetLevel(LevelsManager.Manager.Level + 1);
				StageText.text = (LevelsManager.Manager.GetLevel() + 1).ToString();
				//m_MapManager.CloseDoor();
				//SceneManager.LoadScene("BattleField");
				m_MapManager.Init(m_PlayerManager.DamagePoint);
				m_EnemyHealthManager.Init(m_MapManager.Enemies);
				if ( (int)LevelsManager.Manager.Level % 10 == 0 )
				{
					LoadLightMaps();
				}
			}
			else
			{
				SoundManager.Play("Victory");
				victoryPopupController.IsVictory = true;
				victoryPopupController.gameObject.SetActive(true);
				// UiManager_OnHome();
			}
			levelCompleted = false;
		}, ( status ) =>
		{
			Debug.LogError(status);
			levelCompleted = false;
		});
	}
	int revivesUsed = 0;
	private void GameIsEnede()
	{
		//Debug.Log("GameEnded");
		//StartCoroutine(WaitAndLoadMenu());
		if ( revivesUsed < DataManager.Instance.TitleData().RevivalCount )
		{
			revivesUsed++;
			ServiceLocator.Get<ScreenMgr.ScreenManager>().Show("Revive");
		}
		else
		{
			EndGame();
		}
    }

	public void EndGame()
	{
		SoundManager.Play("Game Over");
		if ( _totalSatoshiCollected > 5000 )
		{
			_totalSatoshiCollected = 5000;
			_totalBizzCoinCollected = 1;
		}
		victoryPopupController.IsVictory = false;
		victoryPopupController.gameObject.SetActive(true);
	}

	void LoadLightMaps()
	{
			var lightmaps = new LightmapData[1];
			for ( int i = 0; i < 1; i++ )
			{
				var lightmapData = new LightmapData();
			
				lightmapData.lightmapDir = Resources.Load(LightmapsFolderNames[(int)LevelsManager.Manager.Level / 10] + "/Lightmap-"+i+"_comp_dir") as Texture2D;
				lightmapData.lightmapColor = Resources.Load(LightmapsFolderNames[(int)LevelsManager.Manager.Level / 10] + "/Lightmap-" + i + "_comp_light") as Texture2D;
				lightmaps[i] = lightmapData;
			}
			LightmapSettings.lightmaps = lightmaps;
	}


    IEnumerator WaitAndLoadMenu()
    {
        yield return new WaitForSeconds(1.1f);
    }


    private void OnApplicationPause(bool pause)
    {
        //TriggerTimer(!pause);
    }
    private void OnApplicationFocus(bool focus)
    {
        //TriggerTimer(focus);
    }

    private void TriggerTimer(bool isOn)
    {
#if !UNITY_EDITOR
        if(!isOn)
        {
            Time.timeScale = 0f;
            Debug.Log("Stop Game");
        }
        else if (hasArms)
        {
            m_UIManager.OpenTimerPopup(() =>
            {
                Time.timeScale = 1f;
            });
        }
#endif
    }

    void UiManager_OnHome()
    {
        Time.timeScale = 1f;
		SoundManager.StopAllSounds();
        SceneManager.LoadScene("MainMenu");
    }


    void UiManager_OnPlay()
    {
        //m_UIManager.OpenTimerPopup(() =>
        //{
        //    Time.timeScale = 1f;
        //});
    }

    void UiManager_OnPause()
    {
       // Time.timeScale = 0f;
    }
	void OnAngelCollission()
	{
		Debug.Log("angel collision");
		m_UIManager.OpenArmsPopupForAngel(m_PlayerArmsManager.GetSpritesForAngel(), (int id ) =>
		{
			hasArms = true;
			m_PlayerManager.AddAbility(m_PlayerArmsManager.GetArms(id));
			AbilityDescription.text = DataManager.Instance.GetAbilityData(m_PlayerArmsManager.GetArms(id).abilityID).AbilityName + ": " + DataManager.Instance.GetAbilityData(m_PlayerArmsManager.GetArms(id).abilityID).AbilityDescription;
			StartCoroutine("SetABilityDescriptionOff");
			m_MapManager.DestroyDoor();
			onAfterCollidedWithAngelEvent?.Invoke();
		});
	}

	public void ReviveUsingSatoshi(Action success)
	{
		isRevived = true;
		reviveCount++;
		Networking.UpdateSession((int)(LevelsManager.Manager.Level ), 0, reviveCount, ( sM ) =>
		{
			success?.Invoke();

		}, ( status ) =>
		{
			Debug.LogError(status);
		});
	}
	private void OnDestroy()
	{
		CollidedWithAngel.onCollidedWithAngelEvent -= OnAngelCollission;
	}

	IEnumerator SetABilityDescriptionOff()
	{
		yield return new WaitForSeconds(3f);
		AbilityDescription.text = "";
	}
}
