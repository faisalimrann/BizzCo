﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class EnemyHealthManager : MonoBehaviour
{
    public Action<Enemy> OnEnemyDestroy;
    public Action OnEnemiesEnded;
    public List<Enemy> Enemies { get; private set; }

    public void Init(List<Enemy> enemies)
    {
		if ( Enemies != null )
		{
			if ( Enemies.Count > 0 )
			{
				foreach ( var enemy in Enemies )
				{
					DestroyImmediate(enemy);
				}
			}
		}
        Enemies = enemies;

        foreach (var enemy in Enemies)
        {
            enemy.healthManager.OnDestroy += ()=> { HealthManager_OnDestroy(enemy); };
        }

        if (Enemies.Count == 0)
        {
            OnEnemiesEnded?.Invoke();
        }
    }

    void HealthManager_OnDestroy(Enemy enemy)
    {
        Enemies.Remove(enemy);
        OnEnemyDestroy?.Invoke(enemy);
        if (Enemies.Count == 0)
        {
            OnEnemiesEnded?.Invoke();
        }
        //Destroy(enemy.gameObject);
    }
}
