﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]
public abstract class Bow : MonoBehaviour
{
    public BulletParametrs parametrs;
    public Action<Collision> OnHit;
    public int HitCount { get; private set; }
    private Rigidbody body;
	public int damage;

    public virtual void Shot(Vector3 position, Vector3 direction, float speedKoeficient = 1f)
    {
        transform.forward = direction;
        body = GetComponent<Rigidbody>();
        body.position = position;
        body.velocity = speedKoeficient * parametrs.speed * direction;
    }

    protected virtual void Hit(Collision collision)
    {
        HitCount++;
        OnHit?.Invoke(collision);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Hit(collision);
    }
}

[System.Serializable]
public class BulletParametrs
{
    public float damageCoefficient = 1;
    public float speed;
}

public class ShotBulletParameters
{
    public Vector3 position;
    public Vector3 direction;
}
