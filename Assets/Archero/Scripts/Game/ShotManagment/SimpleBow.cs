﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBow : Bow
{
    public override void Shot(Vector3 position, Vector3 direction, float speedKoeficient = 1)
    {
        //Create Particle
        base.Shot(position, direction, speedKoeficient);
    }

    protected override void Hit(Collision collision)
    {
        //Create Particle
        base.Hit(collision);
    }
}
