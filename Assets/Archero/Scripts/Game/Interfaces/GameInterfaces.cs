﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveManager
{
    event Action OnStartMove;
    event Action OnStopMove;
    MoveParameters Parameters { get; set; }
    void SetPosition(Vector3 position, Quaternion rotation);
    void Move(Vector3 direction);
	void Disable();
	void Enable();
}

public interface IMoveInputManager
{
    event Action<Vector3> OnControl;
	void Disable();
}

public interface IShotManager
{
    ShotParameters Parameters { get; set; }
    void SetArms(Arms arms);
    void OnShot();
}

public interface IHealthManager
{
    event Action<HealthParameters> OnDamage;
    event Action OnDestroy;

    HealthParameters Parameters { get; set; }

    void DecreaseHealth(float value);
    void Damage();
    void Destroy();
}

