﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsManager : MonoBehaviour
{
    private int achievements;

    private void Awake()
    {
        achievements = PlayerPrefs.GetInt("Achievements");
    }

    public int GetAchievements()
    {
        achievements = PlayerPrefs.GetInt("Achievements");
        return achievements;
    }

    public int GetAchievements(Enemy enemy)
    {
        //Todo
        achievements++;
        PlayerPrefs.SetInt("Achievements", achievements);
        return achievements;
    }
}
