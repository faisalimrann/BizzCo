﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapEditor : MonoBehaviour
{
    [MenuItem("Archero/Save Level")]
    private static void SaveLevel()
    {
        Obstacle[] obstacles = FindObjectsOfType<Obstacle>();
        ObstacleData[] strObstacles = new ObstacleData[obstacles.Length];
        for (int i = 0; i < strObstacles.Length; i++)
        {
            strObstacles[i] = new ObstacleData(obstacles[i]);
        }
    
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        EnemyParameters[] enemyParameters = new EnemyParameters[enemies.Length];
        for (int i = 0; i < enemyParameters.Length; i++)
        {
            enemyParameters[i] = enemies[i].GetParameters();
        }

        LevelData mapData = new LevelData
        {
            obstaclesData = strObstacles,
            enemyParameters = enemyParameters
        };
        string json = JsonUtility.ToJson(mapData);
        string localPath = "/Archero/Levels/" + SceneManager.GetActiveScene().name + ".txt";
        System.IO.File.WriteAllText(Application.dataPath + localPath, json);
        AssetDatabase.ImportAsset("Assets" + localPath);
        Selection.activeObject = AssetDatabase.LoadAssetAtPath<Object>("Assets" + localPath);
        Debug.Log("Level " + SceneManager.GetActiveScene().name + " Was saved successfully");
    }
    [MenuItem("Archero/Save Level", true)]
    static bool ValidateSaveLevel()
    {
        return SceneManager.GetActiveScene().name.Contains("Level_");
    }

}
