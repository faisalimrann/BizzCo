﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FixedJoystick : Joystick
{
	public static event Action<Vector3> OnControl;

	protected override void HandleInput( float magnitude, Vector2 normalised, Vector2 radius, Camera cam )
	{
		base.HandleInput(magnitude, normalised, radius, cam);
		Vector3 dir = Vector3.zero;
		dir+= new Vector3(Horizontal, 0f, Vertical);
		OnControl?.Invoke(dir.normalized);
	}

	public override void OnPointerUp( PointerEventData eventData )
	{
		base.OnPointerUp(eventData);
		Vector3 dir = Vector3.zero;
		OnControl?.Invoke(dir.normalized);

	}
}