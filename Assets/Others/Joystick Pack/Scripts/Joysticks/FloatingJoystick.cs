﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingJoystick : Joystick
{
	public static event Action<Vector3> OnControl;
	public RectTransform rectTransform;


	protected override void Start()
    {
        base.Start();
        background.gameObject.SetActive(false);
		rectTransform = this.transform.GetChild(0).GetChild(5).GetComponent<RectTransform>();
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
        background.gameObject.SetActive(true);
        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        background.gameObject.SetActive(false);
        base.OnPointerUp(eventData);
		Vector3 dir = Vector3.zero;
		OnControl?.Invoke(dir.normalized);
	}

	protected override void HandleInput( float magnitude, Vector2 normalised, Vector2 radius, Camera cam )
	{
		base.HandleInput(magnitude, normalised, radius, cam);
		Vector3 dir = Vector3.zero;
		dir += new Vector3(Horizontal, 0f, Vertical);
		OnControl?.Invoke(dir.normalized);
        
        rectTransform.eulerAngles = new Vector3(60, 0, Vector2.SignedAngle(rectTransform.position, handle.localPosition) -25f);


	}


}