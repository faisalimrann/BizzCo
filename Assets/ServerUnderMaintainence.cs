﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ServerUnderMaintainence : MonoBehaviour
{
	[SerializeField]
	Button Close;
	[SerializeField]
	Button FalseClose;
	[SerializeField]
	Button OkButton;
	[SerializeField]
	DOTweenAnimation animation;
	[SerializeField]
	GameObject Popup;

	[SerializeField]
	Text text;

	[SerializeField]
	Client client;
	// Start is called before the first frame update
	bool isCloseClicked = false;
	string URL = "http://xr-staging.frag-games.com:35090";
	string S3URL = "http://bizzco-frag.s3-website-us-east-1.amazonaws.com/";
	// Start is called before the first frame update
	void Start()
	{

		Networking.OnMaintainanceBreak += Networking_OnMaintainanceBreak;
		StartCoroutine("CheckServerStatus");
		DontDestroyOnLoad(this.gameObject);
	}

	private void Networking_OnMaintainanceBreak(long time)
	{
		Popup.SetActive(true);
		text.text = "The servers are closing in " + Mathf.Ceil((time /1000)/60) + " minutes.";
		FalseClose.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isCloseClicked )
				return;

			isCloseClicked = true;
			animation.DORestart();
		});
		OkButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isCloseClicked )
				return;

			isCloseClicked = true;
			animation.DORestart();
		});
	}

	public void AnimationEnd()
	{
		Close.onClick?.Invoke();
		isCloseClicked = false;
		this.transform.localScale = new Vector3(1f, 1f, 1f);
		Popup.SetActive(false);
	}

	private void OnDestroy()
	{
		Networking.OnMaintainanceBreak -= Networking_OnMaintainanceBreak;

	}

	void doPost()
	{

		//Auth token for http request
		string json = @"{ 'action' : 5000,'app_id' : 203}";

	//Our custom Headers
	Dictionary<string, string> parameters = new Dictionary<string, string>();
		//Encode the access and secret keys
		//Add the custom headers
		parameters.Add("Content-Type", "application/json");
		//Replace single ' for double " 
		//This is usefull if we have a big json object, is more easy to replace in another editor the double quote by singles one
		json = json.Replace("'", "\"");
		//Encode the JSON string into a bytes
		byte[] postData = System.Text.Encoding.UTF8.GetBytes(json);
		//Now we call a new WWW request
		WWW www = new WWW(URL, postData, parameters);
		//And we start a new co routine in Unity and wait for the response.
		StartCoroutine(WaitForRequest(www));
	}

	IEnumerator CheckServerStatus()
	{
		WWWForm form = new WWWForm();
		//form.AddField("action", "5000");
		//form.AddField("app_id", "203");

		//form.headers["Content-Type"] = "application/json";
        //form.headers.Clear();


		using (UnityWebRequest www = UnityWebRequest.Get(S3URL))
		{
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
				
			}
			else
			{
				Debug.Log("Form upload complete! "+www.downloadHandler.text);
				if (www.downloadHandler.text.Contains("true"))
				{
					text.text = "The servers are currently under maintenance. Try again later!";
					Popup.SetActive(true);
					FalseClose.onClick.AddListener(() =>
					{
						SoundManager.Play("Default Button");
						Application.Quit();
					});
					OkButton.onClick.AddListener(() =>
					{
						SoundManager.Play("Default Button");
						Application.Quit();
					});
				}
				else
				{
					client.gameObject.SetActive(true);
				}
			}
		}
	}

	//Wait for the www Request
	IEnumerator WaitForRequest( WWW www )
	{
		yield return www;
		if (www.isDone )
		{
			//Print server response
			Debug.LogError(www.text);
			if ( www.text.Contains("true") )
			{
				text.text = "The servers are currently under maintenance. Try again later!";
				Popup.SetActive(true);
				FalseClose.onClick.AddListener(() =>
				{
					SoundManager.Play("Default Button");
					Application.Quit();
				});
				OkButton.onClick.AddListener(() =>
				{
					SoundManager.Play("Default Button");
					Application.Quit();
				});
			}
			else
			{
				//Something goes wrong, print the error response
				Debug.Log(www.error);
				client.gameObject.SetActive(true);
			}
		}
		else if(!string.IsNullOrEmpty(www.error))
		{
			//Something goes wrong, print the error response
			Debug.Log(www.error);
		}
	}
}
