﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

public class SatoshiAnimationManager : MonoBehaviour
{
	public Transform player;
	CancellationTokenSource cts;
	private void Awake()
	{
		GameManager.onDoorEntered += GameManager_onDoorEntered;
		cts = new CancellationTokenSource();
	}
	public async void MoveTowardsPlayer()
	{
		await Move(cts.Token);
		this.gameObject.SetActive(false);
		GameManager.onDoorEntered -= GameManager_onDoorEntered;

	}

	async Task Move( CancellationToken ct )
	{
		await Task.Delay(1500);
		SoundManager.Play("Coin Collection");
		int initailMag =(int) (player.position - transform.position).magnitude;
		while ( !ct.IsCancellationRequested && (player.position - transform.position).magnitude > 0.3f  )
		{
			await Task.Yield();
			this.transform.position = Vector3.Lerp( this.transform.position, player.transform.position, Time.deltaTime * 5f);
		}
	}
	private void GameManager_onDoorEntered()
	{
		MoveTowardsPlayer();
	}

	private void OnDestroy()
	{
		cts.Cancel();
		GameManager.onDoorEntered -= GameManager_onDoorEntered;
	}
}