#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("Yb0ThL0qkUW5n/tW0cmcG1bI7GttrfWU2ppsaCwQju6jg/C73ZZChx+uoL3itGra1Q756goqIe/yuzkwIuoZ+u3ylZzwoQjV56g16izohnxwwkFicE1GSWrGCMa3TUFBQUVAQ3ARPYhHV+r41RutBuQQl18QiwnTV1/XsRPtDHYVBSPH95VT3Jfd9b4/dCC1sXP+HF/aptv2DxKumzqZUMJBT0BwwkFKQsJBQUDo6ghNC07corZqwWIrw6astdTUar67A6FrR5YqTXWrJI66fA0jcPTdGzwue3XaRRKcTEGqGSUA0BFC0K2iMDc27LfkCC96HLeVZbLR6DTapLxoP+C84NSKdc4QuDE3N21S+fEhBZWH0SXHhnuZgqZ9cSyjpUJDQUBB");
        private static int[] order = new int[] { 4,2,7,11,4,6,9,12,12,13,13,13,12,13,14 };
        private static int key = 64;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
