#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("MaWaYdj2Jce4T0XaPJ/xF0b2/M46qDhvSPrdRLYak4GzWamPSeG4YreBvrey5KyisLBOtbSBsrCwToGsM7Cxt7ibN/k3RtLVtLCBMEOBm7dx0oLGRou2nedaa76Qv2sLwqj+BNjX2NLQxdje35HwxMXZ3sPYxciAtl3MiDI64pFiiXUADiv+u9pOmk0+wjDRd6rquJ4jA0n1+UHRiS+kRIEztQqBM7ISEbKzsLOzsLOBvLe41j65BZFGeh2dkd7BB46wgT0G8n4aEsAj9uLkcB6e8AJJSlLBfFcS/Z2R0tTDxdjX2NLQxdSRwd7d2NLIhyj9nMkGXD0qbULGKkPHY8aB/nC0sbIzsL6xgTOwu7MzsLCxVSAYuLnvgTOwoLey5KyRtTOwuYEzsLWB9M+u/drhJ/A4dcXTuqEy8DaCOzCngaW3suS1sqK88MHB3dSR497exZ6BMHK3uZq3sLS0trOzgTAHqzACtbeis+TigKKBoLey5LW7orvwwcHoFrS4zabx56CvxWIGOpKK9hJk3t/VkdLe39XYxdje38KR3teRxMLUaIfOcDbkaBYoCIPzSmlkwC/PEOOXgZW3suS1uqKs8MHB3dSR8tTDxYyX1pE7gttGvDN+b1oSnkji2+rVJC/LvRX2Ouplp4aCenW+/H+l2GB4qMNE7L9kzu4qQ5SyC+Q+/Oy8QIGgt7Lktbuiu/DBwd3Ukfjf0p+A1YSSpPqk6KwCJUZHLS9+4Qtw6eHD0NLF2NLUkcLF0MXU3NTfxcKfgZHy8IEzsJOBvLe4mzf5N0a8sLCwy4EzsMeBv7ey5Ky+sLBOtbWys7CuIGqv9uFatFzvyDWcWocT5v3kXfhpxy6CpdQQxiV4nLOysLGwEjOw093UkcLF0N/V0MPVkcXUw9zCkdCbN/k3RrywsLS0sYHTgLqBuLey5N3Ukfjf0p+Al4GVt7LktbqirPDBzvAZKUhge9ctldqgYRIKVaqbcq4PRcIqX2PVvnrI/oVpE49IyU7aecbGn9DBwd3Un9Le3J7QwcHd1NLQwd3UkfLUw8XY19jS0MXY3t+R8MSR3teRxdnUkcXZ1N+R0MHB3djS0JHQ39WR0tTDxdjX2NLQxdje35HBxdjX2NLQxdSR08iR0N/IkcHQw8WuNDI0qiiM9oZDGCrxP51lACGjabey5Ky/tae1pZph2PYlx7hPRdo8AIHpXeu1gz3ZAj6sb9TCTtbv1A2+LIxCmviZq3lPfwQIv2jvrWd6jOPU3djQ39LUkd7fkcXZ2MKR0tTDgofrgdOAuoG4t7Lktbeis+TigKIGqgwi85Wjm3a+rAf8Le/Sefoxpp/xF0b2/M6574Gut7LkrJK1qYGnGW3Pk4R7lGRovmfaZROVkqBGEB3B3dSR497exZHy8IGvpryBh4GFg4SDgIWBgofrpryChIGDgYiDgIWBvLe4mzf5N0a8sLC0tLGyM7Cwse3F2d7D2MXIgKeBpbey5LWyorzwwQSLHEW+v7EjugCQp5/FZI28atOnyJHQwsLE3NTCkdDS0tTBxdDf0tSVU1pgBsFuvvRQlntA3MlcVgSmprmat7C0tLazsKev2cXFwcKLnp7G4Rs7ZGtVTWG4toYBxMSQ");
        private static int[] order = new int[] { 32,16,29,34,57,11,51,55,32,21,24,44,48,14,33,25,48,37,29,52,43,58,24,56,29,52,58,43,46,33,46,52,43,42,48,48,37,50,50,57,41,45,57,49,46,58,57,50,49,59,52,54,54,55,54,57,56,58,58,59,60 };
        private static int key = 177;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
