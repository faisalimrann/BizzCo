﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class PopupManager : MonoBehaviour
{
	[SerializeField] PagingView view;
	// Start is called before the first frame update
	private void OnEnable()
	{
		SoundManager.Play("Popup", 0.2f);
		view.Horizontal = false;
	}

	private void OnDisable()
	{
		view.Horizontal = true;
	}

}
