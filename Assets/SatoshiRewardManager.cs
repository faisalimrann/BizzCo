﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatoshiRewardManager : MonoBehaviour
{
	[SerializeField] SatoshiAnimationManager SatoshiPrefab;
	int numberOfSatoshi;
	// Start is called before the first frame update
	[SerializeField] Transform player;
	[SerializeField] Transform bizzCo;
	private void Start()
	{
		if ( GameData.PlayerData.Instance.CurrentHero.HeroKeyID == GameData.HeroID.SpaceRobot )
		{
			player = bizzCo;
		}
	}
	public  GameObject GetRandomCoins(Vector3 _positon)
    {
		numberOfSatoshi = UnityEngine.Random.Range(5, 10);
		GameObject temp = new GameObject();
		for ( int i = 0; i < numberOfSatoshi; i++ )
		{
			Vector3 random = UnityEngine.Random.onUnitSphere;
			random.y = 0;
			random += _positon;
			SatoshiAnimationManager clone = Instantiate(SatoshiPrefab, random, Quaternion.identity, temp.transform);
			clone.player = player;
			clone.gameObject.SetActive(true);
		}

		return temp;
    }

}
