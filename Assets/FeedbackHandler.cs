﻿using System.Collections;
using System.Collections.Generic;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackHandler : MonoBehaviour
{
	[SerializeField] InputField name;
	[SerializeField] InputField email;
	[SerializeField] InputField body;
	[SerializeField] Text error;
	[SerializeField] Button submitButton;
	[SerializeField] Button homeButton;

	[SerializeField] GameObject PopupOverlay;
	[SerializeField] GameObject Feedbackpopup;

	bool isSubmitButtonClicked = false;
		// Start is called before the first frame update
    void Start()
    {
		submitButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isSubmitButtonClicked )
				return;
			isSubmitButtonClicked = true;
			if ( string.IsNullOrEmpty(name.text) || string.IsNullOrEmpty(email.text) || string.IsNullOrEmpty(body.text) )
			{
				error.text = "All fields are mandatory!";
				isSubmitButtonClicked = false;
				return;
			}
			Networking.Feedback(name.text, email.text, body.text, ( sM ) =>
			{
				isSubmitButtonClicked = false;
				PopupOverlay.SetActive(true);
				Feedbackpopup.SetActive(true);
				name.text = "";
				email.text = "";
				body.text = "";
				error.text = "";

			}, ( status ) =>
			{
				Debug.Log(status);
				error.text = status;
			});

		});
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
