﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStepManager : MonoBehaviour
{
    // Start is called before the first frame update
    AudioSource audioSource;

    public AudioClip[] audioClips;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        FloatingJoystick.OnControl += Move;
    }

    // Update is called once per frame
    void Move( Vector3 vec )

    {
        Debug.Log("Moving");
        if ( vec != Vector3.zero )
        {
            if ( !audioSource.isPlaying )
                audioSource.PlayOneShot(audioSource.clip);
        }
        else
            audioSource.Stop();
    }

    AudioClip GetRandomClip()
    {
        return audioClips[Random.Range(0,audioClips.Length)];
    }
}
