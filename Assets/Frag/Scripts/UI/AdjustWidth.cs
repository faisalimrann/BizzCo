﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AdjustWidth : MonoBehaviour
{
    public RectTransform Canvas;
    public RectTransform Page;

    // Update is called once per frame
    void Update()
    {
        //Page.rect.Set(Page.rect.x, Page.rect.y, Canvas.rect.width, Page.rect.height);
        Page.sizeDelta = Canvas.rect.size;
    }
}
