﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPopulator : MonoBehaviour
{
	
	public Image CategoryIcon;
	public Image Item;
	public Text Level;
	public GameObject Upgradeable;
	public Image Rarity;


}
