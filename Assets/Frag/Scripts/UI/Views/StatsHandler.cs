﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsHandler : MonoBehaviour
{
	[SerializeField]
	Text CurrentLevel;
	[SerializeField]
	Slider LevelBar;

	[SerializeField]
	Text CurrentAttack;
	[SerializeField]
	Text UpgradeableAttack;
	[SerializeField]
	Slider AttackBar;

	[SerializeField]
	Text CurrentHealth;
	[SerializeField]
	Text UpgradeableHealth;
	[SerializeField]
	Slider HealthBar;



}
