﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameData;
using UnityEngine;
using UnityEngine.UI;
using static GameData.PlayerData;

public class InventoryPopulator : MonoBehaviour
{

	[SerializeField]
	Transform contentTransform;

	[SerializeField]
	ItemPopulator ItemPrefab;

	[SerializeField]
	string[] CategoriesFilePath;

	[SerializeField]
	string[] RiflesFIlePath;

	[SerializeField]
	string[] HeadgearFilePath;

	[SerializeField]
	string[] ChestItems;

	[SerializeField]
	string[] BootsFilePath;

	[SerializeField]
	string[] RarityFilePaths;

	//Equipped Items
	[SerializeField]
	Transform Chest;

	[SerializeField]
	Transform Boot;

	[SerializeField]
	Transform Weapon;

	[SerializeField]
	Transform Head;

	[SerializeField]
	ItemPopupPopulator itemPopupPopulator;

	[SerializeField]
	GameObject popupOverlay;

	void RefreshInventory()
	{
		var AllHeroes = PlayerData.Instance.UnlockedHeroes;
		if (AllHeroes[0].IsUpgradable && AllHeroes[0].UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount)
		{
			
        }

		PlayerData.Instance.CurrentArmour.UnEquip();
		PlayerData.Instance.UnlockedEquipments[0].Equip();
    }

	public void PopulateInventory( List<WeaponState> weapons, List<EquipmentState> equipments )
	{
		for ( int i = 1; i < contentTransform.childCount; i++ )
		{
			Destroy(contentTransform.GetChild(i).gameObject);
		}
		bool isUpgradeable = false;
		for ( int i = 0; i < weapons.Count; i++ )
		{
			WeaponState state = weapons[i];
			if ( !state.State.Equipped ) {
				ItemPopulator itemInstance = Instantiate(ItemPrefab, contentTransform);
				itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[0]) ;
				itemInstance.Item.sprite = Resources.Load<Sprite>(RiflesFIlePath[(int)state.WeaponData.WeaponRarity]) ;
				itemInstance.Level.text = weapons[i].State.Level.ToString();
				itemInstance.Rarity.sprite = Resources.Load<Sprite>(RarityFilePaths[(int)state.WeaponData.WeaponRarity]) ;
				if ( weapons[i].UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount )
				{
					isUpgradeable = true;
				}
				else
				{
					isUpgradeable = false;
				}
				itemInstance.GetComponent<Button>().onClick.AddListener(() =>
				{
					popupOverlay.SetActive(true);
					itemPopupPopulator.PopulateItemPopup(itemInstance, state, null, true, false, isUpgradeable);
					itemPopupPopulator.gameObject.SetActive(true);
				});
				itemInstance.gameObject.SetActive(true);
			}
		}
		for ( int i = 0; i < equipments.Count; i++ )
		{
			EquipmentState st = equipments[i];
			if ( !st.State.Equipped )
			{
				ItemPopulator itemInstance = Instantiate(ItemPrefab, contentTransform);

				itemInstance.Level.text = equipments[i].State.Level.ToString();
				itemInstance.Rarity.sprite = Resources.Load<Sprite>(RarityFilePaths[(int)st.EquipmentData.Rarity]) ;
				if ( equipments[i].UpgradeCost < PlayerData.Instance.Wallet.Satoshi.Amount )
				{
					isUpgradeable = true;
				}
				else
				{
					isUpgradeable = false;
				}

				if ( st.EquipmentData.EquipmentType == EquipmentType.Chest )
				{
					itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[2]) ;
					itemInstance.Item.sprite = Resources.Load<Sprite>(ChestItems[(int)st.EquipmentData.Rarity]) ;
				}
				else if ( st.EquipmentData.EquipmentType == EquipmentType.Head )
				{
					itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[1]) ;
					itemInstance.Item.sprite = Resources.Load<Sprite>(HeadgearFilePath[(int)st.EquipmentData.Rarity]) ;
				}
				else if ( st.EquipmentData.EquipmentType == EquipmentType.Legs )
				{
					itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[3]) ;
					itemInstance.Item.sprite = Resources.Load<Sprite>(BootsFilePath[(int)st.EquipmentData.Rarity]) ;
				}
				itemInstance.GetComponent<Button>().onClick.AddListener(() =>
				{
					popupOverlay.SetActive(true);
					itemPopupPopulator.PopulateItemPopup(itemInstance,null, st, false, false, isUpgradeable);
					itemPopupPopulator.gameObject.SetActive(true);
				});
				itemInstance.gameObject.SetActive(true);
			}
		}


	}
	public void PopulateEquippedItem()
	{
		PlayerData playerData = GameData.PlayerData.Instance;
		if ( playerData.CurrentWeapon != null )
		{
			bool isUpgradeable = false;
			if ( Weapon.childCount > 1 )
			{
				DestroyImmediate(Weapon.GetChild(1).gameObject);
			}
			ItemPopulator itemInstance = Instantiate(ItemPrefab, Weapon);
			Weapon.GetChild(0).gameObject.SetActive(false);
			itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[0]) ;
			itemInstance.Item.sprite = Resources.Load<Sprite>(RiflesFIlePath[(int)playerData.CurrentWeapon.WeaponData.WeaponRarity]) ;
			itemInstance.Level.text = playerData.CurrentWeapon.State.Level.ToString();
			itemInstance.Rarity.sprite = Resources.Load<Sprite>(RarityFilePaths[(int)playerData.CurrentWeapon.WeaponData.WeaponRarity]) ;
			RectTransform itemRect = itemInstance.GetComponent<RectTransform>();
			itemRect.anchorMin = new Vector2(0f,0f);
			itemRect.anchorMax = new Vector2(1f,1f);
			itemRect.pivot = new Vector2(0.5f, 0.5f);
			itemRect.anchoredPosition = Vector2.zero;
			if ( playerData.CurrentWeapon.UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount && playerData.CurrentWeapon.UpgradeCost  != 0)
			{

				itemInstance.Upgradeable.SetActive(true);
				isUpgradeable = true;
			}
			else
			{
				itemInstance.Upgradeable.SetActive(false);
				isUpgradeable = false;
			}
			itemInstance.GetComponent<Button>().onClick.AddListener(() =>
			{
				popupOverlay.SetActive(true);
				itemPopupPopulator.PopulateItemPopup(itemInstance, playerData.CurrentWeapon, null, true, true,isUpgradeable);
				itemPopupPopulator.gameObject.SetActive(true);
			});
			itemInstance.gameObject.SetActive(true);
		}
		else
		{
			Weapon.GetChild(0).gameObject.SetActive(true);
			if ( Weapon.childCount > 1 )
			{
				DestroyImmediate(Weapon.GetChild(1).gameObject);
			}
		}
		if ( playerData.CurrentArmour != null )
		{
			bool isUpgradeable = false;
			if ( Chest.childCount > 1 )
			{
				DestroyImmediate(Chest.GetChild(1).gameObject);
			}
			ItemPopulator itemInstance = Instantiate(ItemPrefab, Chest);
			Chest.GetChild(0).gameObject.SetActive(false);
			itemInstance.Level.text = playerData.CurrentArmour.State.Level.ToString();
			itemInstance.Rarity.sprite = Resources.Load<Sprite>(RarityFilePaths[(int)playerData.CurrentArmour.EquipmentData.Rarity]) ;
			itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[2]) ;
			itemInstance.Item.sprite = Resources.Load<Sprite>(ChestItems[(int)playerData.CurrentArmour.EquipmentData.Rarity]) ;
			RectTransform itemRect = itemInstance.GetComponent<RectTransform>();
			itemRect.anchorMin = new Vector2(0f, 0f);
			itemRect.anchorMax = new Vector2(1f, 1f);
			itemRect.pivot = new Vector2(0.5f, 0.5f);
			itemRect.anchoredPosition = Vector2.zero;
			if ( playerData.CurrentArmour.UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount && playerData.CurrentArmour.UpgradeCost!=0 )
			{
				itemInstance.Upgradeable.SetActive(true);
				isUpgradeable = true;
			}
			else
			{
				itemInstance.Upgradeable.SetActive(false);
				isUpgradeable = false;
			}
			itemInstance.GetComponent<Button>().onClick.AddListener(() =>
			{
				popupOverlay.SetActive(true);
				itemPopupPopulator.PopulateItemPopup(itemInstance, null, playerData.CurrentArmour, false, true, isUpgradeable);
				itemPopupPopulator.gameObject.SetActive(true);
			});
			itemInstance.gameObject.SetActive(true);
		}
		else
		{
			Chest.GetChild(0).gameObject.SetActive(true);
			if ( Chest.childCount > 1 )
			{
				DestroyImmediate(Chest.GetChild(1).gameObject);
			}
		}
		if ( playerData.CurrentBoots != null )
		{
			bool isUpgradeable = false;
			if ( Boot.childCount > 1 )
			{
				DestroyImmediate(Boot.GetChild(1).gameObject);
			}
			ItemPopulator itemInstance = Instantiate(ItemPrefab, Boot);
			Boot.GetChild(0).gameObject.SetActive(false);
			itemInstance.Level.text = playerData.CurrentBoots.State.Level.ToString();
			itemInstance.Rarity.sprite = Resources.Load<Sprite>(RarityFilePaths[(int)playerData.CurrentBoots.EquipmentData.Rarity]) ;
			itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[3]) ;
			itemInstance.Item.sprite = Resources.Load<Sprite>(BootsFilePath[(int)playerData.CurrentBoots.EquipmentData.Rarity]) ;
			RectTransform itemRect = itemInstance.GetComponent<RectTransform>();
			itemRect.anchorMin = new Vector2(0f, 0f);
			itemRect.anchorMax = new Vector2(1f, 1f);
			itemRect.pivot = new Vector2(0.5f, 0.5f);
			if ( playerData.CurrentBoots.UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount && playerData.CurrentBoots.UpgradeCost != 0 )
			{
				itemInstance.Upgradeable.SetActive(true);
				isUpgradeable = true;
			}
			else
			{
				itemInstance.Upgradeable.SetActive(false);
				isUpgradeable = false;
			}
			itemRect.anchoredPosition = Vector2.zero; itemInstance.GetComponent<Button>().onClick.AddListener(() =>
			{
				popupOverlay.SetActive(true);
				itemPopupPopulator.PopulateItemPopup(itemInstance, null, playerData.CurrentBoots, false, true, isUpgradeable);
				itemPopupPopulator.gameObject.SetActive(true);
			});
			itemInstance.gameObject.SetActive(true);
		}
		else
		{
			Boot.GetChild(0).gameObject.SetActive(true);
			if ( Boot.childCount > 1 )
			{
				DestroyImmediate(Boot.GetChild(1).gameObject);
			}
		}
		if ( playerData.CurrentHelmet != null )
		{

			bool isUpgradeable = false;
			if ( Head.childCount > 1 )
			{
				DestroyImmediate(Head.GetChild(1).gameObject);
			}
			ItemPopulator itemInstance = Instantiate(ItemPrefab, Head);
			Head.GetChild(0).gameObject.SetActive(false);
			itemInstance.Level.text = playerData.CurrentHelmet.State.Level.ToString();
			itemInstance.Rarity.sprite = Resources.Load<Sprite>(RarityFilePaths[(int)playerData.CurrentHelmet.EquipmentData.Rarity]) ;
			itemInstance.CategoryIcon.sprite = Resources.Load<Sprite>(CategoriesFilePath[1]);
			itemInstance.Item.sprite = Resources.Load<Sprite>(HeadgearFilePath[(int)playerData.CurrentHelmet.EquipmentData.Rarity]) ;
			RectTransform itemRect = itemInstance.GetComponent<RectTransform>();
			itemRect.anchorMin = new Vector2(0f, 0f);
			itemRect.anchorMax = new Vector2(1f, 1f);
			itemRect.pivot = new Vector2(0.5f, 0.5f);
			itemRect.anchoredPosition = Vector2.zero;
			if ( playerData.CurrentHelmet.UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount && playerData.CurrentHelmet.UpgradeCost != 0 )
			{
				itemInstance.Upgradeable.SetActive(true);
				isUpgradeable = true;
			}
			else
			{
				itemInstance.Upgradeable.SetActive(false);
				isUpgradeable = false;
			}
			itemInstance.GetComponent<Button>().onClick.AddListener(() =>
			{
				popupOverlay.SetActive(true);
				itemPopupPopulator.PopulateItemPopup(itemInstance, null, playerData.CurrentHelmet, false, true, isUpgradeable);
				itemPopupPopulator.gameObject.SetActive(true);
			});
			itemInstance.gameObject.SetActive(true);
		}
		else
		{
			Head.GetChild(0).gameObject.SetActive(true);
			if ( Head.childCount > 1 )
			{
				DestroyImmediate(Head.GetChild(1).gameObject);
			}
		}


	}

	public Sprite getWeaponImage( Rarity eqR, EquipmentType eqT, InventoryItemType itC )
	{
		Sprite sprite = null;
		if ( itC == InventoryItemType.Weapon )
		{
			sprite = Resources.Load<Sprite>(RiflesFIlePath[(int)eqR]);
		}
		else
		{
			if ( eqT == EquipmentType.Chest )
			{
				sprite = Resources.Load<Sprite>(ChestItems[(int)eqR]);
			}
			else if ( eqT == EquipmentType.Head )
			{
				sprite = Resources.Load<Sprite>(HeadgearFilePath[(int)eqR]);
			}
			else if ( eqT == EquipmentType.Legs )
			{
				sprite = Resources.Load<Sprite>(BootsFilePath[(int)eqR]);
			}
		}
		return sprite;
	}
}
