﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameData;
using RoboArchero.Networking;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopView : MonoBehaviour
{
    [Serializable]
    public class IAPView
    {
        public TextMeshProUGUI Name;
        public TextMeshProUGUI Price;
        public TextMeshProUGUI Amount;
        public Button Button;
    }

    [Serializable]
    public class ChestUX
    {
        public ChestType Chest;
        public Button OpenBtn;
        public Image UXContainer;
        public Animator Animator;
        public Button ReopenBtn;
        public Button CloseBtn;
        public Image Glow;
        public Image UnlockedItem;
        public Button SatoshiBuy;
        public long SatoshiBuyValue;
        public long TimerValue;
        public TMP_Text SatoshiBuyText;
        public TMP_Text Timer;
        public TMP_Text ReopenText;
        public TMP_Text ItemName;
        public TMP_Text ItemRarity;
        public TMP_Text ItemDescription;
		public Image itemImage;
    }

    [SerializeField]
    IAPView[] IAPViews;

    [SerializeField]
    ChestUX[] ChestViews;

    [SerializeField] BuyMoreCoinsPopup popup;
    [SerializeField] BuyMoreCoinsPopup AdNotAvailable;
    [SerializeField] GameObject popupOverlay;
	[SerializeField] InventoryPopulator inventoryPopulator;
	[SerializeField] CanvasGroup bottomBarGroup;


    [SerializeField]
    Color[] Colors;// 0 for common, 1 for Rare, 2 for Epic
    private void Start()
    {
        PlayerData.OnGameDataRecieved += Populate;
    }

    private void Populate()
    {
        GetChests();
        PopulateIAPs();
    }

    void GetChests()
    {
        Networking.GetChestTimers((sM) =>
        {
            StopCoroutine("Timers");
            PopulateChests(sM);
			bottomBarGroup.alpha = 1;
			StartCoroutine("Timers");

        }, (status) =>
        {
            Debug.LogError(status);
        });
    }

    void PopulateIAPs()
    {
        List<IAP> IAPs = DataManager.Instance.GetIAPs();
        int index = 0;
        foreach (IAPView iAPView in IAPViews)
        {
            int test = index;
            iAPView.Name.text = IAPs[index].PackageName;
            iAPView.Amount.text = IAPs[index].PackageQuantity.ToString();
			iAPView.Price.text = IAPManager.Instance.GetLocalizedPrice(IAPManager.Instance.currentIAPs[test].IAPId);
			iAPView.Button.onClick.RemoveAllListeners();
		   iAPView.Button.onClick.AddListener(async ()=>
            {
                SoundManager.Play("Default Button");
				await WaitForSeconds();

				IAPManager.Instance.BuyProductID(IAPManager.Instance.currentIAPs[test].IAPId);
            });
            index++;
        }
    }

    public void PopulateChests(ServerMessage serverMessage)
    {
		
        foreach (ChestUX chestUX in ChestViews)
        {
            chestUX.UXContainer.gameObject.SetActive(false);
            chestUX.Animator.enabled = false;
            chestUX.Animator.gameObject.SetActive(false);

            chestUX.OpenBtn.onClick.RemoveAllListeners();
            chestUX.ReopenBtn.onClick.RemoveAllListeners();
            chestUX.SatoshiBuy.onClick.RemoveAllListeners();
            chestUX.CloseBtn.onClick.RemoveAllListeners();

            if (chestUX.Chest == ChestType.Golden && serverMessage.GetChestTimersResponse.GoldenChest > 0 ||
                chestUX.Chest == ChestType.Obsidian && serverMessage.GetChestTimersResponse.ObsidianChest > 0)
            {
                chestUX.OpenBtn.gameObject.SetActive(false);

                chestUX.SatoshiBuy.gameObject.SetActive(true);
                chestUX.SatoshiBuyValue = chestUX.Chest == ChestType.Golden ? serverMessage.GetChestTimersResponse.GoldenPrice : serverMessage.GetChestTimersResponse.ObsidianPrice;
                chestUX.SatoshiBuyText.text = "x" + chestUX.SatoshiBuyValue;

                chestUX.Timer.enabled = true;
                chestUX.TimerValue = chestUX.Chest == ChestType.Golden ? (serverMessage.GetChestTimersResponse.GoldenChest) : (serverMessage.GetChestTimersResponse.ObsidianChest);
                chestUX.Timer.text = chestUX.TimerValue.ToString();
            }
            else
            {
                chestUX.Timer.enabled = false;
                chestUX.OpenBtn.gameObject.SetActive(true);
                chestUX.SatoshiBuy.gameObject.SetActive(false);
            }

            chestUX.OpenBtn.onClick.AddListener(() =>
            {
				SoundManager.Play("Default Button");
				if ( chestUX.Chest == ChestType.Golden )
				{
					AdsManager.Instance.showRewardedAd(() =>
					{
						LoaderScreen.Instance.SetStatus(true);
						Networking.OpenChests(chestUX.Chest, ( sM ) =>
						{
							OpenChestInternal(chestUX, sM.OpenChestResponse.Item.Item, sM.OpenChestResponse.Price);

						}, ( status ) =>
						{
							Debug.LogError(status);
							LoaderScreen.Instance.SetStatus(false);
						});
					}, () =>
					{
						AdMobManager.Instance.ShowAdmobRewardedAd(() =>
						{
							LoaderScreen.Instance.SetStatus(true);
							Networking.OpenChests(chestUX.Chest, ( sM ) =>
							{
								OpenChestInternal(chestUX, sM.OpenChestResponse.Item.Item, sM.OpenChestResponse.Price);

							}, ( status ) =>
							{
								Debug.LogError(status);
								LoaderScreen.Instance.SetStatus(false);
							});
						},
						() =>
						{
							//Time.timeScale = 1;
							AdNotAvailable.gameObject.SetActive(true);
							popupOverlay.SetActive(true);
							Debug.LogError("Could not load Ad");
						}, ()=>
						{
						});
					}, () =>
					{
						
					});
				}
				else
				{
					Networking.OpenChests(chestUX.Chest, ( sM ) =>
					{
						OpenChestInternal(chestUX, sM.OpenChestResponse.Item.Item, sM.OpenChestResponse.Price);

					}, ( status ) =>
					{
						Debug.LogError(status);
						LoaderScreen.Instance.SetStatus(false);
					});
				}
            });

            chestUX.SatoshiBuy.onClick.AddListener(() =>
            {
                SoundManager.Play("Default Button");
                if (PlayerData.Instance.Wallet.Satoshi.Amount >= chestUX.SatoshiBuyValue)
                {
                    LoaderScreen.Instance.SetStatus(true);
                    Networking.PurchaseChest(chestUX.Chest, (sM) =>
                    {
						PlayerData.Instance.Wallet.Satoshi.Use((uint)chestUX.SatoshiBuyValue);
                        OpenChestInternal(chestUX, sM.PurchaseChestResponse.Item.Item, sM.PurchaseChestResponse.Price);
                    }, (status) =>
                    {
                        Debug.LogError(status);
                        LoaderScreen.Instance.SetStatus(false);
                    });
                }
                else
                {
                    popup.gameObject.SetActive(true);
                    popupOverlay.SetActive(true);
                    LoaderScreen.Instance.SetStatus(false);
                }
            });

            chestUX.ReopenBtn.onClick.AddListener(() =>
            {
                SoundManager.Play("Default Button");
                if (PlayerData.Instance.Wallet.Satoshi.Amount >= chestUX.SatoshiBuyValue)
                {
                    LoaderScreen.Instance.SetStatus(true);
                    chestUX.Animator.enabled = false;
                    chestUX.Animator.gameObject.SetActive(false);
                    Networking.PurchaseChest(chestUX.Chest, (sM) =>
                    {
                        //sM.PurchaseChestResponse.
                        PlayerData.Instance.Wallet.Satoshi.Use((uint)chestUX.SatoshiBuyValue);
                        OpenChestInternal(chestUX, sM.PurchaseChestResponse.Item.Item, sM.PurchaseChestResponse.Price);
                    }, (status) =>
                    {
                        Debug.LogError(status);
                        LoaderScreen.Instance.SetStatus(false);
                    });
                }
                else
                {
                    popup.gameObject.SetActive(true);
                    popupOverlay.SetActive(true);
                    LoaderScreen.Instance.SetStatus(false);
                }
            });

            chestUX.CloseBtn.onClick.AddListener(() =>
            {
				SoundManager.Play("Default Button");
				bottomBarGroup.alpha = 1;
				chestUX.Animator.gameObject.SetActive(false);
                chestUX.UXContainer.gameObject.SetActive(false);
                chestUX.Animator.enabled = false;
                GetChests();
            });

        }
    }

    void OpenChestInternal(ChestUX chest, string UnlockedItem, long price)
    {
        LoaderScreen.Instance.SetStatus(false);
		bottomBarGroup.alpha = 0;
        chest.Animator.gameObject.SetActive(true);
        chest.UXContainer.gameObject.SetActive(true);
        chest.Animator.enabled = true;
		chest.ReopenText.text = price.ToString();
        InventoryItemType itemType = DataManager.Instance.GetItemType(UnlockedItem);
		chest.SatoshiBuyValue = price;
		switch (itemType)
        {
            case InventoryItemType.Weapon:
                GameData.WeaponData weapon = DataManager.Instance.GetWeapon((WeaponID)Enum.Parse(typeof(WeaponID), UnlockedItem));
                chest.ItemName.text = weapon.WeaponName;
                chest.ItemRarity.text = weapon.WeaponRarity.ToString();
                chest.ItemDescription.text = weapon.WeaponDescription;
				chest.itemImage.sprite = inventoryPopulator.getWeaponImage(weapon.WeaponRarity, 0, InventoryItemType.Weapon); 


				chest.Glow.color = Colors[(int)weapon.WeaponRarity];
                chest.ItemName.color = Colors[(int)weapon.WeaponRarity];
                chest.ItemRarity.color = Colors[(int)weapon.WeaponRarity];
                break;
            case InventoryItemType.Equipment:
                EquipmentData equipment = DataManager.Instance.GetEquipment((EquipmentID)Enum.Parse(typeof(EquipmentID), UnlockedItem));
                chest.ItemName.text = equipment.EquipmentName;
                chest.ItemRarity.text = equipment.Rarity.ToString();
                chest.ItemDescription.text = equipment.EquipmentDescription;
				chest.itemImage.sprite = inventoryPopulator.getWeaponImage(equipment.Rarity, equipment.EquipmentType, InventoryItemType.Equipment);

				chest.Glow.color = Colors[(int)equipment.Rarity];
                chest.ItemName.color = Colors[(int)equipment.Rarity];
                chest.ItemRarity.color = Colors[(int)equipment.Rarity];
                break;
        }
		PlayerData.Instance.UpdateInventoryFromServer();
		SoundManager.Play("Chest");
    }

    IEnumerator Timers()
    {
        int i;
        while (true)
        {
            for (i = 0; i < ChestViews.Length; i++)
            {
                if (ChestViews[i].TimerValue > 0)
                {
                    ChestViews[i].Timer.text = GetRemainingTime(ChestViews[i].TimerValue);
                    ChestViews[i].TimerValue -= 1000;
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }

    string GetRemainingTime(long Milliseconds)
    {
        TimeSpan t = TimeSpan.FromMilliseconds(Milliseconds);
        string answer = string.Format("{0:D2}h:{1:D2}m:{2:D2}s",
                        t.Hours,
                        t.Minutes,
                        t.Seconds);
        if (t.Days > 0)
            answer = string.Format("{0:D2}d:{1}", t.Days, answer);

        return answer;
    }

    private void OnDestroy()
    {
        PlayerData.OnGameDataRecieved -= Populate;
    }
	async Task WaitForSeconds()
	{
		await Task.Delay(500);
	}
}
