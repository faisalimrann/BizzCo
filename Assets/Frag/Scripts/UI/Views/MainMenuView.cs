﻿using ScreenMgr;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] Button PlayBtn;
	[SerializeField] EnergyPopupHandler buyMoreCoins;
	[SerializeField] GameObject popupOverlay;

    MainMenuController Controller;

    public void Start()
    {
        //base.OnShow();

        Controller = new MainMenuController();
        Controller.Initialize();

        PlayBtn.onClick.RemoveAllListeners();
        PlayBtn.onClick.AddListener(()=> {
			SoundManager.Play("Default Button");
			Controller.OnPlayButtonClicked(()=>
			{
            
				popupOverlay.SetActive(true);
				buyMoreCoins.gameObject.SetActive(true);
			});
		});


    }

}
