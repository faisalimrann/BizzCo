﻿using System.Collections;
using System.Collections.Generic;
using ScreenMgr;
using UnityEngine;
using UnityEngine.UI;

public class ReviveView : BaseScreen
{
    [Header("UI Elements")]
    [SerializeField] Button AdRevive;
    [SerializeField] Button SatoshiRevive;
    [SerializeField] Button Cancel;
    [SerializeField] Image Timer;
	[SerializeField] TMPro.TMP_Text text;
	[SerializeField] Text ErrorText;
	int time = 5;
    ReviveController Controller;
	[SerializeField] GameManager manager;

	[SerializeField]
	VictoryPopupController victoryPopup;
    public override void OnShow()
    {
		SoundManager.Play("Popup");
        base.OnShow();
		time = 5;
		Timer.fillAmount = (time * 1f) / 5;
		text.text = time.ToString();
        Controller = new ReviveController();

        AdRevive.onClick.RemoveAllListeners();
        SatoshiRevive.onClick.RemoveAllListeners();
        Cancel.onClick.RemoveAllListeners();

        AdRevive.onClick.AddListener(()=> { SoundManager.Play("Default Button"); StopAllCoroutines(); Controller.OnReviveRequest(()=> {
			ErrorText.text = "Could not show Ad at the moment";
			time = 5;
			text.text = time.ToString();
			Timer.fillAmount = (time * 1f) / 5;
			StartCoroutine("WaitBeforeCancel"); }); });
		SatoshiRevive.onClick.AddListener(() => { SoundManager.Play("Default Button"); StopAllCoroutines(); Controller.OnReviveRequestFromSatoshi(manager, () => { ErrorText.text = "Not enough Satoshi"; }); });

		Cancel.onClick.AddListener(() => { SoundManager.Play("Default Button"); SoundManager.Play("Game Over", 0.5f); this.gameObject.SetActive(false);  Time.timeScale = 1; manager.EndGame(); });
		StartCoroutine("WaitBeforeCancel");
		Time.timeScale = 0;
    }


	IEnumerator WaitBeforeCancel()
	{
		while(time>0)
		{

			SoundManager.Play("Revive Popup");
			yield return new WaitForSecondsRealtime(1f);
			time--;
			text.text = time.ToString();
			Timer.fillAmount =(time * 1f) / 5;
		}
		Cancel.onClick?.Invoke();

	}
}
