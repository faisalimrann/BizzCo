﻿
using System.Collections;
using System.Collections.Generic;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;
using static GameData.PlayerData;

public class PlayerDetailsPopulator : MonoBehaviour
{

	public Text CharacterName;
	public Text LevelText;
	public Text TopBarLevelText;
	public Text AttackText;
	public Text UpgradebaleAttackText;
	public Text HealthText;
	public Text UpgradebaleHealthText;
	public Text UpgradeCostText;

	[SerializeField]
	Button UpgradeButton;
	[SerializeField]
	Button EquipButton;
	[SerializeField]
	Button BuyButton;
	[SerializeField]
	Text BuyButtonText;

	int SapceRoboBuyCost = 100;

	public Image LevelFill;
	public Image TopBarLevelFill;
	public Image AttackFill;
	public Image HealthFill;


	public int CurrentLevel;
	public int CurrentAttack;
	public int CurrentHealth;
	public int UpgradeableAttack;
	public int UpgradeableHealth;
	int MaxHeroLevel;
	int MaxHealth;
	int MaxDamage;
	public GameObject CharacterModelGameObject;
	bool isEquipClicked = false;
	bool isUpgradeClicked = false;
	bool isBuyClicked = false;
	[SerializeField]
	GameObject BuyMoreCoinsPopup;

	[SerializeField]
	GameObject PopupOverlay;

	[SerializeField] PlayerDataPopulator playerDataPopulator;

	public void PopulateDetails( string HeroName, HeroState heroState, HeroID heroId)
	{
		UpgradeButton.gameObject.SetActive(true);
		EquipButton.gameObject.SetActive(false);
		BuyButton.gameObject.SetActive(false);
		isEquipClicked = false;
		isUpgradeClicked = false;
		isBuyClicked = false;
		for ( int i = 0; i < CharacterModelGameObject.transform.childCount; i++ )
		{
			CharacterModelGameObject.transform.GetChild(i).gameObject.SetActive(false);
		}
		if ( heroState == null )
		{
			PopulateLockedHero(HeroName, heroId);
			return;
		}
		Populate(HeroName, heroState);
		PopulateButtons(HeroName, heroState, heroId);
	}

	void PopulateButtons( string HeroName, HeroState heroState, HeroID heroId )
	{
		if ( heroId == PlayerData.Instance.CurrentHero.HeroKeyID )
		{
			UpgradeButton.onClick.RemoveAllListeners();
			UpgradeButton.gameObject.SetActive(true);
			EquipButton.gameObject.SetActive(false);
			BuyButton.gameObject.SetActive(false);
			if ( CurrentLevel == MaxHeroLevel )
			{
				UpgradeButton.gameObject.SetActive(false);
			}

			UpgradeButton.onClick.AddListener(() =>
			{
				SoundManager.Play("Default Button");
				if ( isUpgradeClicked )
					return;
				isUpgradeClicked = true;
				if ( heroState.UpgradeCost <= PlayerData.Instance.Wallet.Satoshi.Amount && heroState.UpgradeCost != 0 )
				{
					Networking.UpgradeItem(heroState.HeroKeyID.ToString(), ItemCategory.Hero, ( sM ) =>
					{
						heroState.Upgrade();
						Populate(HeroName, heroState);
						playerDataPopulator.PopulateData();
						isUpgradeClicked = false;

					}, ( status ) =>
					{
						Debug.LogError(status);
						isUpgradeClicked = false;
					});
				}
				else
				{
					isUpgradeClicked = false;
					PopupOverlay.SetActive(true);
					BuyMoreCoinsPopup.SetActive(true);
				}
			});
		}
		else
		{
			UpgradeButton.gameObject.SetActive(false);
			EquipButton.gameObject.SetActive(true);
			BuyButton.gameObject.SetActive(false);
			EquipButton.onClick.RemoveAllListeners();
			EquipButton.onClick.AddListener(() =>
			{
				SoundManager.Play("Default Button");
				if ( isEquipClicked)
					return;
				isEquipClicked = true;
				Networking.SelectHero(heroId.ToString(), ( sM ) =>
				{
					PlayerData.Instance.CurrentHero.UnEquip();
					heroState.Equip();
					PopulateButtons(HeroName, heroState, heroId);
					isEquipClicked = false;
				}, ( status ) =>
				{
					isEquipClicked = false;
					Debug.LogError(status);
				});
			});
		}
	}



	void PopulateLockedHero( string HeroName, HeroID id )
	{
		CharacterModelGameObject.transform.GetChild((int)id).gameObject.SetActive(true);

		UpgradeButton.gameObject.SetActive(false);
		EquipButton.gameObject.SetActive(false);
		BuyButton.gameObject.SetActive(true);

		CurrentLevel = 1;
		CurrentAttack = (int)DataManager.Instance.GetHeroLevel(id, 1).HeroDamage;
		CurrentHealth = (int)DataManager.Instance.GetHeroLevel(id, 1).HeroHealth;
		MaxHeroLevel = (int)DataManager.Instance.GetHero(id).HeroLevel.Count;
		UpgradeableAttack = (int)DataManager.Instance.GetHeroLevel(id, 2).HeroDamage - CurrentAttack;
		UpgradeableHealth = (int)DataManager.Instance.GetHeroLevel(id, 2).HeroHealth - CurrentHealth;
		MaxHealth = (int)DataManager.Instance.GetHeroLevel(id, (uint)MaxHeroLevel).HeroHealth;
		MaxDamage = (int)DataManager.Instance.GetHeroLevel(id, (uint)MaxHeroLevel).HeroDamage;
		SapceRoboBuyCost = (int)(int)DataManager.Instance.GetHeroLevel(id, 1).HeroUpgradeCost;

		BuyButtonText.text = "x" + SapceRoboBuyCost;
		CharacterName.text = HeroName;
		LevelText.text = CurrentLevel + "/" + MaxHeroLevel;
		AttackText.text = CurrentAttack.ToString();
		HealthText.text = CurrentHealth.ToString();
		UpgradebaleAttackText.text = "(+" + UpgradeableAttack.ToString() + ")";
		UpgradebaleHealthText.text = "(+" + UpgradeableHealth.ToString() + ")";

		LevelFill.fillAmount = (CurrentLevel * 1f) / MaxHeroLevel * 1f;
		AttackFill.fillAmount = CurrentAttack / (MaxDamage * 1f);
		HealthFill.fillAmount = CurrentHealth / (MaxHealth * 1f);

		BuyButton.onClick.RemoveAllListeners();
		BuyButton.onClick.AddListener(() => {
			SoundManager.Play("Default Button");
			if ( isBuyClicked )
				return;
			isBuyClicked = true;
			if ( SapceRoboBuyCost <= PlayerData.Instance.Wallet.Satoshi.Amount )
			{
				Networking.UnlockItem(id.ToString(), ItemCategory.Hero, ( sM ) =>
				{
					PlayerData.Instance.UpdateInventoryFromServer();
				}, ( status ) =>
				{
					Debug.LogError(status);
					isBuyClicked = false;
				});
			}
			else
			{
				isBuyClicked = false;
				PopupOverlay.SetActive(true);
				BuyMoreCoinsPopup.SetActive(true);
			}
		});

	}

	void Populate( string HeroName, HeroState heroState ) {
		CharacterModelGameObject.transform.GetChild((int)heroState.HeroKeyID).gameObject.SetActive(true);

		CurrentLevel = (int)heroState.State.Level;
		CurrentAttack = (int)heroState.GetCurrentHeroLevel.HeroDamage;
		CurrentHealth = (int)heroState.GetCurrentHeroLevel.HeroHealth;
		MaxHeroLevel = (int)heroState.MaxLevel;
		UpgradeableAttack = (int)heroState.GetNextHeroLevel.HeroDamage - CurrentAttack;
		UpgradeableHealth = (int)heroState.GetNextHeroLevel.HeroHealth - CurrentHealth;
		MaxHealth = (int)heroState.GetMaxHeroLevel.HeroHealth;
		MaxDamage = (int)heroState.GetMaxHeroLevel.HeroDamage;

		if ( heroState.HeroKeyID == HeroID.BizzCo )
		{
			CharacterName.text = "<color=#fbbb1d>Bizz</color>Co";
		}
		else
		{
			CharacterName.text = HeroName;
		}
		LevelText.text = CurrentLevel + "/" + MaxHeroLevel;
		TopBarLevelText.text = CurrentLevel.ToString();
		AttackText.text = CurrentAttack.ToString();
		HealthText.text = CurrentHealth.ToString();
		UpgradebaleAttackText.text = "(+" + UpgradeableAttack.ToString() + ")";
		UpgradebaleHealthText.text = "(+" + UpgradeableHealth.ToString() + ")";

		LevelFill.fillAmount = (CurrentLevel * 1f) / MaxHeroLevel * 1f;
		TopBarLevelFill.fillAmount = (CurrentLevel * 1f) / MaxHeroLevel * 1f;
		AttackFill.fillAmount = CurrentAttack / (MaxDamage * 1f);
		HealthFill.fillAmount = CurrentHealth / (MaxHealth * 1f);

		UpgradeCostText.text = "x" + heroState.UpgradeCost.ToString();
	}



}
