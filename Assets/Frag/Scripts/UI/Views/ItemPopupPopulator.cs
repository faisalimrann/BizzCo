﻿

using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;
using static GameData.PlayerData;

public class ItemPopupPopulator : MonoBehaviour
{
	public bool isItemEquipped;

	public Text ItemName;

	public Text ItemType;

	[SerializeField]
	Color RareItemTextColor;
	[SerializeField]
	Color EpicItemTextColor;

	[SerializeField]
	Button EquipItem;
	[SerializeField]
	Text EquipText;
	[SerializeField]
	Button UpgradeItem;
	[SerializeField]
	Text UpgradeCostText;

	[SerializeField]
	Text LevelText;
	[SerializeField]
	Text Description;
	public int UpgradeCost;

	public int CurrentLevel;

	public int MaxLevel;

	public Text ItemDescription;

	public Text Stats;

	public Image ItemImage;

	[SerializeField]
	PlayerDataPopulator playerDataPopulator;

	[SerializeField]
	GameObject BuyMoreCoinsPopup;
	[SerializeField]
	Button Close;
	[SerializeField]
	Button FalseClose;
	[SerializeField]
	DOTweenAnimation animation;
	bool isEquipClicked = false;
	bool isUpgradeClicked = false;
	bool isCloseClicked = false;

	private void Start()
	{
		FalseClose.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isCloseClicked )
				return;
			isCloseClicked = true;
			animation.DORestart();
		});
	}

	public void PopulateItemPopup(ItemPopulator item, WeaponState weaponState, EquipmentState equipmentState, bool isWeapon, bool isEquipped, bool isUpgradeable)
	{
		isItemEquipped = isEquipped;
		ItemImage.sprite = Instantiate(item.Item.sprite);
		if ( isEquipped )
		{
			EquipText.text = "unequip";
		}
		else
		{
			EquipText.text = "equip";
		}
		if ( isWeapon )
		{
			ItemName.text = weaponState.WeaponData.WeaponName;
			ItemType.text = weaponState.WeaponData.WeaponRarity.ToString();
			if ( weaponState.WeaponData.WeaponRarity == GameData.Rarity.Rare )
			{
				ItemType.color = RareItemTextColor;
			}
			else if ( weaponState.WeaponData.WeaponRarity == GameData.Rarity.Epic )
			{
				ItemType.color = EpicItemTextColor;
			}
			UpgradeCost = (int)weaponState.UpgradeCost;
			CurrentLevel = (int)weaponState.State.Level;
			MaxLevel = (int)weaponState.MaxLevel;
			EquipItem.onClick.RemoveAllListeners();
			EquipItem.onClick.AddListener(() =>
			{
				SoundManager.Play("Default Button");
				if ( isEquipClicked || isCloseClicked )
					return;
				isCloseClicked = true;
				isEquipClicked = true;
				
					if ( isItemEquipped )
					{
						Networking.EquipItem("", global::ItemType.Arms, ( sM ) =>
						{
							GameData.PlayerData.Instance.UnequipCurrentWeapon();
							ClosePopup();
						}, ( status ) =>
						{
							Debug.LogError(status);
							ClosePopup();
						});
					}
					else
					{
						Networking.EquipItem(weaponState.WeaponID.ToString(), global::ItemType.Arms, ( sM ) =>
						{
							GameData.PlayerData.Instance.EquipWeapon(weaponState);
							ClosePopup();
						}, ( status ) =>
						{
							Debug.LogError(status);
							ClosePopup();
						});
					}
				
			});
			UpgradeItem.onClick.RemoveAllListeners();
			if ( UpgradeCost == 0 )
			{
				UpgradeItem.gameObject.SetActive(false);
			}
			else
				UpgradeItem.gameObject.SetActive(true);

			UpgradeItem.onClick.AddListener(() =>
			{
				SoundManager.Play("Default Button");
				if ( isUpgradeClicked || isCloseClicked )
					return;
				isCloseClicked = true;


				isUpgradeClicked = true;

				if ( isUpgradeable )
				{
					Networking.UpgradeItem(weaponState.WeaponID.ToString(), ItemCategory.Weapon, ( sM ) =>
					{
						weaponState.Upgrade();
						item.Level.text = weaponState.State.Level.ToString();
						ClosePopup();
					}, ( status ) =>
					{
						Debug.LogError(status);
						ClosePopup();
					});
				}
				else
				{
					isEquipClicked = false;
					isUpgradeClicked = false;
					isCloseClicked = false;
					BuyMoreCoinsPopup.SetActive(true);
					this.gameObject.SetActive(false);
				}
				
			});
			Description.text = weaponState.WeaponData.WeaponDescription;
			string stat = "";
			WeaponLevel level;
			weaponState.WeaponData.WeaponLevels.TryGetValue(weaponState.State.Level.ToString(), out level);
			stat += "Damage: " + level.WeaponDamage + "\nAttack Speed: " + level.BaseAttackSpeed + "\nCritical Damage: " + level.CriticalDamage + "\nCritical Chance: " + level.CriticalDamgeRate;
			Stats.text = stat;
		}
		if ( !isWeapon )
		{
			ItemName.text = equipmentState.EquipmentData.EquipmentName;
			ItemType.text = equipmentState.EquipmentData.Rarity.ToString();
			if ( equipmentState.EquipmentData.Rarity == GameData.Rarity.Rare )
			{
				ItemType.color = RareItemTextColor;
			}
			else if ( equipmentState.EquipmentData.Rarity == GameData.Rarity.Epic )
			{
				ItemType.color = EpicItemTextColor;
			}
			UpgradeCost = (int)equipmentState.UpgradeCost;
			CurrentLevel = (int)equipmentState.State.Level;
			MaxLevel = (int)equipmentState.MaxLevel;
			EquipItem.onClick.RemoveAllListeners();
			EquipItem.onClick.AddListener(() =>
			{
				SoundManager.Play("Default Button");
				if ( isEquipClicked || isCloseClicked )
					return;
				isCloseClicked = true;

				isEquipClicked = true;
				global::ItemType type = global::ItemType.Chest;
				if ( equipmentState.EquipmentData.EquipmentType == EquipmentType.Chest ) {
					type = global::ItemType.Chest;
				}
				else if ( equipmentState.EquipmentData.EquipmentType == EquipmentType.Head )
				{
					type = global::ItemType.Head;
				}
				else if ( equipmentState.EquipmentData.EquipmentType == EquipmentType.Legs )
				{
					type = global::ItemType.Legs;
				}
				if ( isItemEquipped )
				{

					Networking.EquipItem("", type, (sM)=>
					{
						GameData.PlayerData.Instance.UnequipCurrentItem(equipmentState.EquipmentData.EquipmentType);
						ClosePopup();
					}, (status)=>
					{
						Debug.LogError(status);
						ClosePopup();

					}); 
				}
				else
				{
					Networking.EquipItem(equipmentState.EquipmentID.ToString(), type, ( sM ) =>
					{
						GameData.PlayerData.Instance.EquipItem(equipmentState, equipmentState.EquipmentData.EquipmentType);
						ClosePopup();
					}, ( status ) =>
					{
						Debug.LogError(status);
						ClosePopup();
					});
				}
				

			});
			UpgradeItem.onClick.RemoveAllListeners();
			if ( UpgradeCost == 0 )
			{
				UpgradeItem.gameObject.SetActive(false);
			}
			else
				UpgradeItem.gameObject.SetActive(true);

			UpgradeItem.onClick.AddListener(() =>
			{
				SoundManager.Play("Default Button");
				if ( isUpgradeClicked || isCloseClicked )
					return;
				isCloseClicked = true;
				isUpgradeClicked = true;
				if ( isUpgradeable )
				{
					Networking.UpgradeItem(equipmentState.EquipmentID.ToString(), ItemCategory.Equipment, ( sM ) =>
					{
						equipmentState.Upgrade();
						item.Level.text = equipmentState.State.Level.ToString();
						ClosePopup();
					}, ( status ) =>
					{
						Debug.LogError(status);
						ClosePopup();
					});
				}
				else
				{
					BuyMoreCoinsPopup.SetActive(true);
					this.gameObject.SetActive(false);
				}
			});
			Description.text = equipmentState.EquipmentData.EquipmentDescription;
			string stat = "";
			EquipmentLevel level;
			equipmentState.EquipmentData.EquipmentLevel.TryGetValue(equipmentState.State.Level.ToString(), out level);
			stat += "Damage: " + level.Damage + "\nAttack Speed: " + level.AttackSpeed + "\nHealth: " + level.EquipmentHealth;
			Stats.text = stat;
		}

		UpgradeCostText.text = "x" + UpgradeCost;
		LevelText.text = CurrentLevel + "/" + MaxLevel;
	}

	void ClosePopup()
	{
		isCloseClicked = true;
		animation.DORestart();
		playerDataPopulator.PopulateData();
	}

	public void AnimationEnd()
	{
		Close.onClick?.Invoke();
		isEquipClicked = false;
		isUpgradeClicked = false;
		isCloseClicked = false;
		this.transform.localScale = new Vector3(1f, 1f, 1f);
	}
}
