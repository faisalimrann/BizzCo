﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;

public class EnergyPopupHandler : MonoBehaviour
{

	[SerializeField] Button AdButton;
	[SerializeField] Button SatoshiButton;
	[SerializeField] Button CloseButton;
	[SerializeField] Button FalseCloseButton;

	[SerializeField] Text RemainingAds;
	[SerializeField] Text Error;
	[SerializeField] DOTweenAnimation animation;
	[SerializeField] EnergyManager energyManager;
	[SerializeField] BuyMoreCoinsPopup buyMoreCoins;
	[SerializeField] BuyMoreCoinsPopup AdNotAvailable;


	int SatoshiChargeValue = 250;
	public int RemainingAdsValue = 4;

	bool BuyBySatoshiClicked = false;
	bool isCloseClicked = false;
	bool BuyByAdClicked = false;

	bool isEnergyBought = false;
	
	public static Action onSatoshiConsumed;
	// Start is called before the first frame update
	void Start()
	{
		FalseCloseButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isCloseClicked )
				return;
			isCloseClicked = true;
			animation.DORestart();
		});
		RemainingAdsValue = energyManager.RemainingAdsBonuses;
		RemainingAds.text = RemainingAdsValue.ToString();
		AdButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			Error.text = "";
			if ( BuyByAdClicked || isCloseClicked )
				return;
			isCloseClicked = true;
			BuyByAdClicked = true;
			if ( RemainingAdsValue > 0 )
			{
				AdsManager.Instance.showRewardedAd(() =>
				{
					SeenAd();
				}, ()=>
				{
					AdMobManager.Instance.ShowAdmobRewardedAd(() =>
					{
						SeenAd();
					}, () =>
					{
						isCloseClicked = false;
						BuyByAdClicked = false;
						BuyBySatoshiClicked = false;
						this.gameObject.SetActive(false);
						AdNotAvailable.gameObject.SetActive(true);
					},()=>
					{
						isCloseClicked = false;
						BuyByAdClicked = false;
						BuyBySatoshiClicked = false;
					});
				},()=>
				{
					isCloseClicked = false;
					BuyByAdClicked = false;
					BuyBySatoshiClicked = false;
				});
			}
			else
			{
				animation.DORestart();
			}
		});
		SatoshiButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( BuyBySatoshiClicked || isCloseClicked )
				return;
			isCloseClicked = true;
			BuyBySatoshiClicked = true;
			if ( PlayerData.Instance.Wallet.Satoshi.Amount >= SatoshiChargeValue )
			{
				Networking.BuyEnergySatoshi(( sM ) =>
				{
					isEnergyBought = true;
					PlayerData.Instance.Wallet.Satoshi.Use((uint)SatoshiChargeValue);
					animation.DORestart();
					
				}, ( status ) =>
				 {
					 Debug.LogError(status);
					 animation.DORestart();
				 });
			}
			else
			{
				isCloseClicked = false;
				BuyByAdClicked = false;
				BuyBySatoshiClicked = false;
				this.gameObject.SetActive(false);
				buyMoreCoins.gameObject.SetActive(true);
			}
		});
		if ( RemainingAdsValue <= 0 )
		{
			AdButton.interactable = false;
		}
		else
		{
			AdButton.interactable = true;
		}
	}

	void SeenAd()
	{
		Networking.BuyEnergyAd(( sM ) =>
		{
			RemainingAdsValue--;
			RemainingAds.text = RemainingAdsValue.ToString();
			isEnergyBought = true;
			if ( RemainingAdsValue <= 0 )
			{
				AdButton.interactable = false;
			}
			else
			{
				AdButton.interactable = true;
			}
			animation.DORestart();
		}, ( status ) =>
		{
			Debug.LogError(status);
			animation.DORestart();
		});
	}

	public void AnimationEnd()
	{
		if ( isEnergyBought )
		{
			energyManager.AddEnergy();
			isEnergyBought = false;
		}

		CloseButton.onClick?.Invoke();
		isCloseClicked = false;
		BuyByAdClicked = false;
		BuyBySatoshiClicked = false;
		Error.text = "";
		this.transform.localScale = new Vector3(1f, 1f, 1f);
	}
}
