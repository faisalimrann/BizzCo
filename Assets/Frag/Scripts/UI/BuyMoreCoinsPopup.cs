﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BuyMoreCoinsPopup : MonoBehaviour
{
	[SerializeField]
	Button Close;
	[SerializeField]
	Button FalseClose;
	[SerializeField]
	Button OkButton;
	[SerializeField]
	DOTweenAnimation animation;

	bool isCloseClicked = false;
	// Start is called before the first frame update
	void Start()
	{
		FalseClose.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isCloseClicked )
				return;

			isCloseClicked = true;
			animation.DORestart();
		});
		OkButton.onClick.AddListener(() =>
		{
			SoundManager.Play("Default Button");
			if ( isCloseClicked )
				return;

			isCloseClicked = true;
			animation.DORestart();
		});
	}

	public void AnimationEnd()
	{
		Close.onClick?.Invoke();
		isCloseClicked = false;
		this.transform.localScale = new Vector3(1f, 1f, 1f);
	}
}
