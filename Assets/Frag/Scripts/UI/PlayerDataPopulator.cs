﻿using System.Collections;
using System.Collections.Generic;
using GameData;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDataPopulator : MonoBehaviour
{
	[SerializeField]
	PlayerDetailsPopulator detailsPopulator;

	[SerializeField]
	InventoryPopulator inventoryPopulator;

	[SerializeField]
	Button ChangeHeroButton;

	bool isInitialized = false;
	HeroID currentShowingHero;
	private void Start()
	{
		GameData.PlayerData.OnGameDataRecieved += PopulateData;
		GameData.PlayerData.OnUpdateInventory += PopulateData;

		ChangeHeroButton.onClick.AddListener(() => { SoundManager.Play("Default Button"); ChangeHeroButtonClickListener(); });
	}

	public void PopulateData()
	{
		GameData.PlayerData.HeroState heroState = GameData.PlayerData.Instance.CurrentHero;
		if ( !isInitialized )
		{
			currentShowingHero = heroState.HeroKeyID;
			isInitialized = true;
			detailsPopulator.PopulateDetails(heroState.HeroName, heroState, currentShowingHero);	
		}
		else
		{
			heroState = PlayerData.Instance.UnlockedHeroes.Find(( x ) => x.HeroKeyID == currentShowingHero);
			if ( heroState != null )
			{
				detailsPopulator.PopulateDetails(heroState.HeroName, heroState, currentShowingHero);
			}
			else
			{
				detailsPopulator.PopulateDetails(DataManager.Instance.GetHero(HeroID.SpaceRobot).HeroName, heroState, currentShowingHero);
			}
		}
		PopulateInventory();
	}

	void PopulateInventory()
	{
		inventoryPopulator.PopulateInventory(GameData.PlayerData.Instance.UnlockedWeapons, GameData.PlayerData.Instance.UnlockedEquipments);
		inventoryPopulator.PopulateEquippedItem();
	}

	void ChangeHeroButtonClickListener()
	{
		if ( currentShowingHero == HeroID.SpaceRobot ) {
			GameData.PlayerData.HeroState heroState = GameData.PlayerData.Instance.UnlockedHeroes.Find(( x ) => x.HeroKeyID == HeroID.BizzCo);
			currentShowingHero = HeroID.BizzCo;
			detailsPopulator.PopulateDetails(heroState.HeroName, heroState, currentShowingHero);
		}
		else if ( GameData.PlayerData.Instance.UnlockedHeroes.Find(( x ) => x.HeroKeyID == HeroID.SpaceRobot) != null && currentShowingHero == HeroID.BizzCo ) {
			GameData.PlayerData.HeroState heroState = GameData.PlayerData.Instance.UnlockedHeroes.Find(( x ) => x.HeroKeyID == HeroID.SpaceRobot);
			currentShowingHero = HeroID.SpaceRobot;
			detailsPopulator.PopulateDetails(heroState.HeroName, heroState, currentShowingHero);
		}
		else {
			currentShowingHero = HeroID.SpaceRobot;
			detailsPopulator.PopulateDetails(DataManager.Instance.GetHero(HeroID.SpaceRobot).HeroName, null, currentShowingHero);
		}
	}

	private void OnDestroy()
	{
		GameData.PlayerData.OnGameDataRecieved -= PopulateData;
		GameData.PlayerData.OnUpdateInventory -= PopulateData;
	}

}
