﻿
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;

public class BizzCoPopupHandler : MonoBehaviour
{
    [SerializeField] Button CloseButton;
    [SerializeField] Button FalseCloseButton;
    [SerializeField] Button WithdrawButton;

    [SerializeField] InputField SatoshiAmount;
    [SerializeField] InputField CryptoAddress;

    [SerializeField] TransactionHistoryItem prefab;
    [SerializeField] Transform container;

    [SerializeField] Text Error;
    [SerializeField] Text Message;

    [SerializeField] DOTweenAnimation animation;

    //int MinimumAmount = 20;

    bool isCloseClicked = false;

    // Start is called before the first frame update
    void Start()
    {
        FalseCloseButton.onClick.AddListener(() =>
        {
            if (isCloseClicked)
                return;

            isCloseClicked = true;
            SoundManager.Play("Default Button");
            animation.DORestart();
        });
        PopulateTransactions();
        Message.color = Color.white;
        Message.text = "1 Bizzcoin =  " + DataManager.Instance.TitleData().BizzcoinThreshold.ToString() + " Satoshi. ";
        Error.text = "You can withdraw " + DataManager.Instance.TitleData().MinWithdrawal + " Bizzcoin once in 15 days.";
        WithdrawButton.onClick.AddListener(() =>
        {
            SoundManager.Play("Default Button");
            Error.color = Color.red;
            if (string.IsNullOrEmpty(SatoshiAmount.text) || string.IsNullOrEmpty(CryptoAddress.text))
            {
                Error.text = "All fields are mandatory!";
                return;
            }
            int amount = int.Parse(SatoshiAmount.text);
            string address = CryptoAddress.text;
            if (amount < DataManager.Instance.TitleData().MinWithdrawal)
            {
                Error.text = "Minimum withdrawl is " + DataManager.Instance.TitleData().MinWithdrawal + " bizzcoins!";
                return;
            }
            if (amount > DataManager.Instance.TitleData().MinWithdrawal && ((int)PlayerData.Instance.Wallet.Satoshi.Amount / (int)DataManager.Instance.TitleData().BizzcoinThreshold) < 10)
            {
                Error.text = "You don't have enough BizzCoin to withdraw";
                return;
            }
            isCloseClicked = true;
            Networking.WithdrawSatoshi(address, amount, (sM) =>
            {
                PlayerData.Instance.Wallet.Satoshi.Use((uint)amount * DataManager.Instance.TitleData().BizzcoinThreshold);
                Error.color = Color.green;
                Error.text = "Make sure the wallet adress is correct.";
                SatoshiAmount.text = "";
                CryptoAddress.text = "";
                PopulateTransactions();
                isCloseClicked = false;
            }, (status) =>
            {
                Error.color = Color.red;
                Error.text = status;
                Debug.LogError(status);
                isCloseClicked = false;
            });
        });

    }

    public void PLaySound(string name)
    {
        SoundManager.Play(name);
    }

    void PopulateTransactions()
    {
        Networking.GetTransactionHistory((sM) =>
        {
            foreach (Transaction transaction in sM.GetTransactionHistoryResponse.Transaction)
            {
                TransactionHistoryItem item = Instantiate(prefab, container);
                item.Amount.text = (transaction.Amount / (int)DataManager.Instance.TitleData().BizzcoinThreshold).ToString("0");
                item.Status.text = transaction.Status;
                Debug.LogError(DateTime.UtcNow + "    " + DateTime.UtcNow.Ticks);
                item.Date.text = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddMilliseconds(transaction.Time).ToLocalTime().ToString("yyyy - MM - dd");
                item.gameObject.SetActive(true);
                isCloseClicked = false;
            }
        }, (status) =>
        {
            Debug.LogError(status);
        });

    }
    public void AnimationEnd()
    {
        CloseButton.onClick?.Invoke();
        isCloseClicked = false;
        this.transform.localScale = new Vector3(1f, 1f, 1f);
        Error.color = Color.red;
        Error.text = "";
    }
}
