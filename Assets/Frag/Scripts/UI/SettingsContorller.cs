﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsContorller : MonoBehaviour
{
	[SerializeField] Slider BGM;
	[SerializeField] Slider Sound;
	[SerializeField] TMPro.TMP_Text BGMText;
	[SerializeField] TMPro.TMP_Text SoundText;
	
	// Start is called before the first frame update
    void Start()
    {
		BGM.onValueChanged.AddListener((value) =>
		{
			BGMText.text = (value * 100).ToString("f1");
		});
		Sound.onValueChanged.AddListener(( value ) =>
		{
			SoundText.text = (value * 100).ToString("f1");
		});
	}

}
