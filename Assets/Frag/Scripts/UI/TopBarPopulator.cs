﻿using System.Collections;
using System.Collections.Generic;
using GameData;
using UnityEngine;
using UnityEngine.UI;

public class TopBarPopulator : MonoBehaviour
{

	[SerializeField] GameObject abilityTemplate;
	[SerializeField] Transform transform;

	private void Start()
	{
		PlayerManager.onAbilityApplied += PopulateAbility;
	}

	void PopulateAbility( Ability ab )
	{
		GameObject temp = Instantiate(abilityTemplate, transform);
		temp.transform.GetChild(0).GetComponent<Image>().sprite = ab.GetComponent<SpriteRenderer>().sprite;
		temp.SetActive(true);
	}

	public void PlaySound( string sound )
	{
		SoundManager.Play("Default Button");
	}

	private void OnDestroy()
	{
		PlayerManager.onAbilityApplied -= PopulateAbility;
	}
}
