﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PriorityScreens : MonoBehaviour
{
    public enum PScreenType
    {
        AboutUs,
        FeedBack,
        Rules
    }
    [Serializable]
    public struct PScreens
    {
        public PScreenType Type;
        public Button Button;
        public GameObject Screen;
    }

    [SerializeField]
    GameObject Parent;

    [SerializeField]
    Button CloseButton;

    [SerializeField]
    PScreens[] Screens;
    void Start()
    {
        gameObject.SetActive(false);
        //PlayerPrefs.DeleteKey("ShowRules");
        foreach (PScreens screen in Screens)
        {
            screen.Button.onClick.RemoveAllListeners();
            screen.Button.onClick.AddListener(() =>
            {
                SoundManager.Play("Default Button");
                ShowScreen(screen.Type);
            });
        }

        CloseButton.onClick.AddListener(() =>
        {
            SoundManager.Play("Default Button");
            HideAllScreens();
        });

        if (PlayerPrefs.GetInt("ShowRules", 0) == 0)
        {
            ShowScreen(PScreenType.Rules);
            PlayerPrefs.SetInt("ShowRules", 1);
        }
    }

    void ShowScreen(PScreenType type)
    {
        Parent.SetActive(true);
        foreach (PScreens screen in Screens)
        {
            if (screen.Type == type)
                screen.Screen.SetActive(true);
            else
                screen.Screen.SetActive(false);
        }
    }

    void HideAllScreens()
    {
        Parent.SetActive(false);
        foreach (PScreens screen in Screens)
        {
            screen.Screen.SetActive(false);
        }
    }
}
