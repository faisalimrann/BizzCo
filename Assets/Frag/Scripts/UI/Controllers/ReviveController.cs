﻿using System;
using System.Threading.Tasks;
using GameData;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReviveController
{
	int SatoshiReviveAmount = 30;
    public void Initialize()
    {

    }

    public void OnReviveRequest(Action failure)
    {
		AdsManager.Instance.showRewardedAd(() =>
		{
			UnityEngine.Object.FindObjectOfType<PlayerManager>().Revive();
			ServiceLocator.Get<ScreenMgr.ScreenManager>().Hide();
			Time.timeScale = 1;
		}, () =>
		{
			AdMobManager.Instance.ShowAdmobRewardedAd(() =>
			{
				UnityEngine.Object.FindObjectOfType<PlayerManager>().Revive();
				ServiceLocator.Get<ScreenMgr.ScreenManager>().Hide();
				Time.timeScale = 1;
			},
			() =>
			{
				failure?.Invoke();
			}, () =>
			{
				failure?.Invoke();
			});
		}, () =>
		{
			failure?.Invoke();
		});
	}
	public async void OnReviveRequestFromSatoshi( GameManager manager, Action failed)
	{
		if ( ((int)PlayerData.Instance.Wallet.Satoshi.Amount) >= SatoshiReviveAmount)
		{
			manager.ReviveUsingSatoshi(()=>
			{
				PlayerData.Instance.Wallet.Satoshi.Use((uint)SatoshiReviveAmount);
				UnityEngine.Object.FindObjectOfType<PlayerManager>().Revive();
				ServiceLocator.Get<ScreenMgr.ScreenManager>().Hide();
				Time.timeScale = 1;
			});
		}
		else
		{
			Time.timeScale = 1;
			failed?.Invoke();
			await WaitForSeconds();
			manager.EndGame();
		}
	}
	public void OnCancelRequest()
    {
		Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

	async Task WaitForSeconds()
	{
		await Task.Delay(1500);
	}
}
