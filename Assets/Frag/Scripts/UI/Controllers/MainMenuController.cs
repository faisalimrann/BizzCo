﻿using System;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuController
{
    public void Initialize()
    {

    }

	Action callback;
    public void OnPlayButtonClicked(Action failCallBack)
    {
		callback = failCallBack;
		Networking.StartGame(( sM ) =>
		{
			LoaderScreen.Instance.SetStatus(true);
			SceneManager.sceneLoaded += SceneManager_sceneLoaded;
			SceneManager.LoadScene("BattleField");

		}, ( status ) =>
		{
			Debug.LogError(status);
			failCallBack?.Invoke();
		});
    }

	private void SceneManager_sceneLoaded( Scene arg0, LoadSceneMode arg1 )
	{
		LoaderScreen.Instance.SetStatus(false);
	}

}
