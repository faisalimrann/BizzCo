﻿using System.Collections;
using System.Collections.Generic;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TopBarDataManager : MonoBehaviour
{
	[SerializeField] Text ExperienceLevel;
	[SerializeField] Text CurrentEnergy;
	[SerializeField] Text CurrentSatoshi;
	[SerializeField] Text CurrentBizzCoin;
	[SerializeField] TMPro.TMP_Text Username;												

	[SerializeField] Image ExperienceLevelImage;
	[SerializeField] Image EnergyImage;
	// Start is called before the first frame update

	[SerializeField] Button EnergyButton;
	
	void Start()
	{
		GameData.PlayerData.OnGameDataRecieved += Initialize;
		GameData.PlayerData.OnCurrencyAmountChanged += UpdateCurrency;
    }

	private void Initialize()
	{
		CurrentSatoshi.text = GameData.PlayerData.Instance.Wallet.Satoshi.Amount.ToString();
		CurrentBizzCoin.text = ((int)((int)GameData.PlayerData.Instance.Wallet.Satoshi.Amount / DataManager.Instance.TitleData().BizzcoinThreshold)).ToString();
		Username.text = Networking.Username;
	}
	// Update is called once per frame
	void UpdateCurrency(uint value)    
	{
		CurrentSatoshi.text = GameData.PlayerData.Instance.Wallet.Satoshi.Amount.ToString();
		CurrentBizzCoin.text = ((int)((int)GameData.PlayerData.Instance.Wallet.Satoshi.Amount / DataManager.Instance.TitleData().BizzcoinThreshold)).ToString();
	}

	public void PlaySound( string name )
	{
		SoundManager.Play(name);
	}

	private void OnDestroy()
	{
		GameData.PlayerData.OnCurrencyAmountChanged -= UpdateCurrency;
		GameData.PlayerData.OnGameDataRecieved -= Initialize;
	}

}
