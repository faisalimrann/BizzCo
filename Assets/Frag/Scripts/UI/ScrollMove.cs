﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollMove : MonoBehaviour
{

	[SerializeField] Scrollbar verticalScrollBar;
	[SerializeField] int speed;
    // Start is called before the first frame update
    void Start()
    {
		verticalScrollBar.value = 1;        
    }

    // Update is called once per frame
    void Update()
    {
		verticalScrollBar.value = Mathf.Lerp(verticalScrollBar.value, 0, Time.deltaTime * speed);
    }
}
