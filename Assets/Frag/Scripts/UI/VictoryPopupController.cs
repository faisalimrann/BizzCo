﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VictoryPopupController : MonoBehaviour
{

	[SerializeField ] TMPro.TMP_Text StageNumber;
	[SerializeField ] TMPro.TMP_Text SatoshiCollected;
	[SerializeField ] TMPro.TMP_Text energyCollected;
	[SerializeField ] TMPro.TMP_Text bizzCoinCollected;

	[SerializeField] GameObject Victory;
	[SerializeField] GameObject Lose;

	[SerializeField] DOTweenAnimation animation;
	[SerializeField] GameManager gameManager;
	public bool IsVictory;

	bool isCloseClicked = false;

	private void Start()
	{
		GetComponent<Button>().onClick.AddListener(() =>
		{
			if ( isCloseClicked )
				return;
			isCloseClicked = true;
			animation.DORestart();
		});
	}
	private void OnEnable()
	{
		if ( IsVictory )
		{
			Victory.SetActive(true);
			Lose.SetActive(false);
			SatoshiCollected.text = gameManager.TotalSatoshiCollected.ToString();
			energyCollected.text = gameManager.TotalEnergyCollected.ToString();
			bizzCoinCollected.text = gameManager.TotalBizzCoinCollected.ToString();
				//	StageNumber.text = (LevelsManager.Manager.GetLevel() + 1).ToString();
			Networking.PlayCoinAnim = true;
		}
		else
		{
			Victory.SetActive(false);
			Lose.SetActive(true);
		}


	}

	public void AnimationEnd()
	{
		Networking.EndGame((sM)=>
		{
			LoaderScreen.Instance.SetStatus(true);
			SceneManager.sceneLoaded += SceneManager_sceneLoaded;
			SceneManager.LoadScene("MainMenu");
		},
		(status)=>
		{
			Debug.LogError(status);
			LoaderScreen.Instance.SetStatus(true);
			SceneManager.sceneLoaded += SceneManager_sceneLoaded;
			SceneManager.LoadScene("MainMenu");
		});
	}
	// Start is called before the first frame update
	private void SceneManager_sceneLoaded( Scene arg0, LoadSceneMode arg1 )
	{
		PlayerData.Instance.GetGameData();

	}

}
