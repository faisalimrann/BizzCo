﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

/*
 * UNITY IAP Plugin for SuperCricket 
 * created by farukh jamal
 */
public class IAPManager : MonoBehaviour, IStoreListener
{
    #region VARIABLES
    // Initialization of ISTORE CONTROLLER AND EXTENSION PROVIDER
    // 'IStoreController' will be used for intializing products and making purchases
    // 'IExtension provider' will be used for performing further processing on products like restore purchases.
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    // private variables
    int InAppServerCount;
    bool ServerResponded;

    // if you are getting your products from server safe your current IAP's accordingly in a list of IAPS
    [Header("Delecare your IAPs here.")]
    public List<CurrentIAP> currentIAPs = new List<CurrentIAP>();

    //delegates for successful and unsuccessful purchases actions
    public delegate void PurchaseSuccessful(string id);
    public static event PurchaseSuccessful OnPurchaseSuccessful;

    public delegate void PurchaseUnSuccessful(string id);
    public static event PurchaseUnSuccessful OnPurchaseUnSuccessful;

    //loging your IAP's Logs
    [Header("Enable logging?")]
    [Tooltip("Enable it if you want to display IAPManager Logs")]
    public bool EnableLogging;

    //instance for IAP Manager
    public static IAPManager Instance;
    AppleInAppPurchaseReceipt apple;
	#endregion
	private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        //Debug.Log("Starting Coroutine");
        InitializePurchasing();
    }


    public void InitializePurchasing()
    {
        // before initialization add your products on IStore
        Debug.Log("Check initialize Purchase");
        if (IsInitialized())
        {
            return;
        }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        for (int i = 0; i < currentIAPs.Count; i++)
        {
            builder.AddProduct(currentIAPs[i].IAPId, currentIAPs[i].IAPType);
            //  Debug.Log("ProductID" + currentIAPs[i].itemId);
        }
        UnityPurchasing.Initialize(this, builder);
        Debug.Log("initialize Purchase");
        StopCoroutine("GetServerItems");
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    //Buy product by id on google play console
    public void BuyProductID(string productId)
    {

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            if (EnableLogging)
                Debug.LogError("IAP Manager Logs: Internet not connected please connect to internet and try again.");
        }
        if (IsInitialized())
        {

            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                if (EnableLogging)
                    Debug.Log(string.Format("IAP Manager Logs: Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);

            }
            else
            {
                if (EnableLogging)
                {
                    Debug.LogError("IAP Manager Logs: BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                    Debug.Log("IAP Manager Logs: Will Re-try again");
                }
            }
        }
        else
        {
            if (EnableLogging)
                Debug.LogError("IAP Manager Logs: BuyProductID FAIL. Not initialized.");

            if (m_StoreController == null)
            {
                InitializePurchasing();
            }
        }
    }

    // restore all your purchases
    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            if (EnableLogging)
                Debug.LogError("IAP Manager Logs: RestorePurchases FAIL. Not initialized.");
            return;
        }
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            if (EnableLogging)
                Debug.Log("IAP Manager Logs: RestorePurchases started ...");
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                if (EnableLogging)
                    Debug.Log("IAP Manager Logs: RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            var google = m_StoreExtensionProvider.GetExtension<IGooglePlayStoreExtensions>();
            google.RestoreTransactions((result) =>
            {
                if (EnableLogging)
                    Debug.Log("IAP Manager Logs: RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
            //if (EnableLogging)
            //    Debug.LogError("IAP Manager Logs: RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    private void OnTransactionsRestored(bool success)
    {
        Debug.Log("Transactions restored." + success);
    }
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        if (EnableLogging)
            Debug.Log("IAP Manager Logs: OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.LogError("IAP Manager Logs: OnInitializeFailed InitializationFailureReason:" + error);
    }

    // process purchase will execute after successful initialization of purchases
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        bool validPurchase = true;
        string googlePurchasingToken = null;

#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
        // validate your purchase through receipt
        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
        try
        {

            var result = validator.Validate(e.purchasedProduct.receipt);
            Debug.Log("IAP Manager Logs: Receipt is valid. Contents:");
            foreach (IPurchaseReceipt productReceipt in result)
            {
                Debug.Log(productReceipt.productID);
                Debug.Log(productReceipt.purchaseDate);
                Debug.Log(productReceipt.transactionID);
#if UNITY_ANDROID
                GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
                
                if (google != null)
                {
                    if (EnableLogging)
                    {
                        Debug.Log("IAP Manager Logs: Google transactionID: " + google.transactionID);
                        Debug.Log("IAP Manager Logs: Google purchaseState: " + google.purchaseState);
                        Debug.Log("IAP Manager Logs: Google purchaseToken: " + google.purchaseToken);
                    }
                    googlePurchasingToken = google.purchaseToken;
                    validPurchase = true;
                }
#elif UNITY_IOS
                apple = productReceipt as AppleInAppPurchaseReceipt;
                if (apple != null)
                {
                    if (EnableLogging)
                    {
                        Debug.Log("IAP Manager Logs: Apple originalTransactionIdentifier: " + apple.originalTransactionIdentifier);
                        Debug.Log("IAP Manager Logs: Apple subscriptionExpirationDate: " + apple.subscriptionExpirationDate);
                        Debug.Log("IAP Manager Logs: Apple cancellationDate: " + apple.cancellationDate);
                        Debug.Log("IAP Manager Logs: Apple quantity: " + apple.quantity);
                    }
                    validPurchase = true;
                }
#endif
            }
        }
        catch (IAPSecurityException)
        {
            Debug.Log("IAP Manager Logs: Invalid receipt, not unlocking content" );
            validPurchase = false;
        }
#endif
        if (validPurchase)
        {
		
				if (EnableLogging)
                    Debug.Log("IAP Manager Logs: YES!! VALID PURCHASE");
                // get your current IAP by product id
                CurrentIAP currentIAP = currentIAPs.Find((item) => item.IAPId.Equals(e.purchasedProduct.definition.id));
#if UNITY_ANDROID
                if (EnableLogging)
                    Debug.LogError($"IAP Manager Logs: Google Purchase Token Recipt { googlePurchasingToken}");
			
                try
                {
					// now ask your server to verify your purchase and give your rewards or etc.
					Networking.PurchaseIAP(currentIAP.itemId.ToString(), "BizzCo", googlePurchasingToken, currentIAP.IAPId, "com.BizzCoin.BizzCo", (int)currentIAP.IAPType +1, ( sM ) =>
					 {
						 PlayerData.Instance.GetWallet(()=>
						 {
							 GameObject.Find("Animations").transform.GetChild(1).gameObject.SetActive(true);
						 });

					 }, ( status ) => {
						 Debug.LogError(status);
					 });

				}
                catch (Exception ext)
                {
                    Debug.Log("Network Call Exception" + ext);
                }
            

#elif UNITY_IPHONE
            if (EnableLogging)
                Debug.LogError($"IAP Manager Logs: Apple Recipt {e.purchasedProduct.receipt}");

			currentIAP = currentIAPs.Find(( item ) => item.IAPId.Equals(e.purchasedProduct.definition.id));
			Networking.PurchaseIAPApple(currentIAP.itemId.ToString(), e.purchasedProduct.receipt, ( sM ) =>
				{
					PlayerData.Instance.GetWallet(()=>
					{
						GameObject.Find("Animations").transform.GetChild(1).gameObject.SetActive(true);
					});

				}, ( status ) => {
					Debug.LogError(status);
				});

#endif

		}
		return PurchaseProcessingResult.Complete;
    }
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        if (EnableLogging)
            Debug.LogError(string.Format("IAP Manager Logs: OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

	public string GetLocalizedPrice(string id)
	{
		return m_StoreController.products.WithID(id).metadata.isoCurrencyCode +"  "+ m_StoreController.products.WithID(id).metadata.localizedPrice;
	}

    [Serializable]
    public class CurrentIAP
    {	
        public string IAPName;
        public string itemId;
        public string IAPId;
        public double IAPPrice;
        public string IAPDescirption;
        public ProductType IAPType;
        public CurrentIAP(string iapName, string ItemId, string iapId, double iapPrice, string iapDiscrip)
        {
            IAPName = iapName;
            itemId = ItemId;
            IAPId = iapId;
            IAPPrice = iapPrice;
            IAPDescirption = iapDiscrip;
        }
    }
}
