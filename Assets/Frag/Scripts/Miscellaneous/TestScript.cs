﻿using System.Collections;
using System.Collections.Generic;
using GameData;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static GameData.PlayerData;

public class TestScript : ScrollRect
{

    //public override void OnScroll(PointerEventData data)
    //{
    //    //base.OnScroll(data);
    //}

    //public override void OnBeginDrag(PointerEventData eventData)
    //{
    //    //base.OnBeginDrag(eventData);
    //}

    //public override void OnDrag(PointerEventData eventData)
    //{
    //    //base.OnDrag(eventData);
    //}

    //public override void OnEndDrag(PointerEventData eventData)
    //{
    //    //base.OnEndDrag(eventData);
    //}

    public void Scroll(PointerEventData data)
    {
        base.OnScroll(data);
    }

    public void BeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
    }

    public void Drag(PointerEventData eventData)
    {
        base.OnDrag(eventData);
    }

    public void EndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
    }

    class Container
    {
        private StateData state;
        public Container()
        {
            state = new StateData { Equipped = false, ID = "ID", Key = "Key" };
        }

        //public readonly ref StateData GetState()
        //{
        //    return ref State;
        //}

        public StateData State { get => state; }
    }

    void Start()
    {
        //Container container = new Container();
        //HeroState hero = new HeroState(HeroID.BizzCo, container.State, 25);

        //Debug.Log(hero.State.Level);
        //hero.Upgrade();
        //Debug.Log(hero.State.Level);

        //Debug.Log(container.State.Equipped);
    }
}
