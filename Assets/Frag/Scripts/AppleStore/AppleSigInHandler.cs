﻿using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using System;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class AppleSigInHandler
{
    public string TokenId { protected set; get; }
    public string DisplayName { get => PlayerPrefs.GetString("DisplayName"); }
    public IAppleError LastError { protected set; get; }
    public CredentialState CurrentState { protected set; get;}
    public bool IsInitialized { protected set; get; }
    public event Action OnAppleResponse;

    private IAppleAuthManager _appleAuthManager;

    public AppleSigInHandler()
    {
        IsInitialized = false;
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            var deserializer = new PayloadDeserializer();
            this._appleAuthManager = new AppleAuthManager(deserializer);
            TokenId = string.Empty;
            CurrentState = CredentialState.NotFound;
            IsInitialized = true;
        }
    }

    async Task CheckForCallBacks()
    {
        while (this._appleAuthManager != null)
        {
            this._appleAuthManager.Update();
            await Task.Delay(500);
        }
    }

    public void LoginWithApple()
    {
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        try
        {
            _appleAuthManager.LoginWithAppleId(loginArgs,
                credential =>
                {
                    var appleIdCredential = credential as IAppleIDCredential;
                    TokenId = Encoding.UTF8.GetString(appleIdCredential.IdentityToken);
                    if (!PlayerPrefs.HasKey("DisplayName"))
                    {
                        //You get name only on first time the user logs in after that only token id is returned so save it for furture use
                        //Apple will not provide name if it has been already given on first login
                        //If you try to access when its not provided Apple wont throw an error and wont even let you login
                        PlayerPrefs.SetString("DisplayName", (appleIdCredential.FullName as PersonName).GivenName);
                    }
                    CheckCredentialStatusForUserId(appleIdCredential.User);
                },
                error =>
                {
                    Debug.Log("Sign in with Apple failed " + (error as AppleError).ToString());
                    LastError = error;
                    OnAppleResponse?.Invoke();
                });
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        CheckForCallBacks();
    }
    private void CheckCredentialStatusForUserId(string appleUserId)
    {
        this._appleAuthManager.GetCredentialState(
            appleUserId,
            state =>
            {
                CurrentState = state;
                OnAppleResponse?.Invoke();
            },
            error =>
            {
                Debug.LogError("Apple Sign in: Error while trying to get credential state " + error.ToString());
                LastError = error;
                OnAppleResponse?.Invoke();
            });
    }
}
