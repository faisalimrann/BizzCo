﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;


public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    public string UnityIdGoogle = "1234567";
    public string UnityIdApple = "1234567";
	public bool TestMode;
	private Action rewardAction;
	private Action failureAction;
	private Action cancelAction;
	private static readonly Lazy<AdsManager> instance = new Lazy<AdsManager>(() => new AdsManager());

	public static AdsManager Instance { get { return instance.Value; } }

	private static bool isInitialized = false;
	public void OnUnityAdsDidError(string message)
    {
        Debug.Log($"Unity Ads: OnUnityAdsDidError {message}");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        Debug.Log($"Unity Ads: OnUnityAdsDidFinish {placementId} ::::: {showResult}");
        if (showResult == ShowResult.Finished)
        {
			AdsManager.Instance.rewardAction?.Invoke();
			AdsManager.Instance.rewardAction = null;
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
			cancelAction?.Invoke();
			// Do not reward the user for skipping the ad.
		}
		else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
			cancelAction?.Invoke();
        }
    }

    public void OnUnityAdsDidStart(string _placementId)
    {
        Debug.Log($"Unity Ads: OnUnityAdsDidStart {_placementId}");
    }

    public void OnUnityAdsReady(string _placementId)
    {
        Debug.Log($"Unity Ads: OnUnityAdsReady {_placementId}");

    }

	private void Awake()
	{
		if ( isInitialized )
		{
			DestroyImmediate(this.gameObject);
		}
		else
		{
			isInitialized = true;
		}
	}
	// Start is called before the first frame update
	private void Start()
    {

#if UNITY_ANDROID
		Advertisement.AddListener(this);
        Advertisement.Initialize(UnityIdGoogle, TestMode);
#elif UNITY_IOS
		Advertisement.AddListener(this);
        Advertisement.Initialize(UnityIdApple, TestMode);
#endif
		DontDestroyOnLoad(this);
		//StartCoroutine(ShowBannerWhenReady());
	}

    public void showRewardedAd(Action successCallback, Action failureCallback, Action AdCancelled )
    {
        Debug.Log("Unity Ads Rewarded");
		rewardAction = successCallback;
		failureAction = failureCallback;
		cancelAction = AdCancelled;
		if ( Advertisement.IsReady("rewardedVideo"))
        {
            Advertisement.Show("rewardedVideo");
        }
        else
        {
            Debug.LogError("YO Man Ad not Ready");
			failureAction?.Invoke();
        }
    }
	public void showVideoAd()
	{
		Debug.Log("Video Ad");
		if ( Advertisement.IsReady("video") )
		{
			Advertisement.Show("video");
		}
		else
		{
			Debug.LogError("YO Man Ad not Ready");
		}
	}
	public void ShowBannerAd()
	{
		Debug.Log("Banner Ad");
		if ( Advertisement.IsReady("bannerPlacement") )
		{
			Advertisement.Show("bannerPlacement");
		}
		else
		{
			Debug.LogError("YO Man Ad not Ready");
		}
	}
}
