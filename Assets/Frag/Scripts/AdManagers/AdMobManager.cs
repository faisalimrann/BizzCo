﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

public class AdMobManager : MonoBehaviour
{

	void Start()
	{
		DontDestroyOnLoad(this);
		MobileAds.Initialize(initStatus => { });
	}

	private static readonly Lazy<AdMobManager> instance = new Lazy<AdMobManager>(() => new AdMobManager());

	public static AdMobManager Instance { get { return instance.Value; } }


#region ADMOB_REGION

	private  InterstitialAd Interstitial;
	private  BannerView bannerView, DBannerView;
	private  AdRequest GBannerRequest, InterstitialRequest, RewarededRequest;
	private  AdRequest RequestRewarded;
	private  RewardBasedVideoAd RewardBasedVideo;
	private bool AdmobBannerHasLoaded;

	AdPosition GeneralAdmobBannerAdPostion = AdPosition.Bottom;

	public string AdmobBannerId = "test";

	public string AdmobInterstitialId = "test";

	public string AdmobRewardedId = "test";

	#region ADMOB_BANNER_AD

	private void RequestAdmobBanner()
	{
		try
		{
#if UNITY_ANDROID
			AdmobBannerId = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IPHONE
            AdmobBannerId = "ca-app-pub-3940256099942544/2934735716";
#else
            AdmobBannerId = "unexpected_platform";
#endif
			bannerView = new  BannerView(AdmobBannerId,  AdSize.Banner, GeneralAdmobBannerAdPostion);
			bannerView.OnAdLoaded += HandleOnAdLoaded;
			bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
			GBannerRequest = new  AdRequest.Builder().Build();
			bannerView.LoadAd(GBannerRequest);
		}
		catch ( Exception ex )
		{
			Debug.LogError(ex);
		}
	}

	private void HandleOnAdFailedToLoad( object sender, AdFailedToLoadEventArgs e )
	{
		AdmobBannerHasLoaded = false;
	}

	private void HandleOnAdLoaded( object sender, EventArgs e )
	{
		AdmobBannerHasLoaded = true;
	}

	public void ShowAdmobBannerAd()
	{
		try
		{
			if ( AdmobBannerHasLoaded )
			{
				bannerView.Show();
			}
			RequestAdmobBanner();
		}
		catch ( Exception ex )
		{
			Debug.LogError(ex);
		}
	}


	public void DestoryAdmobBannerAd()
	{
		bannerView.Hide();
		bannerView.Destroy();
	}
	#endregion

	#region ADMOB_INTERSTITIAL_AD

	private void RequestAdmobInterstitialAd()
	{
		try
		{
#if UNITY_ANDROID
			AdmobInterstitialId = "ca-app-pub-3940256099942544/1033173712";
#elif UNITY_IPHONE
            AdmobInterstitialId = "ca-app-pub-3940256099942544/4411468910";
#else
           AdmobInterstitialId = "unexpected_platform";
#endif
			Interstitial = new  InterstitialAd(AdmobInterstitialId);
			InterstitialRequest = new  AdRequest.Builder().Build();
			Interstitial.LoadAd(InterstitialRequest);
		}
		catch ( Exception ex )
		{
			Debug.LogError(ex);
		}
	}

	public void ShowAdmobInterstitialAd()
	{
		try
		{
			if ( Interstitial.IsLoaded() )
			{
				Interstitial.Show();
				RequestAdmobInterstitialAd();
			}
			RequestAdmobInterstitialAd();
		}
		catch ( Exception ex )
		{
			Debug.LogError(ex);
		}
	}


	#endregion

	#region ADMOB_REWARDED_AD

	private void RequestAdmobRewardedAd()
	{
#if UNITY_ANDROID
		AdmobRewardedId = "ca-app-pub-9801386889405918/3051187759";
#elif UNITY_IPHONE
            AdmobRewardedId = "ca-app-pub-9801386889405918/5785162308";
#else
            AdmobRewardedId = "unexpected_platform";
#endif
		this.RewardBasedVideo =  RewardBasedVideoAd.Instance;		RewarededRequest = new  AdRequest.Builder().Build();
		this.RewardBasedVideo.LoadAd(RewarededRequest, AdmobRewardedId);
		RewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
		RewardBasedVideo.OnAdClosed += RewardBasedVideo_OnAdClosed; ;
		RewardBasedVideo.OnAdFailedToLoad += HandleRewardFail;
	}

	private void RewardBasedVideo_OnAdClosed( object sender, EventArgs e )
	{
		RewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
		RewardBasedVideo.OnAdClosed -= HandleRewardFail;
		RewardBasedVideo.OnAdFailedToLoad -= HandleRewardFail;
		cancelAction?.Invoke();
	}

	private void HandleRewardFail( object o, EventArgs e)
	{
		RewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
		RewardBasedVideo.OnAdClosed -= HandleRewardFail;
		RewardBasedVideo.OnAdFailedToLoad -= HandleRewardFail;
		failureAction?.Invoke();
	}
	private void HandleRewardBasedVideoRewarded( object sender, Reward e )
	{
		RewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
		RewardBasedVideo.OnAdClosed -= HandleRewardFail;
		RewardBasedVideo.OnAdFailedToLoad -= HandleRewardFail;
		OnRewardedAdShown(sender, e);
	}

	private void OnRewardedAdShown( object sender, Reward args )
	{
		string type = args.Type;
		double amount = args.Amount;
		rewardAction?.Invoke();
	}
	private Action rewardAction;
	private Action failureAction;
	private Action cancelAction;

	public void ShowAdmobRewardedAd( Action successCallback, Action failureCallback, Action canceledAction )
	{
		rewardAction = successCallback;
		failureAction = failureCallback;
		cancelAction = canceledAction;
		ShowVidAd();
	}

	void ShowVidAd()
	{
		try
		{
			if ( RewardBasedVideo != null && RewardBasedVideo.IsLoaded() )
			{
				RewardBasedVideo.Show();
				RequestAdmobRewardedAd();
			}
			else
			{
				RequestAdmobRewardedAd();
				failureAction?.Invoke();
			}
		}
		catch ( Exception ex )
		{
			Debug.LogError(ex);
			failureAction?.Invoke();
		}
	}
	#endregion

	#endregion
}
