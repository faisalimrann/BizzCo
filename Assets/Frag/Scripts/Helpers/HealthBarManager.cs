﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarManager : MonoBehaviour
{
	[SerializeField]
	Image fillImage;
	[SerializeField]
	Text healthText;
	[SerializeField]
	Text damageText;

	Transform lookAt; 

	private void Start()
	{
		lookAt = GameObject.FindGameObjectWithTag("LookAt").transform;
	}

	public void UpdateHealthBar(float remainingPercentage, string  _damageText = "",string _healthText = "")
	{
		fillImage.fillAmount = remainingPercentage;
		healthText.text = _healthText;
		damageText.text = _damageText;
		StopAllCoroutines();
		StartCoroutine("SetDamageOff");
	}

	IEnumerator SetDamageOff()
	{
		yield return new WaitForSeconds(1.5f);
		damageText.text = "";
	}


	private void LateUpdate()
	{
		transform.LookAt(lookAt);
		transform.Rotate(0, 180, 0);
	}

}
