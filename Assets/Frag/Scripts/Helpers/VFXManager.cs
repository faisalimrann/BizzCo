﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VFX {
	Heal,
	DamageTaken,
	DamageTakenFromThorn,
	Death,
	Shot

}

public class VFXManager : MonoBehaviour
{
	[SerializeField]
	ParticleSystem Heal;
	[SerializeField]
	ParticleSystem DamageTaken;
	[SerializeField]
	ParticleSystem DamageTakenFromThorn;
	[SerializeField]
	ParticleSystem Death;
	[SerializeField]
	ParticleSystem Shot;


	public void PlayVFX( VFX vFX )
	{
		if ( vFX == VFX.Heal )
		{
			Heal.Play();
		}
		else if ( vFX == VFX.DamageTaken )
		{
			if ( DamageTaken != null )
			{
				DamageTaken.Play();
			}
		}
		else if ( vFX == VFX.DamageTakenFromThorn )
		{
			DamageTakenFromThorn.Play();
		}
		else if ( vFX == VFX.Death )
		{
			if ( DamageTaken != null )
				DamageTaken.Stop();

			Death.gameObject.SetActive(true);
			Death.Play();
		}
		else if ( vFX == VFX.Shot )
		{
			Shot.Stop();
			Shot.Play();
		}

	}

	public void Stop( VFX vFX )
	{
		if ( vFX == VFX.Heal )
		{
			Heal.Stop();
		}
		else if ( vFX == VFX.DamageTaken )
		{
				DamageTaken.Stop();
		}
		else if ( vFX == VFX.DamageTakenFromThorn )
		{
			DamageTakenFromThorn.Stop();
		}
		else if ( vFX == VFX.Death )
		{
			Death.gameObject.SetActive(false);
			Death.Stop();
		}
		else if ( vFX == VFX.Shot )
		{
			Shot.Stop();
		}

	}

}
