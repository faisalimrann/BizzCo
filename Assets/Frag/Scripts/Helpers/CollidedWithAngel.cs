﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidedWithAngel : MonoBehaviour
{
	public delegate void CollidedWithAngelEvent();
	public static event CollidedWithAngelEvent onCollidedWithAngelEvent;

	[SerializeField] GameObject canvas;

	bool isCollided = false;

	private void Start()
	{
		GameManager.onAfterCollidedWithAngelEvent += GameManager_onAfterCollidedWithAngelEvent;
	}

	private void GameManager_onAfterCollidedWithAngelEvent()
	{
		this.transform.parent.gameObject.SetActive(false);
		canvas.SetActive(false);
	}

	private void OnTriggerEnter( Collider other )
	{
		if ( !isCollided )
		{
			onCollidedWithAngelEvent?.Invoke();
			isCollided = true;
		}
	}

	private void OnDestroy()
	{
		GameManager.onAfterCollidedWithAngelEvent -= GameManager_onAfterCollidedWithAngelEvent;
	}
}
