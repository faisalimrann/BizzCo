﻿
using System.Collections.Generic;
using UnityEditor;

namespace GameData
{

    public struct StateData
    {
        public string ID; // for mapping back to Gamestate
        public string Key; // for mapping to Metadata
        public uint Level;
        public bool Equipped;

        public void Equip()
        {
            Equipped = true;
        }
    }

    public class GameState
    {
        public string Gamestate = "{\"Satoshi\":1000,\"UnlockedItems\":[{\"ID\":\"1\",\"Key\":\"BizzCo\",\"Level\":1,\"Equipped\":true},{\"ID\":\"2\",\"Key\":\"CommonRifle\",\"Level\":1,\"Equipped\":true},{\"ID\":\"3\",\"Key\":\"Mask\",\"Level\":1,\"Equipped\":true},{\"ID\":\"4\",\"Key\":\"ClassicArmour\",\"Level\":1,\"Equipped\":true},{\"ID\":\"5\",\"Key\":\"BrownBoots\",\"Level\":1,\"Equipped\":true},{\"ID\":\"6\",\"Key\":\"Helm\",\"Level\":2,\"Equipped\":false}]}";
        public uint Satoshi;

        public List<StateData> UnlockedItems;



        //public Dictionary<string, Upgradeable> UnlockedHeroes;
        //public Dictionary<string, Upgradeable> UnlockedWeapons;
        //public Dictionary<string, Upgradeable> UnlockedEquipments;

    }




}