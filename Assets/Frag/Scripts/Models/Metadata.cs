﻿namespace GameData
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public enum EnemyID
    {
        WarRobot = 0,
        AlienSerpent,
        AlienSickus,
        AlienMagantis,
        AlienHorror,
        BigAlien,
        AlienUlifo,
        Angel
    }

    public enum AbilityID
    {
        FrontShot,
        SideShot,
        DiagonalShot,
        RearShot,
        Multishot,
        AttackBoost,
        HealthBoost,
        Heal,
        Crit
    }

    public enum WeaponID
    {
        CommonRifle,
        RareRifle,
        EpicRifle
    }

    public enum HeroID
    {
        BizzCo,
        SpaceRobot
    }

    public enum EquipmentID
    {
        Mask,
        Helm,
        Helmet,
        DuneArmour,
        ClassicArmour,
        DexterityArmour,
        BrownBoots,
        BootsofSpeed,
        SeraphTracers
    }

    public enum EquipmentType
    {
        Head,
        Chest,
        Legs
    }


    public enum InventoryItemType
    {
        UnKnown,
        Hero,
        Weapon,
        Equipment
    }

    public partial struct Metadata
    {
        [JsonProperty("Abilities")]
        public Dictionary<AbilityID, AbilityData> Abilities { get; set; }

        [JsonProperty("Enemies")]
        public Dictionary<EnemyID, EnemyData> Enemies { get; set; }

        [JsonProperty("WeaponUpgrade")]
        public Dictionary<WeaponID, WeaponData> Weapons { get; set; }

        [JsonProperty("HeroUpgrade")]
        public Dictionary<HeroID, HeroData> Heroes { get; set; }

        [JsonProperty("EquipmentUpgrade")]
        public Dictionary<EquipmentID, EquipmentData> Equipment { get; set; }

        [JsonProperty("Chapters")]
        public List<ChapterData> Chapters { get; set; }

        [JsonProperty("Chest_Specs")]
        public List<ChestSpec> ChestSpecs { get; set; }

        [JsonProperty("Item_Probs")]
        public List<ItemProb> ItemProbs { get; set; }

        [JsonProperty("TitleData")]
        public TitleData TitleData { get; set; }

        [JsonProperty("IAP")]
        public Dictionary<string, IAP> IAP { get; set; }
    }

    public partial class AbilityData
    {
        [JsonProperty("ability_Name")]
        public string AbilityName { get; set; }

        [JsonProperty("ability_Description")]
        public string AbilityDescription { get; set; }
    }

    public partial class ChapterData
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Stages")]
        public List<StageData> Stages { get; set; }
    }

    public partial class StageData
    {
        [JsonProperty("Stage_No")]
        public uint StageNo { get; set; }

        [JsonProperty("satoshi_reward")]
        public uint SatoshiReward { get; set; }

        [JsonProperty("energy_reward")]
        public uint EnergyReward { get; set; }

        [JsonProperty("xp_reward")]
        public uint XpReward { get; set; }

        [JsonProperty("obstacles")]
        public List<ObstacleData> Obstacles { get; set; }

        [JsonProperty("monsters")]
        public List<MonsterData> Monsters { get; set; }
    }

    public partial class MonsterData
    {
        [JsonProperty("monsterId")]
        public EnemyID MonsterId { get; set; }

        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("damageFactor")]
        public uint DamageFactor { get; set; }

        [JsonProperty("healthFactor")]
        public uint HealthFactor { get; set; }
    }

    public struct Position
    {
        [JsonProperty("x")]
        public uint X { get; set; }

        [JsonProperty("y")]
        public uint Y { get; set; }

        public Position(Position Input)
        {
            X = Input.X;
            Y = Input.Y;
        }

        public UnityEngine.Vector3 ToMapPosition()
        {
            return new UnityEngine.Vector3(X,0,Y);
        }

        public static bool operator ==(Position a, Position b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Position a, Position b)
        {
            return !(a==b);
        }

        public static bool operator <=(Position a, Position b)
        {
            return a.X <= b.X || a.Y <= b.Y;
        }

        public static bool operator >=(Position a, Position b)
        {
            return a.X >= b.X || a.Y >= b.Y;
        }

        public override string ToString()
        {
            return string.Format("({0},{1})",X,Y);
        }
    }

    public partial class ObstacleData
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("min_value")]
        public Position MinValue { get; set; }

        [JsonProperty("max_value")]
        public Position MaxValue { get; set; }

        [JsonProperty("damage")]
        public uint Damage { get; set; }
    }

    public partial class EnemyData
    {
        [JsonProperty("enemy_id")]
        public uint EnemyId { get; set; }

        [JsonProperty("enemy_type")]
        public string EnemyType { get; set; }

        [JsonProperty("enemy_Tier")]
        public uint EnemyTier { get; set; }

        [JsonProperty("enemy_Damage")]
        public uint EnemyDamage { get; set; }

        [JsonProperty("enemy_Health")]
        public uint EnemyHealth { get; set; }

        [JsonProperty("enemy_attackSpeed")]
        public uint EnemyAttackSpeed { get; set; }

        [JsonProperty("satoshi_reward")]
        public uint SatoshiReward { get; set; }
    }

    public partial class EquipmentData
    {
        [JsonProperty("equipment_Name")]
        public string EquipmentName { get; set; }

        [JsonProperty("equipment_Type")]
        public EquipmentType EquipmentType { get; set; }

        [JsonProperty("Rarity")]
        public Rarity Rarity { get; set; }

        [JsonProperty("equipment_Level")]
        public Dictionary<string, EquipmentLevel> EquipmentLevel { get; set; }
		
		[JsonProperty("equipment_Description")]
		public string EquipmentDescription { get; set; }

	}

    public partial class EquipmentLevel
    {
        [JsonProperty("Damage")]
        public uint Damage { get; set; }

        [JsonProperty("equipment_Health")]
        public uint EquipmentHealth { get; set; }

        [JsonProperty("attack_Speed")]
        public uint AttackSpeed { get; set; }

        [JsonProperty("upgrade_Cost")]
        public uint UpgradeCost { get; set; }
    }

    //public partial class BrownBoots
    //{
    //    [JsonProperty("equipment_Name")]
    //    public string EquipmentName { get; set; }

    //    [JsonProperty("equipment_Type")]
    //    public string EquipmentType { get; set; }

    //    [JsonProperty("Rarity", NullValueHandling = NullValueHandling.Ignore)]
    //    public Rarity? Rarity { get; set; }

    //    [JsonProperty("equipment_Level")]
    //    public Dictionary<string, EquipmentLevel> EquipmentLevel { get; set; }

    //    [JsonProperty("rarity", NullValueHandling = NullValueHandling.Ignore)]
    //    public Rarity? BrownBootsRarity { get; set; }
    //}

    //public partial class HeroUpgrade
    //{
    //    [JsonProperty("BizzCo")]
    //    public BizzCo BizzCo { get; set; }

    //    [JsonProperty("SpaceRobot")]
    //    public BizzCo SpaceRobot { get; set; }
    //}

    public partial class HeroData
    {
        [JsonProperty("hero_Name")]
        public string HeroName { get; set; }

        [JsonProperty("hero_Level")]
        public Dictionary<string, HeroLevel> HeroLevel { get; set; }
    }

    public partial class HeroLevel
    {
        [JsonProperty("hero_Health")]
        public uint HeroHealth { get; set; }

        [JsonProperty("hero_Damage")]
        public uint HeroDamage { get; set; }

        [JsonProperty("hero_UpgradeCost")]
        public uint HeroUpgradeCost { get; set; }
    }

    //public partial class WeaponUpgrade
    //{
    //    [JsonProperty("CommonRifle")]
    //    public Rifle CommonRifle { get; set; }

    //    [JsonProperty("RareRifle")]
    //    public Rifle RareRifle { get; set; }

    //    [JsonProperty("EpicRifle")]
    //    public Rifle EpicRifle { get; set; }
    //}

    public partial class WeaponData
    {
        [JsonProperty("weapon_Name")]
        public string WeaponName { get; set; }

        [JsonProperty("weapon_Type")]
        public string WeaponType { get; set; }

        [JsonProperty("weapon_Rarity")]
        public Rarity WeaponRarity { get; set; }

        [JsonProperty("rifleLevels")]
        public Dictionary<string, WeaponLevel> WeaponLevels { get; set; }

		[JsonProperty("weapon_Description")]
		public string WeaponDescription { get; set; }
	}

    public partial class WeaponLevel
    {
        [JsonProperty("weapon_Damage")]
        public uint WeaponDamage { get; set; }

        [JsonProperty("_BaseAttackSpeed")]
        public string BaseAttackSpeed { get; set; }

        [JsonProperty("_CritcalChance%")]
        public string CritcalChance { get; set; }

        [JsonProperty("_CriticalDamgeRate")]
        public uint CriticalDamgeRate { get; set; }

        [JsonProperty("_CriticalDamage")]
        public uint CriticalDamage { get; set; }

        [JsonProperty("upgrade_Cost")]
        public uint UpgradeCost { get; set; }

        [JsonProperty("weapon_knockback")]
        public uint WeaponKnockback { get; set; }
    }

    public partial class ChestSpec
    {
        [JsonProperty("chest_Type")]
        public string ChestType { get; set; }

        [JsonProperty("reward_Type")]
        public string RewardType { get; set; }

        [JsonProperty("reward_Amount")]
        public uint RewardAmount { get; set; }

        [JsonProperty("common_Chance")]
        public uint CommonChance { get; set; }

        [JsonProperty("rare_Chance")]
        public uint RareChance { get; set; }

        [JsonProperty("epic_Chance")]
        public uint EpicChance { get; set; }

        [JsonProperty("rarity_Limit")]
        public Rarity RarityLimit { get; set; }

        [JsonProperty("chest_opening_timer")]
        public uint ChestOpeningTimer { get; set; }

        [JsonProperty("ads_required")]
        public string AdsRequired { get; set; }

        [JsonProperty("buy_chest")]
        public uint BuyChest { get; set; }

        [JsonProperty("after_ad")]
        public uint AfterAd { get; set; }

        [JsonProperty("discount_limit")]
        public uint DiscountLimit { get; set; }
    }

    public partial class IAP
    {
        [JsonProperty("package_Name")]
        public string PackageName { get; set; }

        [JsonProperty("package_price")]
        public uint PackagePrice { get; set; }

        [JsonProperty("package_quantity")]
        public uint PackageQuantity { get; set; }
    }

    public partial class ItemProb
    {
        [JsonProperty("equipment_Type")]
        public string EquipmentType { get; set; }

        [JsonProperty("Item_List")]
        public string ItemList { get; set; }

        [JsonProperty("item_rarity")]
        public Rarity ItemRarity { get; set; }

        [JsonProperty("golden_prob")]
        public double GoldenProb { get; set; }

        [JsonProperty("obsidian_prob")]
        public double ObsidianProb { get; set; }
    }

    public partial class TitleData
    {
        [JsonProperty("BizzcoinThreshold")]
        public uint BizzcoinThreshold { get; set; }

        [JsonProperty("XPThreshold")]
        public uint XpThreshold { get; set; }

        [JsonProperty("EnergyRecharge")]
        public long EnergyRecharge { get; set; }

        [JsonProperty("EnergyTotal")]
        public uint EnergyTotal { get; set; }

        [JsonProperty("EnergyCost")]
        public uint EnergyCost { get; set; }

        [JsonProperty("EnergyAdLimit")]
        public uint EnergyAdLimit { get; set; }

        [JsonProperty("EnergyAdValue")]
        public uint EnergyAdValue { get; set; }

        [JsonProperty("EnergyRefillCost")]
        public uint EnergyRefillCost { get; set; }

        public uint MinWithdrawal;
        public uint RevivalCount;
    }

    public enum Rarity { Common, Rare, Epic };

}
