﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using System;

public class VolumeController : MonoBehaviour
{
    public Slider SFX;
    public Slider BGM;
    private void Awake()
    {
        SFX.value = SoundManager.effectsVolume;
        BGM.value = SoundManager.backgroundVolume;

        SceneManager.sceneLoaded += SceneManager_activeSceneChanged;
        SFX.onValueChanged.AddListener(
        delegate { SetEfXVolume(); }
       ) ;
        BGM.onValueChanged.AddListener(
            delegate { SetBkgVolume(); }
            );

		DontDestroyOnLoad(this);
    }

    private void SceneManager_activeSceneChanged( Scene arg0, LoadSceneMode mode )
    {
		
			SliderRefrences sR = GameObject.FindObjectOfType<SliderRefrences>();
			SFX = sR.SFX;
			BGM = sR.BGM;
		SFX.value = SoundManager.effectsVolume;
		BGM.value = SoundManager.backgroundVolume;
		SFX.onValueChanged.AddListener(
		delegate { SetEfXVolume(); }
	   );
		BGM.onValueChanged.AddListener(
			delegate { SetBkgVolume(); }
			);
	}

    void SetEfXVolume( )
    {
        SoundManager.effectsVolume = SFX.value;
    }
    void SetBkgVolume()
    {
        SoundManager.backgroundVolume = BGM.value;
    }

	private async void OnApplicationFocus( bool focus )
	{
		if ( focus )
		{
			await WaitForSeconds();
			if ( SoundManager.GetSoundPlaying("Main Menu").Length == 0 )
			{
				Debug.LogError("Main Menu sound not playing");
				SoundManager.Stop("Main Menu");
				SoundManager.Play("Main Menu");
			}
		}
	}

	private  void OnApplicationPause( bool pause )
	{
		if ( !pause )
		{
			StartCoroutine("restartBGM");
		}
	}

	IEnumerator restartBGM()
	{
		yield return null;
		yield return new WaitForSeconds(1f);
		if ( SoundManager.GetSoundPlaying("Main Menu").Length == 0 )
		{
			SoundManager.Stop("Main Menu");
			SoundManager.Play("Main Menu");
		}
	}
	async Task WaitForSeconds()
	{
		await Task.Delay(1000);
	}

}


