﻿using System;
using Newtonsoft.Json;
using GameData;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using RoboArchero.Networking;

public class DataManager
{
    #region Singleton

    private DataManager() { Initialize(); }

    private static readonly Lazy<DataManager> instance = new Lazy<DataManager>(() => new DataManager());

    public static DataManager Instance { get { return instance.Value; } }

    #endregion

    public string MetadataString = "";
    Metadata GameData;

	public void Initialize()
    {
		//Networking.OnUserConnected += GetMetaData;
		//TextAsset textAsset = (TextAsset)Resources.Load("Metadata");
		//GameData = JsonConvert.DeserializeObject<Metadata>(textAsset.text);
		//GetMetaData();
    }

	public void GetMetaData()
	{
		Networking.GetMetaData((serverMessage)=>
		{
			//Debug.LogError(serverMessage.FetchMetadataResponse.Metadata.MetaData_);
			GameData = JsonConvert.DeserializeObject<Metadata>(serverMessage.FetchMetadataResponse.Metadata.MetaData_);
			PlayerData.Instance.GetGameData();
			//OnGameDataRecieved?.Invoke();
		}, (status)=>
		{
			Debug.LogError(status);
		});
	}


    public TitleData TitleData()
    {
        return GameData.TitleData;
    }


	public AbilityData GetAbilityData( AbilityID abilityID )
	{
		AbilityData outData;
		GameData.Abilities.TryGetValue(abilityID, out outData);
		return outData;
	}

    public uint TotalStages(uint Chapter)
    {
        return (uint)GameData.Chapters[(int)Chapter-1].Stages.Count;
    }

    public StageData GetStage(uint StageNo,uint Chapter = 1)
    {
        return GameData.Chapters[(int)Chapter-1].Stages[(int)StageNo];
    }

    public EnemyData GetEnemy(EnemyID EnemyID)
    {
        return GameData.Enemies[EnemyID];
    }

    public HeroData GetHero(HeroID HeroID)
    {
        return GameData.Heroes[HeroID];
    }

    public HeroLevel GetHeroLevel(HeroID HeroID, uint Level)
    {
        return GameData.Heroes[HeroID].HeroLevel[Level.ToString()];
    }

    public GameData.WeaponData GetWeapon(WeaponID WeaponID)
    {
        return GameData.Weapons[WeaponID];
    }

    public WeaponLevel GetWeaponLevel(WeaponID WeaponID, uint Level)
    {
        return GameData.Weapons[WeaponID].WeaponLevels[Level.ToString()];
    }

    public EquipmentData GetEquipment(EquipmentID EquipmentID)
    {
        return GameData.Equipment[EquipmentID];
    }

    public EquipmentLevel GetEquipmentLevel(EquipmentID EquipmentID, uint Level)
    {
        return GameData.Equipment[EquipmentID].EquipmentLevel[Level.ToString()];
    }

    public InventoryItemType GetItemType(string ID)
    {
        InventoryItemType ToReturn;
        if (Enum.IsDefined(typeof(HeroID), ID))
            ToReturn = InventoryItemType.Hero;
        else if (Enum.IsDefined(typeof(WeaponID), ID))
            ToReturn = InventoryItemType.Weapon;
        else if (Enum.IsDefined(typeof(EquipmentID), ID))
            ToReturn = InventoryItemType.Equipment;
        else
            ToReturn = InventoryItemType.UnKnown;

        return ToReturn;
    }

    public IEnumerable<EquipmentData> GetAllArmours()
    {
        return GameData.Equipment.Values.Where(arg => arg.EquipmentType == EquipmentType.Chest);
    }

    public IEnumerable<EquipmentData> GetAllHelmets()
    {
        return GameData.Equipment.Values.Where(arg => arg.EquipmentType == EquipmentType.Head);
    }
    public IEnumerable<EquipmentData> GetAllBoots()
    {
        return GameData.Equipment.Values.Where(arg => arg.EquipmentType == EquipmentType.Legs);
    }

    public List<IAP> GetIAPs()
    {
        return GameData.IAP.Values.ToList();
    }
}