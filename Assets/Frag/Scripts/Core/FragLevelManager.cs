﻿
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using GameData;
using UnityEngine;

public class FragLevelManager : LevelsManager
{
    public TextAsset LevelsText;
    public Transform StartingTile;
    public PlayerManager Player;
    public PlayerManager SpaceRobot;
    Metadata Data;
    public override int GetLevels()
    {
        return (int)GetLevels(0);
    }

    public uint GetLevels(uint ChapterNo)
    {
        return DataManager.Instance.TotalStages(ChapterNo);
    }

    public override void LoadLevel(out List<Obstacle> obstacles, out List<Enemy> enemies)
    {
        Debug.Log("Loading Level:" + Level);

		if(PlayerData.Instance.CurrentHero.HeroKeyID == HeroID.BizzCo)
			Player.transform.position = StartingTile.position + new Vector3(5f, 0.1f, 0f);
		else
			SpaceRobot.transform.position = StartingTile.position + new Vector3(5f, 0.1f, 0f);

		var CurrentStage = DataManager.Instance.GetStage(Level);
        obstacles = new List<Obstacle>();

        for (int i = 0; i < CurrentStage.Obstacles.Count; i++)
        {
            if (CurrentStage.Obstacles[i].Position.X == 0 && CurrentStage.Obstacles[i].Position.Y == 0)
            {
                Position StartingPosition = CurrentStage.Obstacles[i].MinValue;
                Position EndingPosition = CurrentStage.Obstacles[i].MaxValue;
				Position TemporaryPosition = new Position();
                //obstacles.Add(Instantiate(Resources.Load<Obstacle>("Prefabs/Obstacles/" + CurrentStage.Obstacles[i].Name), StartingTile.position + StartingPosition.ToMapPosition(), Quaternion.identity));
				for ( int j = (int)StartingPosition.X; j <= EndingPosition.X; j++ )
				{
					for ( int k = (int)StartingPosition.Y; k <= EndingPosition.Y; k++ )
					{

						TemporaryPosition.X = (uint)Mathf.Clamp(j, 0, CurrentStage.Obstacles[i].MaxValue.X);
						TemporaryPosition.Y = (uint)Mathf.Clamp(k, 0, CurrentStage.Obstacles[i].MaxValue.Y);
						Obstacle temp = Instantiate(Resources.Load<Obstacle>("Prefabs/Obstacles/" + CurrentStage.Obstacles[i].Name), StartingTile.position + TemporaryPosition.ToMapPosition(), Quaternion.identity);
						if ( temp is Thorn )
						{
							(temp as Thorn).Damage = (int)CurrentStage.Obstacles[i].Damage;
						}
						obstacles.Add(temp);

					}

				}
                    //Debug.Log(CurrentStage.Obstacles[i].Name + StartingPosition);
            }
            else
            {
                obstacles.Add(Instantiate(Resources.Load<Obstacle>("Prefabs/Obstacles/" + CurrentStage.Obstacles[i].Name), StartingTile.position + CurrentStage.Obstacles[i].Position.ToMapPosition(), Quaternion.identity));
            }
        }
        enemies = new List<Enemy>();
        for (int i = 0; i < CurrentStage.Monsters.Count; i++)
        {
            EnemyData ED = DataManager.Instance.GetEnemy(CurrentStage.Monsters[i].MonsterId);

            //Debug.Log(ED.EnemyDamage);
			Enemy Enemy = Instantiate(Resources.Load<Enemy>("Prefabs/Enemies/" + CurrentStage.Monsters[i].MonsterId.ToString()), StartingTile.position + CurrentStage.Monsters[i].Position.ToMapPosition(), Quaternion.identity);

            Enemy.SetData((int)ED.EnemyHealth, (int)ED.EnemyAttackSpeed, (int)ED.EnemyDamage, CurrentStage.Monsters[i].MonsterId);
            Enemy.Init();// Pass Enemy data in init
            enemies.Add(Enemy);

        }
    }
}
