﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RoboArchero.Networking;
using UnityEngine;

namespace GameData
{

    public class PlayerData
    {
        #region Inventory

        public WalletData Wallet;

        public HeroState CurrentHero
        {
            get
            {
                return UnlockedHeroes.Where(Hero => Hero.State.Equipped).FirstOrDefault();
            }
        }

        public WeaponState CurrentWeapon
        {
            get
            {
                return UnlockedWeapons.Where(Weapon => Weapon.State.Equipped).FirstOrDefault();
            }
        }

        public EquipmentState CurrentArmour
        {
            get
            {
                return UnlockedEquipments.Where(Equipment => Equipment.State.Equipped
                && DataManager.Instance.GetEquipment(Equipment.EquipmentID).EquipmentType == EquipmentType.Chest).FirstOrDefault();
            }
        }

        public EquipmentState CurrentHelmet
        {
            get
            {
                return UnlockedEquipments.Where(Equipment => Equipment.State.Equipped
                && DataManager.Instance.GetEquipment(Equipment.EquipmentID).EquipmentType == EquipmentType.Head).FirstOrDefault();
            }
        }

        public EquipmentState CurrentBoots
        {
            get
            {
                return UnlockedEquipments.Where(Equipment => Equipment.State.Equipped
                && DataManager.Instance.GetEquipment(Equipment.EquipmentID).EquipmentType == EquipmentType.Legs).FirstOrDefault();
            }
        }

        public List<HeroState> UnlockedHeroes;
        public List<WeaponState> UnlockedWeapons;
        public List<EquipmentState> UnlockedEquipments;

		#endregion


		#region Singleton

		public delegate void GameDataRecieved();
		public static event GameDataRecieved OnGameDataRecieved;

		public delegate void UpdateInventory();
		public static event UpdateInventory OnUpdateInventory;
		private PlayerData()
        {
           
        }

		public void GetGameData()
		{
			Networking.GetGameState(( sM ) =>
			{
				Initialize(sM.GetWalletAndInventoryResponse);
				Debug.LogError(" GameState Recieved");

			}, ( status ) =>
			{
				Debug.LogError(status);
			});

		}

		Action GetWalletAction = null;
		public void GetWallet( Action onSuccess)
		{
			GetWalletAction = onSuccess;
			Networking.GetWallet(( sM ) =>
			{
				Wallet = new WalletData((uint)sM.GetWalletResponse.Satoshi);
				OnCurrencyAmountChanged?.Invoke(Wallet.Satoshi.Amount);
				onSuccess?.Invoke();
			}, ( status ) =>
			{
				Debug.LogError(status);
			});

		}

		void Initialize(GetWalletAndInventoryResponse message)
		{
			PopulateInventory(message);
			OnGameDataRecieved?.Invoke();
		}

		public void UpdateInventoryFromServer()
		{
			Networking.GetGameState(( sM ) =>
			{
				PopulateInventory(sM.GetWalletAndInventoryResponse);
				OnUpdateInventory?.Invoke();
				Debug.LogError(" Inventory Recieved");

			}, ( status ) =>
			{
				Debug.LogError(status);
			});


		}

		void PopulateInventory(GetWalletAndInventoryResponse message)
		{
			UnlockedHeroes = new List<HeroState>();
			UnlockedWeapons = new List<WeaponState>();
			UnlockedEquipments = new List<EquipmentState>();


			Wallet = new WalletData((uint)message.GetWalletResponse.Satoshi);

			int count = 1;
			foreach ( InventoryItem state in message.GetInventoryResponse.Equipment )
			{

				EquipmentID Equipment;
				if ( Enum.TryParse(state.Name, true, out Equipment) )
				{
					StateData data = new StateData();
					data.ID = count.ToString();
					count++;
					data.Key = state.Name;
					data.Level = (uint)state.Level;
					data.Equipped = message.GetCurrentlyEquippedResponse.Head.Equals(state.Name) || message.GetCurrentlyEquippedResponse.Legs.Equals(state.Name) || message.GetCurrentlyEquippedResponse.Chest.Equals(state.Name);
					UnlockedEquipments.Add(new EquipmentState(Equipment, data, (uint)DataManager.Instance.GetEquipment(Equipment).EquipmentLevel.Count));
				}
			}
			foreach ( InventoryItem state in message.GetInventoryResponse.Weapons )
			{

				WeaponID Weapon;
				if ( Enum.TryParse(state.Name, true, out Weapon) )
				{
					StateData data = new StateData();
					data.ID = count.ToString();
					count++;
					data.Key = state.Name;
					//TODO Fix this
					data.Level = (uint)state.Level;
					data.Equipped = message.GetCurrentlyEquippedResponse.Arms.Equals(state.Name);
					UnlockedWeapons.Add(new WeaponState(Weapon, data, (uint)DataManager.Instance.GetWeapon(Weapon).WeaponLevels.Count));
				}
			}
			foreach ( InventoryItem state in message.GetInventoryResponse.Heros )
			{
				HeroID Hero;

				if ( Enum.TryParse(state.Name, true, out Hero) )
				{
					StateData data = new StateData();
					data.ID = count.ToString();
					count++;
					data.Key = state.Name;
					data.Level = (uint)state.Level;
					data.Equipped = state.Name.Equals(message.GetSelectedHeroResponse.Name);
					UnlockedHeroes.Add(new HeroState(Hero, data, (uint)DataManager.Instance.GetHero(Hero).HeroLevel.Count));
				}
			}
		}

        private static readonly Lazy<PlayerData> instance = new Lazy<PlayerData>(() => new PlayerData());

        public static PlayerData Instance { get { return instance.Value; } }

        #endregion

        #region Helper Classes

        public abstract class Upgradeable
        {
            protected Upgradeable(StateData state, uint MaxLevel)
            {
                this.State = state;
                this.MaxLevel = MaxLevel;
            }

            public StateData State { set; get; }

            public uint MaxLevel;
            public bool IsUpgradable
            {
                get
                {
                    return State.Level < MaxLevel;
                }
            }
            public abstract uint UpgradeCost { get; }
            public virtual void Upgrade()
            {
				if ( State.Level < MaxLevel )
				{
					PlayerData.Instance.Wallet.Satoshi.Use(UpgradeCost);
					State = new StateData { ID = State.ID, Key = State.Key, Level = State.Level + 1, Equipped = State.Equipped };
				}
            }
            public virtual void Equip()
            {
                State = new StateData { ID = State.ID, Key = State.Key, Level = State.Level, Equipped = true };
            }
            public virtual void UnEquip()
            {
                State = new StateData { ID = State.ID, Key = State.Key, Level = State.Level, Equipped = false };
            }
        }

        public class HeroState : Upgradeable
        {
            HeroID HeroID;

            public HeroState(HeroID ID, StateData State, uint MaxLevel) : base(State, MaxLevel)
            {
                HeroID = ID;
            }

			public override uint UpgradeCost => UpgradeCostFunc();
			uint UpgradeCostFunc() {
				if(State.Level < MaxLevel)
					return DataManager.Instance.GetHeroLevel(HeroID, State.Level + 1).HeroUpgradeCost;

				return 0;
			}
			public uint GetCurrentLevel => State.Level;
			public uint GetMaxLevel => MaxLevel;
			public HeroLevel GetCurrentHeroLevel => DataManager.Instance.GetHeroLevel(HeroID, State.Level);
			public HeroLevel GetMaxHeroLevel => DataManager.Instance.GetHeroLevel(HeroID, MaxLevel);
			public HeroLevel GetNextHeroLevel => GetNextHeroLevelFunc();
			HeroLevel GetNextHeroLevelFunc()
			{
				if ( State.Level < MaxLevel )
					return DataManager.Instance.GetHeroLevel(HeroID, State.Level + 1);

				return GetCurrentHeroLevel;
			}

			public string HeroName => DataManager.Instance.GetHero(HeroID).HeroName;
			public HeroData HeroData => DataManager.Instance.GetHero(HeroID);
			public HeroID HeroKeyID => HeroID;

		}

        public class WeaponState : Upgradeable
        {
            public WeaponID WeaponID;

            public WeaponState(WeaponID ID, StateData State, uint MaxLevel) : base(State, MaxLevel)
            {
                WeaponID = ID;
            }

			public override uint UpgradeCost => UpgradeCostFunc();
			uint UpgradeCostFunc()
			{
				if ( State.Level < MaxLevel )
					return DataManager.Instance.GetWeaponLevel(WeaponID, State.Level + 1).UpgradeCost;

				return 0;
			}
			public WeaponData WeaponData => DataManager.Instance.GetWeapon(WeaponID);
			public WeaponLevel CurrentWeaponLevel => DataManager.Instance.GetWeaponLevel(WeaponID, State.Level);

		}

		public class EquipmentState : Upgradeable
        {
            public EquipmentID EquipmentID;

            public EquipmentState(EquipmentID ID, StateData State, uint MaxLevel) : base(State, MaxLevel)
            {
                EquipmentID = ID;
            }
			public override uint UpgradeCost => UpgradeCostFunc();
			uint UpgradeCostFunc()
			{
				if ( State.Level < MaxLevel )
					return DataManager.Instance.GetEquipmentLevel(EquipmentID, State.Level + 1).UpgradeCost;

				return 0;
			}
			public EquipmentData EquipmentData => DataManager.Instance.GetEquipment(EquipmentID);
			public EquipmentLevel CurrentEquipmentLevel => DataManager.Instance.GetEquipmentLevel(EquipmentID, State.Level);

		}

		#endregion

		public void EquipWeapon(WeaponState newWeapon)
		{
			UnequipCurrentWeapon();
			newWeapon.Equip();
		}

		public void UnequipCurrentWeapon() {
			if(CurrentWeapon != null)
				CurrentWeapon.UnEquip();
		}

		public void EquipItem( EquipmentState equipment, EquipmentType type)
		{
			UnequipCurrentItem(type);
			equipment.Equip();
		}

		public void UnequipCurrentItem( EquipmentType type )
		{
			if ( type == EquipmentType.Chest )
			{
				if ( CurrentArmour != null )
					CurrentArmour.UnEquip();
			}
			else if ( type == EquipmentType.Head )
			{
				if ( CurrentHelmet != null )
					CurrentHelmet.UnEquip();
			}
			else if ( type == EquipmentType.Legs)
			{
				if ( CurrentBoots != null )
					CurrentBoots.UnEquip();
			}
		}

		#region Wallet

		public delegate void CurrencyAmountChanged( uint currency );
		public static event CurrencyAmountChanged OnCurrencyAmountChanged;

		public class WalletData
        {

			public class Currency
            {
				public uint Amount { private set; get; }

                public Currency(uint StartingAmount)
                {
                    Amount = StartingAmount;
                }

                public void Earn(uint ToAdd)
                {
                    Amount += ToAdd;
					OnCurrencyAmountChanged?.Invoke(Amount);
                }
                public void Use(uint ToUse)
                {
                    Amount = Math.Max(uint.MinValue, Amount - ToUse);
					OnCurrencyAmountChanged?.Invoke(Amount);
				}
			}

            public Currency Satoshi;

            public WalletData(uint SatoshiAmount)
            {
                Satoshi = new Currency(SatoshiAmount);
            }

            //public void EarnSatoshi(uint ToAdd)
            //{
            //    Satoshi.Earn(ToAdd);
            //}

            //public void UseSatoshi(uint ToUse)
            //{
            //    Satoshi.Use(ToUse);
            //}
        }

        #endregion



    }
}