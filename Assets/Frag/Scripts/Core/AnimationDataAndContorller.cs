﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationData
{
	Attack,
	Die,
	Forward,
	Idle
}



public class AnimationDataAndContorller : MonoBehaviour
{
	[SerializeField]
	string ForwardAnimationParameter;
	[SerializeField]
	string DieAnimationParameter;
	[SerializeField]
	string AttackAnimationParametet;

	[SerializeField]
	Animator animator;

	public void PlayAnimation( AnimationData data )
	{
		if ( data == AnimationData.Forward )
		{
			animator.SetBool(ForwardAnimationParameter, true);
		}
		else if ( data == AnimationData.Attack )
		{
			animator.SetTrigger(AttackAnimationParametet);
		}
		else if ( data == AnimationData.Die )
		{
			animator.SetTrigger(DieAnimationParameter);
		}
		else
		{
			animator.SetBool(ForwardAnimationParameter, false);
		}
	}

	public void SetIsDeadForPlayer(bool value)
	{
		animator.SetBool("IsDead", value);
	}

	public bool IsMoving
	{
		get { return animator.GetCurrentAnimatorStateInfo(0).IsName(ForwardAnimationParameter); }
	}




}
