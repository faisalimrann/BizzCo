﻿using UnityEngine;
using System.Collections;

namespace RoboArchero.Networking
{
    public class InBoundMessage : Message
    {
        public ServerMessage SM { protected set; get; }

        public InBoundMessage(string action = null, string data = null): base(action, data)
        {
        }
    
        public InBoundMessage(string action = null, byte[] data = null): base(action, data)
        {
        }

        public InBoundMessage(ServerMessage sM): base(string.Empty,string.Empty)
        {
            this.SM = sM;
        }
    
        public virtual void Dispatch()
        {
//        Client.OnReceivedCallback handler = null;
//        if (Client.Handlers.TryGetValue (Action, out handler))
//        {
//            Networking.instance.ResponseProcessRequests.Enqueue(() => { 
//                handler(this); 
//            } );
//        }
        }
    }
}