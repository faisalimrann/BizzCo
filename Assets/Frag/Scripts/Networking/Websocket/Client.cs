﻿
using HybridWebSocket;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoboArchero.Networking
{
    public class Client : MonoBehaviour
    {

        //private static readonly ILog Log = new ILog();//LogManager.GetLogger(typeof(Client));
        public static Client Instance;
        public static bool PrintLogs = true;

        public static Action OnConnected;

        public static event Action OnDisconnect;

        private float TimeOut = 30f;

        private string URI;
        [SerializeField]
        string NetworkServerAddress;// = "ws://dev-server.frag-games.com:10097/websocket";


        public WebSocket websocket;

        private const string CORELATIONID = "correlationId";
        Dictionary<string, Request> UnReplied = new Dictionary<string, Request>();
        Dictionary<string, Action<InBoundMessage>> Handlers = new Dictionary<string, Action<InBoundMessage>>();
        Dictionary<ServerMessageType, Action<InBoundMessage>> ProtoHandlers = new Dictionary<ServerMessageType, Action<InBoundMessage>>();
        List<string> Ids = new List<string>();

        public static bool isInitialized = false;
        public static bool isFirstConnectCalled = false;


        void Awake() // TODO: Application entery point should be one
        {
            if (!isInitialized)
            {
                ProtoHandlers.Clear();
                Handlers.Clear();

                Instance = this;
                ResponseProcessRequests = new Queue<Action>();
                PriorityResponseProcessRequests = new Queue<Action>();
                ResponseProcessRequests.Clear();
                StartCoroutine("ProcessRequests");

                isInitialized = true;
                Connect();
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }
        }




        private void OnDisable()
        {
            //Debug.LogError("Client Disabled");
            //OnConnected = null;
            //StopAllCoroutines();
        }

        public static Queue<Action> ResponseProcessRequests;
        public static Queue<Action> PriorityResponseProcessRequests;
        IEnumerator ProcessRequests()
        {
            while (true)
            {
                //if (ResponseProcessRequests.Count >= 1)
                //{
                //    try   // this will keep co-routine alive even after some exception occurs in invoke
                //    {
                //        ResponseProcessRequests.Dequeue().Invoke();
                //    } catch (Exception e)
                //    {

                //        Debug.LogError(e);
                //    }
                //}
                if (PriorityResponseProcessRequests.Count >= 1)
                {
                    try   // this will keep co-routine alive even after some exception occurs in invoke
                    {
                        PriorityResponseProcessRequests.Dequeue().Invoke();
                    }
                    catch (Exception e)
                    {

                        Debug.LogError(e);
                    }
                }
                yield return null;
            }
        }

        IEnumerator CheckRequestTimeout()
        {
            while (true)
            {
                var Values = UnReplied.Values;
                foreach (var item in Values)
                {
                    //Debug.Log("CheckRequestTimeout " + item.ElaspedTime + " " + Time.time);
                    if (TimeOut <= item.ElaspedTime)
                    {
                        Ids.Add(item.CorrelationID);
                        if (item.onFailure != null)
                        {
                            item?.onFailure("Request Time Out.\n" + item.CorrelationID);
                        }
                    }
                    else
                    {
                        item.ElaspedTime++;
                    }
                }

                for (int i = 0; i < Ids.Count; i++)
                {
                    UnReplied.Remove(Ids[i]);
                }
                Ids.Clear();
                yield return new WaitForSeconds(1f);
            }
        }

        public void OpenWebSocket()
        {
            if (websocket != null && websocket.GetState() != WebSocketState.Open & websocket.GetState() != WebSocketState.Connecting)
                Connect();

        }

        public WebSocketState GetWebSocketState()
        {
            return websocket.GetState();
        }

        public void Connect()
        {
            if (websocket != null)
            {
                switch (websocket.GetState())
                {
                    case WebSocketState.Closing:
                        websocket.Close();
                        ConnectInternal();
                        break;
                    case WebSocketState.Closed:
                        ConnectInternal();
                        break;
                    case WebSocketState.Connecting:
                        //do nothing
                        break;
                    case WebSocketState.Open:
                        websocket_Opened();
                        break;
                }
            }
            else
            {
                ConnectInternal();
            }
        }

        private void ConnectInternal()
        {
            websocket = WebSocketFactory.CreateInstance(NetworkServerAddress);
            try
            {
                websocket.OnOpen -= websocket_Opened;
                websocket.OnError -= websocket_Error;
                websocket.OnClose -= websocket_Closed;
                websocket.OnMessage -= Websocket_DataReceived;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            websocket.OnOpen += websocket_Opened;
            websocket.OnError += websocket_Error;
            websocket.OnClose += websocket_Closed;
            websocket.OnMessage += Websocket_DataReceived;
            websocket.Connect();
            Debug.Log("URI: ConnectCalled");


            DontDestroyOnLoad(this);
            StopCoroutine("CheckRequestTimeout");
            StartCoroutine("CheckRequestTimeout");
        }

        private void websocket_Opened()
        {
            if (OnConnected != null)
            {
                Debug.LogError("[WS] Connected");
                PriorityResponseProcessRequests.Enqueue(() =>
                {
                    OnConnected?.Invoke();
                    isFirstConnectCalled = true;
                });
            }
            else
                Debug.Log("OnConnected not Registered!!!");

        }

        public void Websocket_DataReceived(byte[] data)
        {
            ServerMessage Response = ServerMessage.Parser.ParseFrom(data);
            if (Response.Type != ServerMessageType.PingMessage)
                Debug.Log("[WS]Received: " + Response.ToString());
            string CorelationID = null;
            if (!string.IsNullOrEmpty(Response.RequestId))
            {
                CorelationID = Response.RequestId;
                //Debug.LogError(Response.Type);

                Request Req = null;
                if (UnReplied.TryGetValue(CorelationID, out Req))
                {
                    if (Req.onReply != null)
                    {
                        if (Response.Type != null)
                        {
                            PriorityResponseProcessRequests.Enqueue(() =>
                            {
                                Req.onReply(new Reply(Response));
                            });
                        }
                        else
                        {
                            PriorityResponseProcessRequests.Enqueue(() =>
                            {
                                Req.onReply(new Reply(Response));
                            });
                            //ResponseProcessRequests.Enqueue(() =>
                            //{
                            //	Req.onReply(new Reply(Response));
                            //});
                        }
                        UnReplied.Remove(CorelationID);
                    }
                }
            }
            else
            {
                try
                {

                    Action<InBoundMessage> CallBack = null;
                    if (ProtoHandlers.TryGetValue(Response.Type, out CallBack))
                    {
                        if (Response.Type != null)
                        {
                            PriorityResponseProcessRequests.Enqueue(() =>
                            {
                                CallBack(new InBoundMessage(Response));
                            });
                        }
                        else
                        {
                            PriorityResponseProcessRequests.Enqueue(() =>
                            {
                                CallBack(new InBoundMessage(Response));
                            });
                            //ResponseProcessRequests.Enqueue(() =>
                            //{
                            //	CallBack(new InBoundMessage(Response));
                            //});
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError(GetType().Name + " Exception Message: " + ex.Message + "\n " + ex.StackTrace);
                }
            }

        }

        void websocket_Error(string errorMsg)
        {
            Debug.LogError("Error: " + errorMsg);
        }

        void websocket_Closed(WebSocketCloseCode closeCode)
        {
            Debug.Log("Socket Closed:" + closeCode.ToString());
            UnReplied.Clear();
            Handlers.Clear();
        }


        public void Disconnect()
        {
            Debug.LogError("Client Disconnected");
            OnDisconnect?.Invoke();
            UnReplied.Clear();
            Handlers.Clear();
            if (websocket != null && websocket.GetState() == WebSocketState.Open)
                websocket.Close();
        }



        public void On(ServerMessageType type, Action<InBoundMessage> CallBack)
        {
            if (!ProtoHandlers.ContainsKey(type))
                ProtoHandlers.Add(type, CallBack);
            else
                Debug.LogError(type + " already exists!");
        }

        public void Send(Request req)
        {
            UnReplied.Add(req.CorrelationID, req);
            if (websocket.GetState() == WebSocketState.Open)
            {

                websocket.Send(req.Data);
                //Debug.Log("[WS]Sent: " + ClientMessage.Parser.ParseFrom(req.Data)); ;
            }
            else
            {

                Debug.LogError(" WebSocket Closed: " + req.ToString());
                OnConnected += SendFailedRequest;
                Connect();
            }

            void SendFailedRequest()
            {
                OnConnected -= SendFailedRequest;
                websocket.Send(req.Data);
            }
        }

        public void OutBoundMessage(Request req)
        {
            if (websocket.GetState() == WebSocketState.Open)
            {

                //Debug.Log(ClientMessage.Parser.ParseFrom(req.Data).Type);
                websocket.Send(req.Data);
            }
            else
            {
                try
                {
                    //Log.Debug(" WebSocket Closed: " + req.ToString());
                }
                catch (Exception ex)
                {
                    Debug.LogError(GetType().Name + " Exception Message: " + ex.Message + "\n " + ex.StackTrace);
                }
            }
        }


        void OnDestroy()
        {
            //Disconnect();
            //ProtoHandlers.Clear();
            //Handlers.Clear();
        }
        private void OnApplicationPause(bool pause)
        {
            Application.runInBackground = false;
            Debug.Log("OnApplicationPause: "+pause);
            if (!pause && isFirstConnectCalled)
            {
                Debug.Log("Application resumed: " + websocket.GetState().ToString());
                if (websocket.GetState() == WebSocketState.Closing)
                {
                    websocket.Close();
                    Connect();
                }
                else if (websocket.GetState() == WebSocketState.Closed)
                {
                    Connect();
                }
                else if (websocket.GetState() == WebSocketState.Open)
                {
                    Networking.Login();
                }
            }
        }

        void OnApplicationQuit()
        {
            Disconnect();
        }

    }
}

