﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RoboArchero.Networking
{
    public class RoboClientMessages
    {

		public static string ExchangeName = "203.v4.user.presence";
		public static string PlayerExchange = "";
        public static ClientMessage Ping()
        {

            PingRequest pingRequest = new PingRequest()
            {
				Id = Guid.NewGuid().ToString()
			};


			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.PingRequest,
				PingRequest = pingRequest
			};
			return message;
        }

		public static ClientMessage RegisterUser()
		{
			RegisterUserRequest RegisterUserRequest = new RegisterUserRequest()
			{
				UserName = DateTime.UtcNow.ToString().Replace("/", "").Replace(":","").Replace(" ", ""),
                HashedPassword = DateTime.UtcNow.ToString().Replace("/", "").Replace(":", "").Replace(" ", ""),
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = ExchangeName,
				Type = ClientMessageType.RegisterUserRequest,
				RegisterUserRequest = RegisterUserRequest

			};
			return message;
		}

		public static ClientMessage LoginRequestViaGoogle()
		{
			LoginWithGoogleToken loginWithGoogle = new LoginWithGoogleToken()
			{
				IdToken = Networking.authCode,
				DeviceId = SystemInfo.deviceUniqueIdentifier

			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = ExchangeName,
				Type = ClientMessageType.LoginWithGoogleToken,
				LoginWithGoogleToken = loginWithGoogle

			};
			return message;
		}

		public static ClientMessage LoginWithApple(string TokenID)
		{
			LoginWithAppleRequest LoginWithApple = new LoginWithAppleRequest()
			{
				IdToken = TokenID,
				DeviceId = SystemInfo.deviceUniqueIdentifier
                
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = ExchangeName,
				Type = ClientMessageType.LoginWithAppleRequest,
				LoginWithAppleRequest = LoginWithApple

			};
			return message;
		}

		public static ClientMessage LoginRequest()
		{
			LoginUserRequest loginUserRequest = new LoginUserRequest()
			{
				UserName = PlayerPrefs.GetString("Username"),
				HashedPassword = PlayerPrefs.GetString("Password"),
				DeviceId = SystemInfo.deviceUniqueIdentifier
			};

			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = ExchangeName,
				Type = ClientMessageType.LoginUserRequest,
				LoginUserRequest = loginUserRequest

			};

			return message;
		}

		public static ClientMessage GetMetaData()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.FetchMetadataRequest
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}

		public static ClientMessage GetInventory()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetInventory
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}

		public static ClientMessage GetGameState()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetWalletAndInventory
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}
		public static ClientMessage Equip(string itemName, ItemType itemType)
		{
			EquipItem item = new EquipItem()
			{
				Item = itemName,
				Type = itemType
			};

			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.EquipItem,
				EquipItem = item

			};
			return message;
		}

		public static ClientMessage Upgrade( string itemName, ItemCategory itemCategory )
		{
			UpgradeItem item = new UpgradeItem()
			{
				Item = itemName,
				Category = itemCategory
				
			};

			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.UpgradeItem,
				UpgradeItem = item

			};
			return message;
		}

		public static ClientMessage GetCurrentEnergy()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetCurrentEnergy
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}

		public static ClientMessage StartGame()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.StartGame
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}
		public static ClientMessage EndGame()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.EndGameSession
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}
		public static ClientMessage SessionUpdate( int stageNumber, int satoshi, int revived)
		{
			UpdateSession item = new UpdateSession()
			{
				Stage = stageNumber,
				SatoshiCollected = satoshi,
				Revived = revived
			};

			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.UpdateSession,
				UpdateSession = item

			};
			return message;
		}

		public static ClientMessage PurchaseEnergy()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.PurchaseEnergy
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}

		public static ClientMessage AdEnergyBonus()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetAdEnergyBonus
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}
		public static ClientMessage GetChestTimers()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetChestTimers
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}
		public static ClientMessage GetAvailableChests()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetAvailableChests
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}

		public static ClientMessage OpenChests(ChestType type)
		{
			OpenChest open = new OpenChest()
			{
				Type = type
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.OpenChest,
				OpenChest = open
			};
			return message;
		}

		public static ClientMessage PurchaseChest( ChestType type )
		{
			PurchaseChest open = new PurchaseChest()
			{
				Type = type
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.PurchaseChest,
				PurchaseChest = open
			};
			return message;
		}

		public static ClientMessage GiveFeedback(string name, string email, string body)
		{
			Feedback feedback = new Feedback()
			{
				Name = name,
				Email = email,
				Subject = "Feedback",
				Body = body
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.Feedback,
				Feedback = feedback
			};
			return message;
		}

		public static ClientMessage ChangeHero( string name )
		{
			SelectHero hero = new SelectHero()
			{
				Name = name
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.SelectHero,
				SelectHero = hero
			};
			return message;
		}

		public static ClientMessage UnlockEquipment( string name, ItemCategory category )
		{
			UnlockItem unlockItem = new UnlockItem()
			{
				Item = name,
				Category= category

			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.UnlockItem,
				UnlockItem = unlockItem
			};
			return message;
		}

		public static ClientMessage Withdraw( string address, int amount)
		{
			WithdrawSatoshi withdrawSatoshi = new WithdrawSatoshi()
			{
				Address = address,
				Amount = amount
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.WithdrawSatoshi,
				WithdrawSatoshi = withdrawSatoshi
			};
			return message;
		}

		public static ClientMessage GetTransactions()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetTransactionHistory
				//FetchMetadataRequest = new FetchMetaDataRequest()
			};
			return message;
		}

		public static ClientMessage PurchaseIAP(string itemId, string appName, string purchaseToken, string productId, string packageName, int purchaseType)
		{
			VerifyPlayStorePurchase verifyPlay = new VerifyPlayStorePurchase()
			{
				ItemId = itemId,
				AppName = appName,
				PurchaseToken = purchaseToken,
				ProductId = productId,
				PackageName = packageName,
				PurchaseType = purchaseType
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.VerifyPlayStore,
				VerifyPlaystorePurchase = verifyPlay
			};
			return message;
		}

		public static ClientMessage PurchaseIAPApple( string itemId, string receipt )
		{
			VerifyItunesPurchase verifyPlay = new VerifyItunesPurchase()
			{
				ItemId = itemId,
				Receipt = receipt
			};
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.VerifyItunesStore,
				VerifyItunesPurchase = verifyPlay
			};
			return message;
		}

		public static ClientMessage GetWallet()
		{
			ClientMessage message = new ClientMessage()
			{
				RequestId = Guid.NewGuid().ToString(),
				Exchange = PlayerExchange,
				Type = ClientMessageType.GetWallet,
			};
			return message;
		}
	}

}