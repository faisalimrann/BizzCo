﻿using System;
using System.Collections;
using System.Threading.Tasks;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RoboArchero.Networking
{
    public class Networking : MonoBehaviour
    {
        public static string Username = "";
        public static string authCode = "";
        public static bool PlayCoinAnim = false;

        public static bool isInitialized = false;
        public static bool isGameSateReceived = false;
        //public delegate void UserConnected(Room room);
        //public static event UserConnected OnUserConnected;

        public delegate void UserConnected();
        public static event UserConnected OnUserConnected;


		public delegate void MaintainanceBreak(long time);
		public static event MaintainanceBreak OnMaintainanceBreak;

		public object CustomMessages { get; private set; }

        //public void DebindVOIP()
        // {
        //     onInvitationToVoIPRoomRecieved.G;

        // }
        private void OnDestroy()
        {
            Client.OnConnected -= OnConnection;
        }

        public void OnEnable()
        {
            if (!isInitialized)
            {
                Client.OnConnected += OnConnection;
                DontDestroyOnLoad(this);
                isInitialized = true;
            }
            else
            {
                DestroyImmediate(this.gameObject);
            }
            // Client.Instance.Connect();

        }

        public void OnConnection()
        {
            Debug.Log("On Connection");
            Client.Instance.On(ServerMessageType.PingMessage, (sM) =>
            {
                Ping();
            });

            Client.Instance.On(ServerMessageType.MaintenancePush, (sM) =>
            {
                OnMaintainanceBreak?.Invoke(sM.SM.MaintenancePush.Time);
            });

            Login();
        }

		public static PlayGamesPlatform platform;
		static AppleSigInHandler apple;
		public static void Login()
		{

#if UNITY_ANDROID && !UNITY_EDITOR
			if ( string.IsNullOrEmpty(authCode) && platform == null )
			{
				
					PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().RequestIdToken().RequestEmail().AddOauthScope("profile").Build();
					PlayGamesPlatform.InitializeInstance(config);
					// recommended for debugging:
					PlayGamesPlatform.DebugLogEnabled = true;
					// Activate the Google Play Games platform
				
				platform = PlayGamesPlatform.Activate();
				PlayGamesPlatform.Instance.Authenticate(( result, status ) =>
				{
					// handle results
					if ( result )
					{
						Networking.authCode = PlayGamesPlatform.Instance.GetIdToken();
						Networking.LoginRequestViaGoogleAuth();
					}
					else
					{
						Debug.LogError(status);
					}
				});
			}
			else
			{
				Networking.LoginRequestViaGoogleAuth();
			}

#elif UNITY_IOS && !UNITY_EDITOR
            if (string.IsNullOrEmpty(authCode) && apple == null)
            {
                apple = new AppleSigInHandler();
                void AppleResponseHandler()
                {
                    Debug.Log(apple.CurrentState + " " + apple.LastError);
                    if (apple.CurrentState == AppleAuth.Enums.CredentialState.Authorized)
                    {
                        authCode = apple.TokenId;
                        Username = apple.DisplayName;
                        LoginWithApple(authCode);//send auth code to server
                    }
                    apple.OnAppleResponse -= AppleResponseHandler;
                    apple = null;
                }

                if (apple.IsInitialized)
                {
                    apple.OnAppleResponse -= AppleResponseHandler;
                    apple.OnAppleResponse += AppleResponseHandler;
                    apple.LoginWithApple();
                }
                else
                {
                    LoginWithServer();
                }
            }
            else
            {
                LoginWithApple(authCode);
            }
#else
			LoginWithServer();
#endif
		
            void LoginWithServer()
            {
                if (string.IsNullOrEmpty(PlayerPrefs.GetString("Username")))
                {
                    RegisterUser();
                }
                else
                {
                    LoginRequest();
                }
            }
        }

        public void Ping()
        {
            var builder = RoboClientMessages.Ping();
            PushToServer(builder);
        }

        private static void GeneralQuery(string debugString, ClientMessage CM, Action<ServerMessage> OnSuccess = null, Action<string> OnFail = null)
        {
            new Request(CM)
            .OnReply((reply) =>
            {
                try
                {
                    bool success = (reply.SM.Code == "200");
                    if (success && OnSuccess != null)
                    {

                        OnSuccess(reply.SM);
                    }
                    else if (OnFail != null)
                    {
                        OnFail(reply.SM.Description);

                    }
                    else if (!success)
                        OnFail(reply.SM.Description);

                }
                catch (Exception _ex)
                {
                    Debug.LogException(_ex);
                }
            })
            .OnFailure((message) =>
            {
                OnFail?.Invoke(message);
            })
            .Send();
        }

        private static void PushToServer(ClientMessage CM)
        {
            Request req = new Request(CM);
            Client.Instance.OutBoundMessage(req);
        }

        void Disconnect()
        {
            Client.Instance.Disconnect();
        }

        public static void RegisterUser()
        {
            var builder = RoboClientMessages.RegisterUser();
            GeneralQuery(ClientMessageType.RegisterUserRequest.ToString(), builder, (sM) =>
            {
                PlayerPrefs.SetString("Username", builder.RegisterUserRequest.UserName);
                PlayerPrefs.SetString("Password", builder.RegisterUserRequest.HashedPassword);
                LoginRequest();
            }, (status) =>
            {
                Debug.LogError(status);
            });
        }

        public static void LoginRequestViaGoogleAuth()
        {
            var builder = RoboClientMessages.LoginRequestViaGoogle();
            GeneralQuery(ClientMessageType.LoginWithGoogleToken.ToString(), builder, (sM) =>
            {
                RoboClientMessages.PlayerExchange = sM.SocialLoginResponse.Exchange;
                Username = sM.SocialLoginResponse.UserName;
                if (!isGameSateReceived)
                {
                    DataManager.Instance.GetMetaData();
                    isGameSateReceived = true;
                }
            }, (status) =>
            {
                Debug.LogError(status);
            });
        }

        public static void LoginWithApple(string TokenID)
        {
            var builder = RoboClientMessages.LoginWithApple(TokenID);
            GeneralQuery(ClientMessageType.LoginWithAppleRequest.ToString(), builder, (sM) =>
            {
                RoboClientMessages.PlayerExchange = sM.SocialLoginResponse.Exchange;
                //Username = sM.SocialLoginResponse.UserName;
                if (!isGameSateReceived)
                {
                    DataManager.Instance.GetMetaData();
                    isGameSateReceived = true;
                }
            }, (status) =>
            {
                Debug.LogError(status);
            });
        }

        public static void LoginRequest()
        {
            var builder = RoboClientMessages.LoginRequest();
            GeneralQuery(ClientMessageType.LoginUserRequest.ToString(), builder, (sM) =>
            {
                RoboClientMessages.PlayerExchange = sM.LoginUserResponse.Exchange;
				if ( !isGameSateReceived )
				{
					DataManager.Instance.GetMetaData();
					isGameSateReceived = true;
				}
			}, (status) =>
            {
                Debug.LogError(status);
            });
        }

        public static void GetMetaData(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.FetchMetadataRequest.ToString(), RoboClientMessages.GetMetaData(), onSuccess, onFail);
        }

        public static void GetInventory(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetInventory.ToString(), RoboClientMessages.GetInventory(), onSuccess, onFail);
        }

        public static void GetGameState(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetWalletAndInventory.ToString(), RoboClientMessages.GetGameState(), onSuccess, onFail);
        }

        public static void EquipItem(string itemName, ItemType type, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.EquipItem.ToString(), RoboClientMessages.Equip(itemName, type), onSuccess, onFail);
        }

        public static void UpgradeItem(string itemName, ItemCategory category, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.EquipItem.ToString(), RoboClientMessages.Upgrade(itemName, category), onSuccess, onFail);
        }

        public static void GetCurrentEnergy(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetCurrentEnergy.ToString(), RoboClientMessages.GetCurrentEnergy(), onSuccess, onFail);
        }

        public static void StartGame(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.StartGame.ToString(), RoboClientMessages.StartGame(), onSuccess, onFail);
        }

        public static void EndGame(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.EndGameSession.ToString(), RoboClientMessages.EndGame(), onSuccess, onFail);
        }

        public static void UpdateSession(int stageNo, int satoshiReward, int revived, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.UpdateSession.ToString(), RoboClientMessages.SessionUpdate(stageNo, satoshiReward, revived), onSuccess, onFail);
        }

        public static void BuyEnergySatoshi(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.PurchaseEnergy.ToString(), RoboClientMessages.PurchaseEnergy(), onSuccess, onFail);
        }
        public static void BuyEnergyAd(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetAdEnergyBonus.ToString(), RoboClientMessages.AdEnergyBonus(), onSuccess, onFail);
        }
        public static void GetChestTimers(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetChestTimers.ToString(), RoboClientMessages.GetChestTimers(), onSuccess, onFail);
        }
        public static void GetAvailableChests(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetAvailableChests.ToString(), RoboClientMessages.GetAvailableChests(), onSuccess, onFail);
        }
        public static void OpenChests(ChestType type, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.OpenChest.ToString(), RoboClientMessages.OpenChests(type), onSuccess, onFail);
        }
        public static void PurchaseChest(ChestType type, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.PurchaseChest.ToString(), RoboClientMessages.PurchaseChest(type), onSuccess, onFail);
        }

        public static void Feedback(string name, string email, string body, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.Feedback.ToString(), RoboClientMessages.GiveFeedback(name, email, body), onSuccess, onFail);
        }
        public static void SelectHero(string name, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.SelectHero.ToString(), RoboClientMessages.ChangeHero(name), onSuccess, onFail);
        }
        public static void UnlockItem(string name, ItemCategory cat, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.UnlockItem.ToString(), RoboClientMessages.UnlockEquipment(name, cat), onSuccess, onFail);
        }
        public static void WithdrawSatoshi(string address, int amount, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.WithdrawSatoshi.ToString(), RoboClientMessages.Withdraw(address, amount), onSuccess, onFail);
        }
        public static void GetTransactionHistory(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetTransactionHistory.ToString(), RoboClientMessages.GetTransactions(), onSuccess, onFail);
        }

        public static void PurchaseIAP(string itemId, string appName, string purchaseToken, string productId, string packageName, int purchaseType, Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.VerifyPlayStore.ToString(), RoboClientMessages.PurchaseIAP(itemId, appName, purchaseToken, productId, packageName, purchaseType), onSuccess, onFail);
        }
		public static void PurchaseIAPApple( string itemId, string receipt, Action<ServerMessage> onSuccess, Action<string> onFail )
		{
			GeneralQuery(ClientMessageType.VerifyPlayStore.ToString(), RoboClientMessages.PurchaseIAPApple(itemId, receipt), onSuccess, onFail);
		}

		public static void GetWallet(Action<ServerMessage> onSuccess, Action<string> onFail)
        {
            GeneralQuery(ClientMessageType.GetWallet.ToString(), RoboClientMessages.GetWallet(), onSuccess, onFail);
        }
    }

}
