﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System.Threading.Tasks;
using RoboArchero.Networking;

public class GooglePlayBuilder : MonoBehaviour
{
	//Start is called before the first frame update
	public static PlayGamesPlatform platform;
	void Start()
	{
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().RequestIdToken().RequestEmail().AddOauthScope("profile").Build();
		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		platform = PlayGamesPlatform.Activate();
		PlayGamesPlatform.Instance.Authenticate(( result, status ) =>
		{
			// handle results
			if ( result )
			{
				Networking.authCode = PlayGamesPlatform.Instance.GetIdToken();
				Networking.LoginRequestViaGoogleAuth();
			}
			else
			{
				Debug.LogError(status);
			}
		});
		DontDestroyOnLoad(this);

	}
}
