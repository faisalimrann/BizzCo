﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransactionHistoryItem : MonoBehaviour
{
	public Text Amount;
	public Text Status;
	public Text Date;

}
