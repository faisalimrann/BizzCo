﻿using System.Threading.Tasks;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;

public class LoaderScreen : MonoBehaviour
{
    public static LoaderScreen Instance;

    [SerializeField] CanvasGroup Parent;
    [SerializeField] Image fillImage;
    [SerializeField] TMPro.TMP_Text tips;
    [SerializeField] string[] TipsText;
    float fillIMageVal = 0;
    bool isEnabled = false;
    int tipCount = 0;

	static bool isInitialized = false;

	private void Start()
	{
		if ( isInitialized )
		{
			this.enabled = false;
			DestroyImmediate(this.gameObject);
		}
		else
		{
			isInitialized = true;
			Instance = this;
			DontDestroyOnLoad(this);
		//isEnabled = true;
			SetStatus(true);
			SoundManager.Play("Main Menu");
		}
	}

    public void SetStatus(bool enable)
    {
        Debug.Log("SetStatus");
        isEnabled = enable;
        if (isEnabled)
        {
            Enable();
        }
        else
        {
            Disable();
        }
    }

    async Task Loading()
    {
        while (isEnabled)
        {
            fillIMageVal += 0.05f;
            fillImage.fillAmount = fillIMageVal;
            if (fillIMageVal > 1f)
            {
                fillIMageVal = 0f;
                tipCount = (tipCount + 1) % TipsText.Length;
                tips.text = TipsText[Random.Range(0, TipsText.Length)];
            }
            await Task.Delay(300);
        }
    }

    private void Enable()
    {
        fillImage.fillAmount = fillIMageVal;
        tipCount = Random.Range(0, TipsText.Length);
        tips.text = TipsText[Random.Range(0, TipsText.Length)];
        Loading();

        Parent.alpha = 1;
        Parent.blocksRaycasts = true;

		
    }

    private void Disable()
    {
        Parent.alpha = 0;
        Parent.blocksRaycasts = false;
		if ( Networking.PlayCoinAnim )
		{
			GameObject.Find("Animations").transform.GetChild(1).gameObject.SetActive(true);
			Networking.PlayCoinAnim = false;
		}
	}

}
