﻿using System.Collections;
using System.Collections.Generic;
using GameData;
using RoboArchero.Networking;
using UnityEngine;
using UnityEngine.UI;

public class EnergyManager : MonoBehaviour
{
	// Start is called before the first frame update
	[SerializeField]
	Text energyText;
	[SerializeField]
	Image fillImage;
	[SerializeField]
	Text EnergyTimerText;
	[SerializeField]
	GameObject EnergyAnimation;

	[SerializeField]
	Text energyTip;

	int MaxEnergyAllowed;
	int CurrentEnergy;
	long timeTillNextEnergy = 0;

	int _remainingAdsBonuses = 4;
	public int RemainingAdsBonuses { get { return _remainingAdsBonuses; } private set { } }

	Coroutine coroutine;
	private void Start()
	{
		PlayerData.OnGameDataRecieved += Populate;
	}

	private void Populate()
	{
		MaxEnergyAllowed = (int)DataManager.Instance.TitleData().EnergyTotal;
		Networking.GetCurrentEnergy((sM) =>
		{
			CurrentEnergy = sM.GetCurrentEnergyResponse.Energy;
			DisplayEnergy();
			if ( sM.GetCurrentEnergyResponse.NextEnergy > 0 )
			{
				timeTillNextEnergy = sM.GetCurrentEnergyResponse.NextEnergy;
				if ( coroutine != null )
				{
					StopCoroutine(coroutine);
				}
				coroutine = StartCoroutine("EnergyTimer");
			}
			else
			{
				EnergyTimerText.text = "";
			}
			//TOdo set remiaining ad bonuses;
			_remainingAdsBonuses = sM.GetCurrentEnergyResponse.RemainingAdBonus;
			if ( PlayerPrefs.GetInt("FirstRun", 0) == 0 )
			{
				energyTip.gameObject.SetActive(true);
				StartCoroutine("EnergyText");
				PlayerPrefs.SetInt("FirstRun", 1);
			}
			LoaderScreen.Instance.SetStatus(false);
		}, ( status ) =>
		{
			Debug.LogError(status);
		});
	}

	void DisplayEnergy()
	{
		energyText.text = CurrentEnergy + "/" + MaxEnergyAllowed;
		fillImage.fillAmount = (CurrentEnergy * 1f) / MaxEnergyAllowed;
	}

	private void OnDestroy()
	{
		PlayerData.OnGameDataRecieved -= Populate;
		if(coroutine != null)
		StopCoroutine(coroutine);
	}

	public void AddEnergy()
	{
		Networking.GetCurrentEnergy(( sM ) =>
		{
			CurrentEnergy = sM.GetCurrentEnergyResponse.Energy;
			DisplayEnergy();
			if ( sM.GetCurrentEnergyResponse.NextEnergy > 0 )
			{
				timeTillNextEnergy = sM.GetCurrentEnergyResponse.NextEnergy;
				if ( coroutine != null )
				{
					StopCoroutine(coroutine);
				}
				coroutine = StartCoroutine("EnergyTimer");
			}
			else
			{
				if ( coroutine != null )
				{
					StopCoroutine(coroutine);
				}
				EnergyTimerText.text = "";
			}
			PlayEnergyGainAnimation();
		}, ( status ) =>
		{
			Debug.LogError(status);
		});
	}

	void PlayEnergyGainAnimation()
	{
		EnergyAnimation.SetActive(true);
	}

	IEnumerator EnergyTimer()
	{
		while ( timeTillNextEnergy > 0 )
		{
			timeTillNextEnergy -= 1000;
			EnergyTimerText.text = (timeTillNextEnergy / 60000) + ":" + ((timeTillNextEnergy % 60000) / 1000).ToString("00");
			yield return new WaitForSeconds(1f);
		}
		Populate();
	}

	IEnumerator EnergyText()
	{
		yield return new WaitForSeconds(5f);
		energyTip.gameObject.SetActive(false);
	}
}
